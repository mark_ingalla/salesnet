﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Ap21API.Resources;

namespace Ap21API.Entities.Product
{
	public abstract partial class ProductsBase<T, R> : ResourceCollection<T, R>
	{
		/// <summary>
		/// Return a collection of products
		/// </summary>
		/// <param name="startRow">The record number to return first from the matching recordset. If 200 products are returned, and startRow is set to 50, then products 50 to 200 will be returned.</param>
		/// <param name="pageRows">The size of the complete recordset to return. If the resultset matched 200 products, and pageRows was set to 20, then only 20 products will be returned, starting from startRow.</param>
		/// <returns>Collection of Products. Returns null if nothing is found.</returns>
		public static T Find(int? startRow = null, int? pageRows = null)
		{
			return FindInternal(new { PageRows = pageRows, StartRow = startRow });
		}

		/// <summary>
		/// Return a collection of products
		/// </summary>
		/// <param name="query">Product search query string</param>
        /// <param name="styleQuery">Product search style query string</param>
		/// <param name="colourFilter">Product colour code</param>
		/// <param name="sizeFilter">Product size code</param>
		/// <param name="orderBy">Field to order results on. e.g. 'price'</param>
		/// <param name="startRow">The record number to return first from the matching recordset. If 200 products are returned, and startRow is set to 50, then products 50 to 200 will be returned.</param>
		/// <param name="pageRows">The size of the complete recordset to return. If the resultset matched 200 products, and pageRows was set to 20, then only 20 products will be returned, starting from startRow.</param>
        /// <param name="showNoStockProducts">Determines whether to include no stock products or not.</param>
		/// <returns>Collection of Products. Returns null if nothing is found.</returns>
        public static T Find(string query, string styleQuery = null, string colourFilter = null, string sizeFilter = null,
									string orderBy = null, int? startRow = null, int? pageRows = null, bool? showNoStockProducts = true)
		{
            return FindInternal(new { ColourFilter = colourFilter, OrderBy = orderBy, PageRows = pageRows, Query = query, StyleQuery = styleQuery, SizeFilter = sizeFilter, StartRow = startRow, ShowNoStockProducts = showNoStockProducts });
		}
	}
}