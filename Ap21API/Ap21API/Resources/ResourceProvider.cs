﻿using System;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Xml.Serialization;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace Ap21API.Resources
{
	/// <summary>
	/// This interface provides configuration information to the resource provider.
	/// </summary>
	/// <remarks>It must be implemented</remarks>
	public interface IResourceProviderConfiguration
	{
		/// <summary>
		/// The Id of the person making API calls.
		/// </summary>
		string RequestPersonId { get; }

        /// <summary>
        /// The Id of the customer the person attached to making API calls.
        /// </summary>
        string RequestCustomerId { get; }

		/// <summary>
		/// The URL of the site providing the API
		/// </summary>
		/// <example>http://www.shop.com/store/salesnet.svc</example>
		string Site { get; }
	}

	/// <summary>
	/// CLass that makes the HTTP calls to the REST API
	/// </summary>
	public class ResourceProvider
	{
		private static IResourceProviderConfiguration configuration = null;

		/// <summary>
		/// Set this property to an object which implements the required interface. ResourceProvider then calls it to get configuration information.
		/// </summary>
		public static IResourceProviderConfiguration Configuration
		{
			private get
			{
				if (ResourceProvider.configuration != null)
				{
					return ResourceProvider.configuration;
				}
				else
				{
					throw new AP21APIException("Configuration information for the ResourceProvider is not set.");
				}
			}

			set
			{
				ResourceProvider.configuration = value;
			}
		}

		/// <summary>
		/// Create a resource by making a HTTP POST call
		/// </summary>
		/// <param name="path">Resource path. e.g. 'products/34'</param>
		/// <param name="payload">HTTP body content</param>
		/// <returns>Response from server</returns>
		public static string Create(string path, string payload)
		{
			string url = ResourceProvider.URL(path);
			
			return HttpCall("POST", url, payload);
		}

		/// <summary>
		/// Delete a resource by making a HTTP DELETE call
		/// </summary>
		/// <param name="path">Resource path. e.g. 'products/34'</param>
		/// <param name="query">HTTP query string to append to the call URL. e.g. 'Email=bob@home.com&Age=22'</param>
		/// <returns>Response from the server</returns>
		public static string Delete(string path, string query)
		{
			string url = ResourceProvider.URL(path, query);

			return HttpCall("DELETE", url);
		}
		
		/// <summary>
		/// Get a resource by making a HTTP GET call
		/// </summary>
		/// <param name="path">Resource path. e.g. 'products/34'</param>
		/// <param name="query">HTTP query string to append to the call URL. e.g. 'Email=bob@home.com&Age=22'</param>
		/// <returns>Response from the server</returns>
		public static string Get(string path, string query)
		{
			string url = ResourceProvider.URL(path, query);

			return HttpCall("GET", url);
		}

		private static Stream GetStreamFromResponse(WebResponse response)
		{
			switch (response.Headers["Content-Encoding"] != null ? response.Headers["Content-Encoding"].ToString().ToLowerInvariant() : "")
			{
				case "gzip":
					return new GZipStream(response.GetResponseStream(), CompressionMode.Decompress);
				case "deflate":
					return new DeflateStream(response.GetResponseStream(), CompressionMode.Decompress);
				default:
					return response.GetResponseStream();
			}
		}

        //Accept any certificate for SSL connections... 
        public static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        /// <summary>
		/// Make a HTTP call
		/// </summary>
		/// <param name="method">HTTP Method. e.g. "GET", "POST", "PUT" or "DELETE"</param>
		/// <param name="url">API URL</param>
		/// <param name="payload">HTTP body content</param>
		/// <returns>Response from the server</returns>
		private static string HttpCall(string method, string url, string payload = null)
		{
            var request = (HttpWebRequest)WebRequest.Create(url);
            //allows for validation of SSL certificates 
            ServicePointManager.ServerCertificateValidationCallback += new System.Net.Security.RemoteCertificateValidationCallback(ValidateServerCertificate);
			request.ContentType = "text/xml;charset=\"utf-8\"";
			request.Headers.Add("Accept-Encoding", "gzip, deflate");
            request.Accept = "version_" + System.Configuration.ConfigurationManager.AppSettings.Get("API_Version");
            if (!String.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings.Get("API_AuthUser")))
            {
                request.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings.Get("API_AuthUser"), System.Configuration.ConfigurationManager.AppSettings.Get("API_AuthPassword"));
            }
			request.Method = method;

			try
			{
				if (payload != null)
				{
					byte[] byteData = System.Text.UTF8Encoding.UTF8.GetBytes(payload);
					request.ContentLength = byteData.Length;

					using (Stream stream = request.GetRequestStream())
					{
						stream.Write(byteData, 0, byteData.Length);
					}  
				}

				using (WebResponse response = request.GetResponse())
				{
                    using (Stream responseStream = ResourceProvider.GetStreamFromResponse(response))
					{
						using (StreamReader reader = new StreamReader(responseStream))
						{
							return reader.ReadToEnd();
						}
					}
				}
			}
            catch (WebException ex)
			{
                if (ex.Response is HttpWebResponse)
                {
                    HttpWebResponse response = ex.Response as HttpWebResponse;
                    using (Stream responseStream = ResourceProvider.GetStreamFromResponse(response))
                    using (StreamReader ap21exreader = new StreamReader(responseStream))
                    {
                        AP21ErrorResponse curreuntEX = new AP21ErrorResponse();
                        XmlSerializer deserializer = new XmlSerializer(typeof(AP21ErrorResponse));

                        using (StringReader reader = new StringReader(ap21exreader.ReadToEnd()))
                        {
                            curreuntEX = (AP21ErrorResponse)deserializer.Deserialize(reader);
                        }

                        throw new AP21APIException(curreuntEX.Description, curreuntEX.ErrorCode);
                    }                    
                }

                throw;
			}
		}

		/// <summary>
		/// Update a resource by making a HTTP PUT call
		/// </summary>
		/// <param name="path">Resource path. e.g. 'products/34'</param>
		/// <param name="payload">HTTP body content</param>
		/// <returns>Response from server</returns>
		public static void Update(string path, string payload)
		{
			string url = ResourceProvider.URL(path);

			HttpCall("PUT", url, payload);
		}

		/// <summary>
		/// Given a path and query string, return a complete URL to make API calls
		/// </summary>
		/// <param name="path">Resource path. e.g. 'products/34'</param>
		/// <param name="query">HTTP query string to append to the call URL. e.g. 'Email=bob@home.com&Age=22'</param>
		/// <returns>Complete URL with Site, path and query.</returns>
		private static string URL(string path, string query = "")
		{
			UriBuilder uri = new UriBuilder(string.Format("{0}/{1}", ResourceProvider.Configuration.Site, path));
            uri.Query = string.Format("requestPersonId={0}&requestCustomerId={1}&{2}", ResourceProvider.Configuration.RequestPersonId, ResourceProvider.Configuration.RequestCustomerId, query);

			return uri.ToString();
		}
	}
}
