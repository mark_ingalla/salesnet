﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Ap21API.Resources;

namespace Ap21API.Entities.Product
{
	public partial class Product : ProductBase<Product>
	{
		public static string Path
		{
			get
			{
				return "products/:Id";
			}
		}
	}
}