﻿using Ap21API.Resources;

namespace Ap21API.Entities.Reference
{
    public partial class ReferenceType : Resource<ReferenceType>
    {
        public static string Path
        {
            get
            {
                return "referencetypes/:Id";
            }
        }

        public static ReferenceType Find(string id)
        {
            return FindInternal(new { Id = id });
        }
    }
}
