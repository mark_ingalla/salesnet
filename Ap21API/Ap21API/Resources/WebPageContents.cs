﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ap21API.Resources;
namespace Ap21API.Entities.WebPageContent
{

    public partial class WebPageContents : ResourceCollection<WebPageContents, WebPageContent>
    {
        public static string Path
        {
            get
            {
                //return "PageContents/:requestPersonId";
                return "PageContents";
            }
        }

        /// <summary>
        /// Find an Order by its Id
        /// </summary>
        /// <param name="orderId">Order Id to find</param>
        /// <returns>Order object</returns>
        public static WebPageContents Find(string id)
        {
            return FindInternal(new { Id = id });
        }
    }
}
