﻿using Ap21API.Resources;

namespace Ap21API.Entities.Customer
{
    public partial class Customer : Resource<Customer>
    {
        public static string Path
        {
            get
            {
                return "customers/:Id";
            }
        }

        /// <summary>
        /// Find an Customer by its Id
        /// </summary>
        /// <param name="customerId">Order Id to find</param>
        /// <returns>Order object</returns>
        public static Customer Find(string id)
        {
            return FindInternal(new { Id = id });
        }
    }
}
