﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ap21API
{
    public class AP21APIException : Exception
    {
        private int errorCode;
        private string message;

        public int ErrorCode
        {
            get { return errorCode; }
        }

        public override string Message
        {
            get { return message; }
        }

        public AP21APIException(string message)
        {
            this.message = message;
        }

        public AP21APIException(string message, int errorCode)
        {
            this.errorCode = errorCode;
            this.message = message;
        }

        public int GetHttpStatusCode()
        {
            if (errorCode == 5008 || errorCode == 5029)
            {
                return 404;  //result not found
            }

            if (errorCode == 5010)
            {
                return 409;  //Timestamp conflict
            }

            if (errorCode == 5000 || errorCode == 5001 || errorCode == 5002 || errorCode == 5003 || errorCode == 5004 || errorCode == 5005 || errorCode == 5006
                || errorCode == 5007 || errorCode == 5009 || errorCode == 5011 || errorCode == 5013 || errorCode == 5018 || errorCode == 5020
                || errorCode == 5021 || errorCode == 5022 || errorCode == 5023 || errorCode == 5024
                || errorCode == 5025 || errorCode == 5026 || errorCode == 5033 || errorCode == 5034 || errorCode == 5045)
            {
                return 400;
            }
            return 403;   //Other ap21 errors
        }
    }	
}
