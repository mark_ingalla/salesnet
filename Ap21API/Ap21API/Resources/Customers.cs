﻿using Ap21API.Resources;

namespace Ap21API.Entities.Customer
{
    public partial class Customers : ResourceCollection<Customers, Customer>
    {
        public static string Path
        {
            get
            {
                return "customers";
            }
        }

        /// <summary>
        /// Find a collection of Orders matching a given search criteria
        /// </summary>
        /// <param name="startRow">The record number to return first from the matching recordset. If 200 orders are returned, and startRow is set to 50, then orders 50 to 200 will be returned.</param>
        /// <param name="pageRows">The size of the complete recordset to return. If the resultset matched 200 orders, and pageRows was set to 20, then only 20 orders will be returned, starting from startRow.</param>
        /// <returns>Collection of Orders. Returns null if nothing is found.</returns>
        public static Customers Find(int? startRow = null, int? pageRows = null)
        {
            return FindInternal(new
            {
                PageRows = pageRows,
                StartRow = startRow
            });
        }
    }
}
