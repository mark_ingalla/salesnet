﻿
using Ap21API.Resources;

namespace Ap21API.Entities.Order
{
	public partial class Order : Resource<Order>
	{
		public static string Path
		{
			get
			{
				return "orders/:Id";
			}
		}

		/// <summary>
		/// Find an Order by its Id
		/// </summary>
		/// <param name="orderId">Order Id to find</param>
		/// <returns>Order object</returns>
		public static Order Find(string id)
		{
			return FindInternal(new { Id = id });
		}
	}
}