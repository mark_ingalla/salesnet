﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Ap21API.Resources;

namespace Ap21API.Entities.Order
{
	public partial class Orders : ResourceCollection<Orders, Order>
	{
		public static string Path
		{
			get
			{
				return "orders/";
			}
		}																									 

		/// <summary>
		/// Find a collection of Orders matching a given search criteria
		/// </summary>
		/// <param name="createdDateFrom">Find orders created between this date and createdDateTo. Format: yyyy-mm-dd</param>
		/// <param name="createdDateTo">Find orders created after createdDateFrom and this date. Format: yyyy-mm-dd</param>
		/// <param name="customerReference">Order customer reference</param>
		/// <param name="dueDateFrom">Find orders due between this date and dueDateTo. Format: yyyy-mm-dd</param>
		/// <param name="dueDateTo">Find orders due after dueDateFrom to this date. Format: yyyy-mm-dd</param>
		/// <param name="productFilter">Find the given string anywhere in the code or description properties of products</param>
		/// <param name="transactionNumber">Order transaction number</param>
		/// <param name="transactionType">Order transaction type. e.g. Invoice</param>
		/// <param name="startRow">The record number to return first from the matching recordset. If 200 orders are returned, and startRow is set to 50, then orders 50 to 200 will be returned.</param>
		/// <param name="pageRows">The size of the complete recordset to return. If the resultset matched 200 orders, and pageRows was set to 20, then only 20 orders will be returned, starting from startRow.</param>
		/// <returns>Collection of Orders. Returns null if nothing is found.</returns>
		public static Orders Find(string createdDateFrom = null, string createdDateTo = null, string customerReference = null, 
									string dueDateFrom = null, string dueDateTo = null, string productFilter = null,
									string transactionNumber = null, string transactionType = null, int? startRow = null, int? pageRows = null)
		{
			return FindInternal(new { CreatedDateFrom = createdDateFrom, CreatedDateTo = createdDateTo, CustRef = customerReference,
										DueDateFrom = dueDateFrom, DueDateTo = dueDateTo, PageRows = pageRows, ProductFilter = productFilter,
										StartRow = startRow, TransNumber = transactionNumber, TransType = transactionType });
		}
	}
}