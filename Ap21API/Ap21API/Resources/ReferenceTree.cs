﻿
using Ap21API.Resources;

namespace Ap21API.Entities.Reference
{
	public partial class ReferenceTree : Resource<ReferenceTree>
	{
		public static string Path
		{
			get
			{
				return "referencetree";
			}
		}

		/// <summary>
		/// Return product reference tree, given matching criteria.
		/// </summary>
		/// <param name="level1">Level 1 reference Id</param>
		/// <param name="level2">Level 2 reference Id</param>
		/// <param name="level3">Level 3 reference Id</param>
		/// <param name="level4">Level 4 reference Id</param>
		/// <param name="level5">Level 5 reference Id</param>
		/// <param name="orderBy">Option used to order returned references. e.g. 'ReferenceName:asc'</param>
		/// <returns>ReferenceTree structure</returns>
		public static ReferenceTree Find(string level1 = null, string level2 = null, string level3 = null, string level4 = null, string level5 = null, string orderBy = null)
		{
			return FindInternal(new { Level1 = level1, Level2 = level2, Level3 = level3, Level4 = level4, Level5 = level5, OrderBy = orderBy });
		}
	}
}