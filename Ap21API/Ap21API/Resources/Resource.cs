using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Caching;

namespace Ap21API.Resources
{
	/// <summary>
	/// Base class which represents a "resource" in Apparel21, as represented by the Apparel21 API.
	/// </summary>
	/// <typeparam name="T">Class type that the resource represents. e.g. Person, Product, Order etc.</typeparam>
    public abstract class Resource<T>
    {
		/// <summary>
		/// Create a new resource
		/// </summary>
		/// <returns>An instance of the resource T.</returns>
		public static T Create()
		{
			return Resource<T>.CreateInternal(new { }, string.Format("<{0}></{0}>", typeof(T).Name));
		}

		/// <summary>
		/// Create a new resource given an instance of T
		/// </summary>
		/// <param name="source">An instance of T, which will be serialized to XML and passed to the API</param>
		/// <param name="parameters">Parameters used to replace any placeholders in the resource path, or add to the resulting API URL.
		/// e.g. if the resource path is "orders/:orderId", then any key/value pair in parameters whose name is "orderId" will have its value used in the resource path: "/orders/123"
		/// </param>
		/// <returns>An instance of the resource T.</returns>
		protected static T CreateInternal(object parameters, T source)
		{
			return Resource<T>.CreateInternal(parameters, Resource<T>.SerializeToXML(source));
		}

		/// <summary>
		/// Create a new resource
		/// </summary>
		/// <param name="parameters">Parameters to substitute in to the Path property placeholder string.</param>
		/// <param name="payload">Payload to pass to the API</param>
		/// <returns>An instance of the resource T.</returns>
		protected static T CreateInternal(object parameters, string payload)
		{
			string path = "";
			string query = "";
			string resourcePath = Regex.Replace(Resource<T>.GetPathProperty(), @"\:Id", "", RegexOptions.IgnoreCase).Trim('/');		// Remove any :Id place holder, since the ID of this newly created object is not known until it's created
			Resource<T>.ResourcePathAndQuery(parameters, resourcePath, out path, out query);

			string xml = ResourceProvider.Create(path, payload);	
			return Resource<T>.DeserializeFromXML(xml);
		}

		/// <summary>
		/// Delete a resource
		/// </summary>
		/// <param name="parameters">Parameters to substitute in to the Path property placeholder string.</param>
		protected static void DeleteInternal(object parameters)
		{
			string path = "";
			string query = "";

			Resource<T>.ResourcePathAndQuery(parameters, Resource<T>.GetPathProperty(), out path, out query);

			ResourceProvider.Delete(path, query);
		}
		
		/// <summary>
		/// Given an XML representation of a resource, return an instance thereof
		/// </summary>
		/// <param name="xml">XML payload</param>
		/// <returns>Instance of XML resource</returns>
		private static T DeserializeFromXML(string xml)
		{
            XmlSerializer deserializer = GetSerialiser(typeof(T).ToString());
			
			using (StringReader reader = new StringReader(xml))
			{
				return (T)deserializer.Deserialize(reader);
			}
		}

		/// <summary>
		/// Find resource T
		/// </summary>
		/// <returns>Instance of T</returns>
		protected static T FindInternal()
		{
			return Resource<T>.FindInternal(new { });
		}

		/// <summary>
		/// Find resource T matching the given criteria
		/// </summary>
		/// <param name="parameters">Dynamic collection of parameter and value pairs to march when finding. e.g. 'new { Email = "charlie@brown.com", Password = "secret" }'</param>
		/// <returns>Instance of T</returns>
		///	<remarks>The type of parameters should actually be dynamic. It's of type object because of Moles (the mocking library we're using for testing) we're using can't yet support mocking dynamic types.</remarks>
		protected static T FindInternal(object parameters)
        {
			string path = "";
			string query = "";

			Resource<T>.ResourcePathAndQuery(parameters, Resource<T>.GetPathProperty(), out path, out query);

			string xml = ResourceProvider.Get(path, query);

			if (xml != null)
			{
				return Resource<T>.DeserializeFromXML(xml);
			}
			else
			{
				return default(T);
			}
        }

		/// <summary>
		///	Get the resource's Path property.
		/// </summary>
		/// <remarks> Resources need to have a <code>public static string Path</code> getter property with the resource's REST API path.</remarks>
		/// <returns>Path property for the resource</returns>
		private static string GetPathProperty()
		{
			PropertyInfo pathProp = typeof(T).GetProperty("Path");

			if (pathProp != null)
			{
				return pathProp.GetValue(null, null).ToString();
			}
			else
			{
				throw new NotImplementedException("Resource Path has not been implemented");
			}
		}

		/// <summary>
		/// Get the path and query string used to call the REST API
		/// </summary>
		/// <param name="parameters">Any parameters we want to pass to the API</param>
		/// <param name="resourcePath">The path of the resource. Can contain placeholders for substitution from parameters. e.g. orders/:orderId/order_details/:Id</param>
		/// <param name="path">Resource path part of the REST API URI. .e.g. orders/123 when getting order with Id 123 if the Id of this instance is 123. In this example, the resource's Path would be "orders/:Id". This method then looks for a property named Id in the parameters parameter and places it in the path.</param>
		/// <param name="query">The query string used for the resulting REST API URI. Takes all properties from the parameters parameter (excluding any that matched the Path place holders) and combines them as a HTTP query string. e.g. Email=bob@home.com&amp;Age=34 if parameters contained properties named Email and Age.</param>
        private static void ResourcePathAndQuery(object parameters, string resourcePath, out string path, out string query)
        {
			path = "";
			query = "";

			PropertyInfo[] paramProps = parameters.GetType().GetProperties();
			StringBuilder resourceQuery = new StringBuilder();

			foreach (PropertyInfo propInfo in paramProps)
			{
				string propNameTag = string.Format(":{0}", propInfo.Name);
				object propValue = propInfo.GetValue(parameters, null);

				if (resourcePath.IndexOf(propNameTag, StringComparison.OrdinalIgnoreCase) >= 0)
				{
					// Found a property name placeholder in the Path resource string. 
					// Replace the property placeholder in the string with the property's value
					// e.g. "orders/:Id" would become "orders/123" of propNameTag is "Id" and the property's value is 123
					resourcePath = Regex.Replace(resourcePath, propNameTag, propValue.ToString(), RegexOptions.IgnoreCase);
				}
				else if (propValue != null)
				{
					// No match found in the Path string, so add it to the query string parameters: "property_name=property_value"
					if (resourceQuery.Length > 0)
					{
						resourceQuery.AppendFormat("&{0}={1}", propInfo.Name, Uri.EscapeDataString(propValue.ToString()));
					}
					else
					{
						resourceQuery.AppendFormat("{0}={1}", propInfo.Name, Uri.EscapeDataString(propValue.ToString()));
					}
				}
			}

			path = resourcePath;
			query = resourceQuery.ToString();
        }

		/// <summary>
		/// Serialize an object to XML
		/// </summary>
		/// <param name="obj">Object instance to serialize</param>
		/// <returns>XML representation of obj</returns>
		private static string SerializeToXML(object obj)
		{
			var xml = new StringBuilder();

			// Because the XML given to the API must not have the <?xml...> element, we need to do a few more steps when serializing to XML
			using (XmlWriter writer = XmlWriter.Create(xml, new XmlWriterSettings { Indent = false, ConformanceLevel = ConformanceLevel.Auto, OmitXmlDeclaration = true }))
			{
				var ns = new XmlSerializerNamespaces();
                var xs = GetSerialiser(obj.GetType().ToString());
				xs.Serialize(writer, obj, ns);
			}

			return xml.ToString();
		}

		public void Update()
		{
			UpdateInternal(new { });
		}

		/// <summary>
		/// Call the REST API to update the instance of a resource
		/// </summary>
		protected void UpdateInternal(object parameters)
		{
			// Replace anny path placeholders with property values of this resource object
			// e.g. if Path is "product/:Id", and this Resource is a Product with a Property "Id", replace ":Id" with the value of this.Id
			string path = Resource<T>.GetPathProperty();
			string instancePath = path;

			Regex rx = new Regex(@"\:\w*", RegexOptions.IgnoreCase | RegexOptions.Singleline);
			var matches = rx.Matches(path);

			if (matches.Count > 0)
			{
				foreach (var match in matches)
				{
					string matchNamePlaceholder = match.ToString();
					string matchName = matchNamePlaceholder.Substring(1);

					// see if the placeholder is in parameters. if not, check if it's a property of this object
					PropertyInfo matchProp = parameters.GetType().GetProperty(matchName);

					if (matchProp != null)
					{
						instancePath = instancePath.Replace(matchNamePlaceholder, matchProp.GetValue(parameters, null).ToString());
					}
					else {
						matchProp = this.GetType().GetProperty(matchName);

						if (matchProp != null)
						{
							instancePath = instancePath.Replace(matchNamePlaceholder, matchProp.GetValue(this, null).ToString());
						}
					}

				}
			}

			ResourceProvider.Update(instancePath, Resource<T>.SerializeToXML(this));
		}

        private static System.Xml.Serialization.XmlSerializer GetSerialiser(string typeName)
        {
            var cache = (CacheManager)CacheFactory.GetCacheManager();

            if (cache.Contains(typeName))
            {
                return (System.Xml.Serialization.XmlSerializer)cache.GetData(typeName);
            }

            var serialiser = new System.Xml.Serialization.XmlSerializer(typeof(T));
            cache.Add(typeName, serialiser);
            return serialiser;
        }
    }
}
