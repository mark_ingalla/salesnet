﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ap21API.Resources;

namespace Ap21API.Entities.Product
{
    public partial class ProductNoteTypes : Resource<ProductNoteTypes>
    {
       public static string Path
       {
           get
           {
               return "ProductNotes/:id";
           }
       }
       public static ProductNoteTypes Find(string id)
       {
           try
           {
               return FindInternal(new { Id = id });
           }
           catch
           {
               return null;
           }
       }
    }
    public partial class ProductNoteType : Resource<ProductNoteType>
    {
        public static string Path
        {
            get
            {
                return "ProductNotes/:id";
            }
        }
        public static ProductNoteType Find(string id)
        {
            try
            {
                return FindInternal(new { Id = id });
            }
            catch
            {
                return null;
            }
        }
    }
}
