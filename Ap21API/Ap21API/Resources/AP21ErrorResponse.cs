﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ap21API.Resources
{
    [Serializable]
    [XmlRoot("AP21Error")]
    public class AP21ErrorResponse
    {
        [XmlElement(Order = 1)]
        public int ErrorCode { get; set; }
        [XmlElement(Order = 2)]
        public string Description { get; set; }

        public AP21ErrorResponse()  //If the excetpion is not an AP21UserException, it will parse as null
        {
            ErrorCode = 5100;  // We don't have any existing code to handle the exceptions happened on bugs, etc, I'll use this as default for now.
            Description = "Unknow error!";
        }

        public AP21ErrorResponse(AP21APIException e)
        {
            ErrorCode = e.ErrorCode;
            Description = e.Message;
        }
    }
}
