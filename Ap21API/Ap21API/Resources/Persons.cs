﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Ap21API.Resources;

namespace Ap21API.Entities.Person
{
	public partial class Persons : ResourceCollection<Persons, Person>
	{
		public static string Path
		{
			get
			{
				return "persons";
			}
		}

		/// <summary>
		/// Find people that match all of the given search criteria
		/// </summary>
		/// <param name="code">Person code</param>
		/// <param name="email">Email address</param>
		/// <param name="firstName">Person's firstname</param>
		/// <param name="password">Account password</param>
		/// <param name="phone">Phone number</param>
		/// <param name="surname">Person's surname</param>
		/// <returns>Collection of Persons. Returns null is nothing is found.</returns>
		public static Persons Find(string code = null, string email = null, string firstName = null, string password = null, string phone = null, string surname = null)
		{
			return FindInternal(new { Code = code, Email = email, FirstName = firstName, Password = password, Phone = phone, Surname = surname });
		}
	}
}