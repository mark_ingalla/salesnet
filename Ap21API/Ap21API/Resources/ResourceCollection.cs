﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ap21API.Resources
{
	/// <summary>
	/// Class that represents collections of resources in the Apparel21 API
	/// </summary>
	/// <typeparam name="T">Collection class. e.g. Persons, Products, Orders etc.</typeparam>
	/// <typeparam name="R">Class of single instance of the resources in the collection T. e.g. Person, Product, Order etc.</typeparam>
	public class ResourceCollection<T, R> : Resource<T>
	{
		/// <summary>
		/// Access items in the collection via an index
		/// </summary>
		/// <param name="index">Index in to the collection of resources</param>
		/// <returns>Resource at index</returns>
		public R this[int index]
		{
			get
			{
				return (R) GetCollectionPropertyValue("get_Item", new object[] { index } );
			}
		}

		/// <summary>
		/// Number of resources in the collection
		/// </summary>
		public int Count
		{
			get
			{
				return (int) GetCollectionPropertyValue("get_Count");
			}
		}

		// Get the value of a property by calling the method "methodName"
		// Pass any required arguments to methodName via args.
		private object GetCollectionPropertyValue(string methodName, object[] args = null)
		{
			object collectionProperty = typeof(T).GetProperty(typeof(R).Name).GetValue(this, null);
			Type collectionPropertyType = collectionProperty.GetType();

			return collectionPropertyType.InvokeMember(methodName, System.Reflection.BindingFlags.InvokeMethod, null, collectionProperty, args);
		}
	}
}
