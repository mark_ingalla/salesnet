﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Ap21API.Resources;

namespace Ap21API.Entities.Order
{
	public partial class OrderDetail : Resource<OrderDetail>
	{
		public static string Path
		{
			get
			{
				return "orders/:OrderId/orderdetails/:Id";
			}
		}

		public static OrderDetail Create(string orderId, OrderDetail source)
		{
			return CreateInternal(new { OrderId = orderId }, source);
		}

		public static void Delete(string orderId, string id)
		{
			DeleteInternal(new { OrderId = orderId, Id = id });
		}

		public void Update(string orderId)
		{
			UpdateInternal(new { OrderId = orderId });
		}
	}
}