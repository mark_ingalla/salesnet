﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ap21API.Resources;
namespace Ap21API.Entities.WebPageContent
{

    public partial class WebPageContent : Resource<WebPageContent>
    {
        public static string Path
        {
            get
            {
               // return "PageContents/:Id";
                return "PageContents";
            }
        }

        /// <summary>
        /// Find a Person by its Id
        /// </summary>
        /// <param name="orderId">Person Id to find</param>
        /// <returns>Person object</returns>
        public static WebPageContent Find(string id)
        {
            return FindInternal(new { Id = id });
        }
    }
}
