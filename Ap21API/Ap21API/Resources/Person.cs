﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Ap21API.Resources;

namespace Ap21API.Entities.Person
{
    public partial class Person : Resource<Person>
    {
		public static string Path
		{
			get
			{
				return "persons/:Id";
			}
		}

		/// <summary>
		/// Find a Person by its Id
		/// </summary>
		/// <param name="orderId">Person Id to find</param>
		/// <returns>Person object</returns>
        public static Person Find(string id)
        {
            return FindInternal(new { Id = id });
        }
    }
}