﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Ap21API.Resources;

namespace Ap21API.Entities.Product
{
	public abstract partial class ProductBase<T> : Resource<T>
	{
		/// <summary>
		/// Find a product given a product Id
		/// </summary>
		/// <param name="id">Product Id</param>
		/// <returns>Product object. Returns null if no match is found.</returns>
		public static T Find(string id)
		{
			return FindInternal(new { Id = id });
		}
	}
}