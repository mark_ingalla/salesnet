﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Ap21API.Entities.Order
{
    public class OrderClrs
    {
        [XmlElement]
        public OrderClrList Clr { get; set; } 
        [XmlAttribute]
        public int TotalRows { get; set; }
        [XmlAttribute]
        public string Type = "Array";

		public OrderClrs()
		{
			Clr = new OrderClrList();
		}

		public OrderClr this[int index]
		{
			get
			{
				return Clr[index];
			}
		}
    }

    [XmlRoot("Clrs")]
    public class OrderClrList : List<OrderClr>
    {
    }
}
