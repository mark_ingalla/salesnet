﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Ap21API.Entities.Order
{
    public class OrderSKUs
    {
        [XmlElement]
        public OrderSkuList SKU { get; set; }
        [XmlAttribute]
        public int TotalRows { get; set; }
        [XmlAttribute]
        public string Type = "Array";

		public OrderSKUs()
		{
			SKU = new OrderSkuList();
		}

		public OrderSKU this[int index]
		{
			get
			{
				return SKU[index];
			}
		}
    }

    [XmlRoot("SKUs")]
    public class OrderSkuList : List<OrderSKU>
    {
    }
}
