﻿namespace Ap21API.Entities.Order
{
    public class Address
    {
        public string Id { get; set; }
        public string AddressType { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Postcode { get; set; }
        public string State { get; set; }

        public bool IsEmpty()
        {
            return string.IsNullOrEmpty(Id) &
                   string.IsNullOrEmpty(AddressType) &
                   string.IsNullOrEmpty(AddressLine1) &
                   string.IsNullOrEmpty(AddressLine2) &
                   string.IsNullOrEmpty(City) &
                   string.IsNullOrEmpty(Country) &
                   string.IsNullOrEmpty(Postcode) & 
                   string.IsNullOrEmpty(State);
        }
    }

}
