﻿using System;
using System.Collections.Generic;

namespace Ap21API.Entities.Order
{
    public class OrderClr
    {
		public string DueDateFrom { get; set; }
		public string DueDateTo { get; set; }
        public string Id { get; set; }
        public string ClrId { get; set; }
        public string ClrCode { get; set; }
        public string ClrName { get; set; }
        public string Sequence { get; set; }
        public string OrderState { get; set; }
        public decimal DiscountPercent { get; set; }
        public decimal TaxPercent { get; set; }
        public Matric Ordered { get; set; }
        public Matric Outstanding { get; set; }
        public Matric Invoiced { get; set; }
        public OrderSKUs SKUs { get; set; }

		public OrderClr()
		{
			Invoiced = new Matric();
			Ordered = new Matric();
			Outstanding = new Matric();
			SKUs = new OrderSKUs();
		}
    }

    public class ClrSequenceComparer : IComparer<OrderClr>
    {
        public int Compare(OrderClr x, OrderClr y)
        {
            int xSeq = 0;
            int ySeq = 0;

            try
            {
                xSeq = int.Parse(x.Sequence);
                ySeq = int.Parse(y.Sequence);
            }
            catch (InvalidCastException ex)
            {
                var error = ex;
                return x.Sequence.CompareTo(y.Sequence);
            }

            return xSeq.CompareTo(ySeq);
        }
    }
}
