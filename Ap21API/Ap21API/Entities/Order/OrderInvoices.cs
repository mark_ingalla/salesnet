﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Ap21API.Entities.Order
{
    public class OrderInvoices
    {
        [XmlElement]
        public OrderInvoiceList Invoice { get; set; }
        [XmlAttribute]
        public int TotalRows { get; set; }
        [XmlAttribute]
        public string Type = "Array";

		public OrderInvoices()
		{
			Invoice = new OrderInvoiceList();
		}
    }

    [XmlRoot("Invoices")]
    public class OrderInvoiceList : List<OrderInvoice>
    {
    }
}
