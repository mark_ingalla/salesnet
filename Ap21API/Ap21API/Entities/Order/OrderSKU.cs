﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Ap21API.Entities.Order
{
    public class OrderSKU
    {
        public string Id { get; set; }
        public Matric Invoiced { get; set; }
        public Matric Ordered { get; set; }
        public Matric Outstanding { get; set; }
        public decimal Price { get; set; }
		[XmlElement]
		[System.ComponentModel.DefaultValue(0)]
		public long Sequence { get; set; }
        public string SizeCode { get; set; }
        public string SkuId { get; set; }

		public OrderSKU()
		{
			Invoiced = new Matric();
			Ordered = new Matric();
			Outstanding = new Matric();
		}

        public override int GetHashCode()
        {
            return SizeCode.GetHashCode();
        }
    }

    public class SkuSizeCodeComparer : IEqualityComparer<OrderSKU>
    {
        public bool Equals(OrderSKU x, OrderSKU y)
        {
            return x.SizeCode.Equals(y.SizeCode);
        }

        public int GetHashCode(OrderSKU sku)
        {
            return sku.GetHashCode();
        }
    }

    public class SkuSequenceComparer : IComparer<OrderSKU>
    {
        public int Compare(OrderSKU x, OrderSKU y)
        {
            long xSeq = 0;
            long ySeq = 0;

            try
            {
                xSeq = x.Sequence;
                ySeq = y.Sequence;
            }
            catch
			{
                return x.Sequence.CompareTo(y.Sequence);
            }

            return xSeq.CompareTo(ySeq);
        }
    }
}
