﻿namespace Ap21API.Entities.Order
{
    public class Addresses
    {
        public Address Billing { get; set; }
        public Address Delivery { get; set; }

		public Addresses()
		{
			Billing = new Address();
			Delivery = new Address();
		}
    }
}
