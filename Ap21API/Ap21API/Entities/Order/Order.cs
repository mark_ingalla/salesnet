﻿namespace Ap21API.Entities.Order 
{    
    public partial class Order 
    {
        public Addresses Addresses { get; set; }
        public string CarrierCode { get; set; }
        public string CarrierId { get; set; }
        public string CarrierName { get; set; }
        public string ConNote { get; set; }
        public string CreatedDate { get; set; }
        public string CustomerId { get; set; }
        public string CustomerReference { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string DeliveryInstructions { get; set; }
		public string DueDateFrom { get; set; }
		public string DueDateTo { get; set; }
        public string Id { get; set; }
        public Matric Invoiced { get; set; }
        public OrderInvoices Invoices { get; set; }
        public InvoiceOrders FromOrders { get; set; }
        public decimal Lines { get; set; }
        public LinkUri Link { get; set; }
        public OrderDetails OrderDetails { get; set; }
        public Matric Ordered { get; set; }
        public string OrderState { get; set; }
        public Matric Outstanding { get; set; }
        public int RowNumber { get; set; }
        public string SpecialInstructions { get; set; }
        public string SupplyFromWarehouseCode { get; set; }
        public string SupplyFromWarehouseId { get; set; }
        public string SupplyFromWarehouseName { get; set; }
        public string TransactionType { get; set; }
        public string TransNumber { get; set; }
        public Currency Currency { get; set; }

        //added by raju 14 dec 2012

        public string CarrierUrl { get; set; }

		public Order()
		{
			Addresses = new Addresses();
			Invoiced = new Matric();
			Invoices = new OrderInvoices();
            FromOrders = new InvoiceOrders();
			Link = new LinkUri();
			OrderDetails = new OrderDetails();
			Ordered = new Matric();
			Outstanding = new Matric();
            Currency = new Currency();
		}
    }
}
