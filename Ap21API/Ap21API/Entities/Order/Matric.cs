﻿namespace Ap21API.Entities.Order
{
    public class Matric
    {
        public decimal Discount { get; set; }
        public decimal Gross { get; set; }
        public decimal Net { get; set; }
        public decimal Quantity { get; set; }
        public decimal Tax { get; set; }
        public decimal Value { get; set; }
    }
}
