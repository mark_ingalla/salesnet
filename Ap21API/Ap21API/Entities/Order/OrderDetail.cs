﻿namespace Ap21API.Entities.Order
{
    public partial class OrderDetail
    {
        public OrderClrs Clrs { get; set; }
        public string Id { get; set; }
        public Matric Invoiced { get; set; }
        public Matric Ordered { get; set; }
        public Matric Outstanding { get; set; }
        public string ProductCode { get; set; }
        public string ProductDescription { get; set; }
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public string Sequence { get; set; }

		public OrderDetail()
		{
			Clrs = new OrderClrs();
			Invoiced = new Matric();
			Ordered = new Matric();
			Outstanding = new Matric();
		}
    }
}
