﻿using System.Xml.Serialization;

namespace Ap21API.Entities.Order
{
    public class LinkUri
    {
        [XmlAttribute]
        public string Rel = "Self";
        [XmlTextAttribute]
        public string Link { get; set; }
    }
}
