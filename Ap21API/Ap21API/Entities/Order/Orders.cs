﻿using System.Xml.Serialization;
using System.Collections.Generic;

namespace Ap21API.Entities.Order
{
    public partial class Orders
    {
        [XmlElement]
        public OrderList Order { get; set; } 
        [XmlAttribute]
        public int PageRows { get; set; }
        [XmlAttribute]
        public int PageStartRow { get; set; }
        [XmlAttribute]
        public int TotalRows { get; set; }
        [XmlAttribute]
        public string Type = "Array";

		public Orders()
		{
			Order = new OrderList();
		}
    }

    [XmlRoot("Orders")]
    public class OrderList : List<Order>
    {
    }
}
