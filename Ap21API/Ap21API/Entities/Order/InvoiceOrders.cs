﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Ap21API.Entities.Order
{
    public class InvoiceOrders
    {
        [XmlElement]
        public InvoiceOrderList FromOrder { get; set; }
        [XmlAttribute]
        public int TotalRows { get; set; }
        [XmlAttribute]
        public string Type = "Array";

        public InvoiceOrders()
        {
            FromOrder = new InvoiceOrderList();
        }
    }

    [XmlRoot("FromOrders")]
    public class InvoiceOrderList : List<InvoiceOrder>
    {
    }
}
