﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Ap21API.Entities.Order
{
    public class OrderDetails
    {
        [XmlElement]
        public OrderDetailList OrderDetail { get; set; }
        [XmlAttribute]
        public int TotalRows { get; set; }
        [XmlAttribute]
        public string Type = "Array";

		public OrderDetails()
		{
			OrderDetail = new OrderDetailList();
		}

		public OrderDetail this[int index]
		{
			get
			{
				return OrderDetail[index];
			}
		}
    }

    public class OrderDetailList : List<OrderDetail>
    {
    }
}
