﻿namespace Ap21API.Entities.Order
{
    public class OrderInvoice
    {
        public string CreatedDate { get; set; }
        public decimal Discount { get; set; }
        public decimal Gross { get; set; }
        public string Id { get; set; }
        public LinkUri Link { get; set; }
        public decimal Net { get; set; }
        public decimal Quantity { get; set; }
        public int RowNumber { get; set; }
        public decimal Tax { get; set; }
        public string TransNumber { get; set; }
        public decimal Value { get; set; }

		public OrderInvoice()
		{
			Link = new LinkUri();
		}
    }
}
