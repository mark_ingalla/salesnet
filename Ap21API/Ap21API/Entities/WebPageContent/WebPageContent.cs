﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Ap21API.Entities.WebPageContent
{
    public partial class WebPageContent
    {

        public string Code { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }

        
        //added by raju 14 dec 2012
        public WebPageContent()
		{
		   
		}
    }
}
