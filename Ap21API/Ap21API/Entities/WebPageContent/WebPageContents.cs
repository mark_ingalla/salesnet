﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using System.Text;

namespace Ap21API.Entities.WebPageContent
{
    public partial class WebPageContents
    {
        [XmlElement]
        public WebPageContentList WebPageContent { get; set; }

        //[XmlAttribute]
        //public int PageRows { get; set; }
        //[XmlAttribute]
        //public int PageStartRow { get; set; }
        //[XmlAttribute]
        //public int TotalRows { get; set; }
        //[XmlAttribute]
        //public string Type = "Array";

        public WebPageContents()
		{
            WebPageContent = new WebPageContentList();
		}
    }
    [XmlRoot("WebPageContents")]
    public class WebPageContentList : List<WebPageContent>
    {
    }
}
