﻿

namespace Ap21API.Entities.Customer
{
    public partial class Customer
    {
        public string Code { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public int RowNumber { get; set; }
        public bool AnyOutstandingOrders { get; set; }
        public int CreditStatusFlag { get; set; }
        public string CreditStatus { get; set; }
        public Locations Locations { get; set; }
    }
}
