﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Ap21API.Entities.Customer
{
    public partial class Customers
    {
        [XmlElement]
        public CustomerList Customer { get; set; }
        [XmlAttribute]
        public int PageRows { get; set; }
        [XmlAttribute]
        public int PageStartRow { get; set; }
        [XmlAttribute]
        public int TotalRows { get; set; }
        [XmlAttribute]
        public string Type = "Array";

        public Customers()
        {
            Customer = new CustomerList();
        }
    }

    [XmlRoot("Customers")]
    public class CustomerList : List<Customer>
    {
    }
}
