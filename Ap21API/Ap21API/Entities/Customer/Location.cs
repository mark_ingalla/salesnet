﻿using System.Collections.Generic;
using System.Xml.Serialization;
using Ap21API.Entities.Order;

namespace Ap21API.Entities.Customer
{
    public class Locations
    {
        [XmlAttribute]
        public string Type = "Array";

        [XmlElement(ElementName = "Location", Type = typeof(Address))]
        public List<Address> Location { get; set; } 

        public Locations()
        {
            Location = new List<Address>();
        }
    }
}
