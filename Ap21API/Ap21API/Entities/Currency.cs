﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ap21API.Entities
{
    public class Currency
    {
        public string Code { get; set; }
        public string Format { get; set; }
    }

}
