﻿using System.Xml.Serialization;

namespace Ap21API.Entities.Reference
{
    [XmlRoot("Reference")]
    public class Reference
    {
        public Reference[] Children { get; set; }
        public string Level { get; set; }
        public string LevelProductQuery { get; set; }
        public string NodeId { get; set; }
        public string ProductQuery { get; set; }
        public string ProductQueryNames { get; set; }
        public string ReferenceCode { get; set; }
        public string ReferenceId { get; set; }
        public string ReferenceName { get; set; }
        public string ReferenceTypeId { get; set; }
    }
}
