﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Ap21API.Entities.Reference 
{
    public partial class ReferenceTree 
    {
        [XmlElement]
        public ReferenceList Reference { get; set; }

		public ReferenceTree()
		{
            Reference = new ReferenceList();
		}
    }

    public class ReferenceList : List<Reference>
    {
    }
}
