﻿
using System.Xml.Serialization;

namespace Ap21API.Entities.Reference
{
    public partial class ReferenceType
    {
        [XmlElement]
        public string Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
