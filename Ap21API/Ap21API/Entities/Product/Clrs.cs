﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Ap21API.Entities.Product
{
    public class Clrs
    {
        [XmlElement]
        public ClrList Clr { get; set; }
        [XmlAttribute]
        public string Type = "Array";

		public Clrs()
		{
			Clr = new ClrList();
		}
    }

    public class ClrList : List<Clr>
    {
    }
}
