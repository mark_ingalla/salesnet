﻿namespace Ap21API.Entities.Product 
{
    public partial class Product 
    {
        public Clrs Clrs { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string RrpText { get; set; }
        public ObjectReferences References { get; set; }
        public int RowNumber { get; set; }

		public Product()
		{
			Clrs = new Clrs();
			References = new ObjectReferences();
		}
    }    
}
