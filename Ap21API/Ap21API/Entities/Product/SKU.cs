﻿using System.Xml.Serialization;
namespace Ap21API.Entities.Product
{
    public class SKU
    {
        public string ClrId { get; set; }
        public int FreeStock { get; set; }
        public int GlobalSequence { get; set; }
        public string Id { get; set; }
		public string NextAvailable { get; set; }
        public decimal OriginalPrice { get; set; }
        public decimal Price { get; set; }
        public decimal RetailPrice { get; set; }

		[XmlElement]
		[System.ComponentModel.DefaultValue(0)]
        public long Sequence { get; set; }
        public string SizeCode { get; set; }
    }
}
