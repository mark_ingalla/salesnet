﻿namespace Ap21API.Entities.Product
{
    public class Clr
    {
        public string Code { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string ProductId { get; set; }
        public string Sequence { get; set; }
        public SKUs SKUs { get; set; }
        public string TypeCode { get; set; }
        public string TypeName { get; set; }

		public Clr()
		{
			SKUs = new SKUs();
		}
    }
}
