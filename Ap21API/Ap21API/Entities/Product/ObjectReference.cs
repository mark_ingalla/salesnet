﻿using System.Collections.Generic;

namespace Ap21API.Entities.Product
{
  public class ObjectReference
  {
    public string Id { get; set; }
    public string ReferenceTypeId { get; set; }
  }

  public class ObjectReferences : List<ObjectReference>
  {
  }
}
