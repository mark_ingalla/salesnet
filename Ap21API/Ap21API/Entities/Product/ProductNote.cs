﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Ap21API.Entities.Product
{
    public partial class ProductNoteTypes
    {

        [XmlElement]
        public ProductNoteTypeList ProductNoteType { get; set; }


        public ProductNoteTypes()
        {
            ProductNoteType = new ProductNoteTypeList();
        }
        
    }
    [XmlRoot("ProductNoteTypes")]
    public class ProductNoteTypeList : List<ProductNoteType>
    {
    }

    public partial class ProductNoteType
    {
        
        public string Code { get; set; }
        public string Note { get; set; }
        public string Name { get; set; }
        public ProductNoteType()
		{
			
		}
        
    }
}
