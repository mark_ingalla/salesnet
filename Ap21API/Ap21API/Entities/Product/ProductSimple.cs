﻿namespace Ap21API.Entities.Product 
{
    public partial class ProductSimple
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public decimal OriginalPriceFrom { get; set; }
        public decimal OriginalPriceTo { get; set; }
        public decimal PriceFrom { get; set; }
        public decimal PriceTo { get; set; }
        public int RowNumber { get; set; }
        public int AvailableStock { get; set; }
    }

}
