﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Ap21API.Entities.Product
{
    public class SKUs
    {
        [XmlElement]
        public SKUList SKU { get; set; }
        [XmlAttribute]
        public string Type = "Array";

		public SKUs()
		{
			SKU = new SKUList();
		}
    }

    public class SKUList : List<SKU>
    {
    }
}
