﻿using System.Xml.Serialization;
using System.Collections.Generic;

namespace Ap21API.Entities.Product
{
	public partial class Products
	{
		[XmlElement]
		public ProductList Product { get; set; }
		[XmlAttribute]
		public int PageRows { get; set; }
		[XmlAttribute]
		public int PageStartRow { get; set; }
		[XmlAttribute]
		public int TotalRows { get; set; }
		[XmlAttribute]
		public string Type = "Array";

		public Products()
		{
			Product = new ProductList();
		}
	}

	[XmlRoot("Products")]
	public class ProductList : List<Product>
	{
	}
}
