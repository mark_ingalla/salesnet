﻿using System.Xml.Serialization;
using System.Collections.Generic;

namespace Ap21API.Entities.Product
{
	public partial class ProductsSimple
	{
		[XmlElement]
		public ProductSimpleList ProductSimple { get; set; }
		[XmlAttribute]
		public int PageRows { get; set; }
		[XmlAttribute]
		public int PageStartRow { get; set; }
		[XmlAttribute]
		public int TotalRows { get; set; }
		[XmlAttribute]
		public string Type = "Array";

		public ProductsSimple()
		{
			ProductSimple = new ProductSimpleList();
		}
	}

	[XmlRoot("ProductsSimple")]
	public class ProductSimpleList : List<ProductSimple>
	{
	}
}
