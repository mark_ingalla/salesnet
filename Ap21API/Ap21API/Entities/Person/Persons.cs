﻿using System.Xml.Serialization;
using System.Collections.Generic;

namespace Ap21API.Entities.Person
{
    public partial class Persons
    {
        [XmlElement]
        public PersonList Person { get; set; }
        [XmlAttribute]
        public int PageRows { get; set; }
        [XmlAttribute]
        public int PageStartRow { get; set; }
        [XmlAttribute]
        public int TotalRows { get; set; }
        [XmlAttribute]
        public string Type = "Array";

		public Persons()
		{
			Person = new PersonList();
		}
    }

    [XmlRoot("Persons")]
    public class PersonList : List<Person>
    {
    }
}
