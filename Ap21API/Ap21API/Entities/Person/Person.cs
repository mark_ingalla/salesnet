﻿using System.Xml.Serialization;

using Ap21API.Entities.Order;
using System.ComponentModel;

namespace Ap21API.Entities.Person {  
    public partial class Person
    {
        public Addresses Addresses { get; set; }
        public string Code { get; set; }
        public Contacts Contacts { get; set; }
		public string DateOfBirth { get; set; }
        public string Firstname { get; set; }
        public string Id { get; set; }
        public string Initials { get; set; }
        public string JobTitle { get; set; }
        public string Password { get; set; }
        public string Sex { get; set; }
		public string StartDate { get; set; }
        public string Surname { get; set; }
        public string Title { get; set; }
		public string UpdateTimeStamp { get; set; }
        public Currency Currency { get; set; }
        public bool IsAgent { get; set; }

		public Person()
		{
			Addresses = new Addresses();
			Contacts = new Contacts();
            Currency = new Currency();
		}
    }
 }
