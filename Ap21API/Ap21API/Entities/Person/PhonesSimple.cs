﻿namespace Ap21API.Entities.Person
{
    public class PhonesSimple
    {
        public string Home { get; set; }
        public string Mobile { get; set; }
        public string Work { get; set; }
    }
}
