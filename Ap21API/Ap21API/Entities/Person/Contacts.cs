﻿namespace Ap21API.Entities.Person
{
    public class Contacts
    {
        public string Email { get; set; }
        public PhonesSimple Phones { get; set; }

		public Contacts()
		{
			Phones = new PhonesSimple();
		}
    }
}
