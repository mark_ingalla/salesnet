﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ap21API.Entities.Product;
using Ap21API.Entities.Order;
using SalesNet.WebUI.Models;
using System.Text.RegularExpressions;

namespace SalesNet.WebUI.Helpers
{
    public static class ColorSizeHelper
    {
        public static ColorSizeUnit GetProductItemCell(this ColorUnit clr, string sizeCode)
        {
            var sizeCollection = (from sku in clr.SkuList
                                  where sku.Size == sizeCode
                                  select sku).ToList();
            if (sizeCollection.Count == 0)
            {
                return null;
            }
            else
            {
                return sizeCollection[0];
            }
        }

        public static List<string> AvailableSize(this Clrs clrs)
        {
            return (from clr in clrs.Clr
                    from sku in clr.SKUs.SKU
                    orderby sku.Sequence ascending
                    select (sku.SizeCode == "-" ? "Each" : sku.SizeCode)).Distinct().ToList();
        }
    }
}