﻿using System.Text;
using System.Web;
using System.Web.Mvc;

using Ap21API.Entities.Order;

namespace SalesNet.WebUI.Helpers
{
    public static class AddressHelper
    {
        public static HtmlString AddressContext(this HtmlHelper helper, Address address)
        {
            StringBuilder result = new StringBuilder();
            result.Append(address.AddressLine1);
            result.Append("&nbsp;"); //result.Append("<br>");
            result.Append(address.AddressLine2);
            result.Append("<br>");
            result.Append(address.City);
            result.Append("&nbsp;"); //result.Append("<br>");
            result.Append(address.State);
            result.Append("&nbsp;"); // result.Append("<br>");
            result.Append(address.Postcode);
            result.Append("&nbsp;"); // result.Append("<br>");
            return MvcHtmlString.Create(result.ToString());
        }
    }
}