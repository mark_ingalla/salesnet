﻿using System.Configuration;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace SalesNet.WebUI.Helpers
{
    /// <summary>
    /// Provide helper methods which are general to the application
    /// </summary>
    public static class ApplicationHelper
    {
        private static readonly string[] FlashTypes = { "error", "warning", "notice" };

        /// <summary>
        /// Output JavaScript which shows any pending flash messages we need to show
        /// </summary>
        /// <param name="helper">Helper class to extend</param>
        /// <returns>Returns JavaScript which will show a flash message, if one has been set in TempData.</returns>
        public static HtmlString ShowFlash(this HtmlHelper helper)
        {
            string script = "";

            foreach (string type in FlashTypes)
            {
                dynamic flash = helper.ViewContext.TempData[type];

                if (flash == null) continue;
                script = string.Format("$(window).trigger('show_flash_message', {{message: '{0}', description: '{1}', type: '{2}'}});",
                                     MvcHtmlString.Create(flash.Message),
                                     MvcHtmlString.Create(flash.Description.ToString()),
                                     type);
                break;
            }

            return helper.JavaScript(script);
        }


        public static MvcHtmlString ActionLinkToModal(
              this HtmlHelper html,
              string partialName,
              string linkText, string controllerName, string actionName)
        {
            var link = html.ActionLink(
                linkText,
                actionName,
                controllerName,
                new { path = partialName },
                new { id = partialName + "_colorbox", @class = "colorbox" });

            return link;
        }

        public static HtmlString GetSalesNetVersion(this HtmlHelper html)
        {
            return new HtmlString(ConfigurationManager.AppSettings["SalesNet_Version"]);
        }
    }
}