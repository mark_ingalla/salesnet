﻿using System;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Text.RegularExpressions;

namespace SalesNet.WebUI.Helpers
{
    public static class TextFormatHelper
    {
        public static HtmlString GetLineBreakContext(this HtmlHelper helper, string text, string delimiter)
        {
            StringBuilder result = new StringBuilder();

            string[] textCollection = Regex.Split(text, delimiter);

            foreach (string textLine in textCollection)
            {
                result.Append(textLine);
                result.Append("<br>");
            }

            return MvcHtmlString.Create(result.ToString());
        }

        public static string FormatCurrency(decimal? amount, string format)
        {
            amount = amount ?? 0;
            if (!String.IsNullOrEmpty(format))
            {
                return String.Format("{0:" + format + "}", amount);
            }

            return String.Format("{0:f2}", amount);
        }

        public static string GetDeimalValFromString(string str)
        {
            char[] refArray = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.' };
            char[] inputArray = str.ToCharArray();
            string ext = string.Empty;
            foreach (char item in inputArray)
            {
                for (int count = 0; count < refArray.Length; count++)
                {
                    if (refArray[count]==item)
                    {
                        if (item == '.' && ext.Equals('.') == true)
                        {
                        }
                        else
                        {
                            ext += item.ToString();
                        }
                        break;
                    }
                }
            }
            return ext;
        }

        public static string GetRequiredString(string str)
        {
            char[] refArray = { 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T', 'U','V','W','X','Y','Z','$',};
            char[] inputArray = str.ToCharArray();
            string ext = string.Empty;
            foreach (char item in inputArray)
            {
                for (int count = 0; count < refArray.Length; count++)
                {
                    if (refArray[count] == item)
                    {
                        ext += item.ToString();
                        
                        break;
                    }
                }
            }
            return ext;
        }

        // Format a date
        public static HtmlString FormatDateTime(DateTime date, string format)
        {
            return MvcHtmlString.Create(date.ToString(format));
        }

        // Take a string formatted date in the model & reformat it as a user friendly date.
        public static HtmlString FormatDateTime(this HtmlHelper helper, string date, string format)
        {
            DateTime result;
            if (DateTime.TryParse(date, out result))
            {
                return FormatDateTime(result, format);
            }

            return MvcHtmlString.Create(String.Empty);
        }

        public static HtmlString FormatCurrency(this HtmlHelper helper, decimal? amount, string format)
        {
            return MvcHtmlString.Create(FormatCurrency(amount, format));
        }

        public static HtmlString GetItemUnit(this HtmlHelper helper, decimal? amount, string text)
        {
            amount = amount ?? 0;

            if (amount > 1)
                return MvcHtmlString.Create(amount + " " + text + "s");

            return MvcHtmlString.Create(amount + " " + text);
        }

        public static HtmlString ConvertToPercentage(this HtmlHelper helper, decimal? amount)
        {
            if (amount != null)
                return MvcHtmlString.Create(amount * 100 + "%");

            return MvcHtmlString.Create("0%");
        }
    }
}