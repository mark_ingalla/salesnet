﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using SalesNet.WebUI.Classes;

namespace SalesNet.WebUI.Helpers
{
    public static class ProductMenuHelper
    {
        private static string targetAction = "ProductList";
        private static string fromAction = "ProductMenu";

        public static HtmlString GetProductMenuLiId(this HtmlHelper helper, string id)
        {
            return MvcHtmlString.Create("nav_" + id);
        }

        public static HtmlString GetProductMenuDivId(this HtmlHelper helper, string id)
        {
            return MvcHtmlString.Create("nav_" + id + "_item");
        }

        public static HtmlString GetProductMenuLevel1Link(this HtmlHelper helper, string updateTarget, string refname, string category_season)
        {
            var virtualPathData = RouteTable.Routes.GetVirtualPath(helper.ViewContext.RequestContext, "Default", new RouteValueDictionary());

            if (virtualPathData != null)
                return MvcHtmlString.Create("<a style=\"font-weight:bold;\" data-ajax=\"true\" data-ajax-mode=\"replace\" data-ajax-update=\"" +
                                      updateTarget + "\" href=\"" + virtualPathData.VirtualPath.Replace(fromAction, targetAction) + "?category_season=" +
                                      category_season + "&category_name=" + refname + "\" data-remote=\"true\">" + refname + "</a>");

            return MvcHtmlString.Create(String.Empty);
        }

        public static HtmlString GetProductMenuLevel2Link(this HtmlHelper helper, string updateTarget, string refname, string category_season, string category_type,string category_name)
        {
            var virtualPathData = RouteTable.Routes.GetVirtualPath(helper.ViewContext.RequestContext, "Default", new RouteValueDictionary());

            if (virtualPathData != null)//style="background-color:#F0F8FF;height:42px;width:100%;"
                return MvcHtmlString.Create("<a style=\"height:auto;padding-top:5px;color:#525252;margin-left:10px;\" data-ajax=\"true\" data-ajax-mode=\"replace\" data-ajax-update=\"" +
                                                  updateTarget + "\" href=\"" + virtualPathData.VirtualPath.Replace(fromAction, targetAction) + "?category_season=" +
                                                  category_season + "&category_type=" + category_type + "&category_name=" + category_name + "\" data-remote=\"true\">" + refname + "</a>");

            return MvcHtmlString.Create(String.Empty);
        }

        public static HtmlString GetProductMenuLevel3Link(this HtmlHelper helper, string updateTarget, string refname, string category_season, string category_type, string category_product, string category_name)
        {
            var virtualPathData = RouteTable.Routes.GetVirtualPath(helper.ViewContext.RequestContext, "Default", new RouteValueDictionary());

            if (virtualPathData != null)
                return MvcHtmlString.Create("<a style=\"color:#007acc;font-weight:bold;margin-left:10px;\"data-ajax=\"true\" data-ajax-mode=\"replace\" data-ajax-update=\"" +
                                                   updateTarget + "\" href=\"" + virtualPathData.VirtualPath.Replace(fromAction, targetAction) + "?category_season=" + category_season + "&category_type=" +
                                                   category_type + "&category_product=" +
                                                   category_product + "&category_name=" + category_name + "\" data-remote=\"true\">" + refname + "</a>");

            return MvcHtmlString.Create(String.Empty);
        }

        public static bool IsPriceDifferentBySku(this HtmlHelper helper, Ap21API.Entities.Product.ProductSimple product)
        {
            return product.PriceFrom != product.PriceTo;
        }

        public static bool IsPriceDifferentBySku(this HtmlHelper helper, Models.OrderDetailProductModel product)
        {
            return product.ProductPriceFrom != product.ProductPriceTo;
        }

        public static bool IsRRPPriceDifferentBySku(this HtmlHelper helper, Models.OrderDetailProductModel product)
        {
            return product.RRPPriceFrom != product.RRPPriceTo;
        }
    }
}