﻿using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Configuration;
using Ninject.Activation;

namespace SalesNet.WebUI.Helpers
{
    /// <summary>
    /// Provide helper methods relating to IMG tags
    /// </summary>
    public static class ImageHelper
    {
        /// <summary>
        /// Output an IMG tag
        /// </summary>
        /// <param name="helper">Class to extend</param>
        /// <param name="url">URL or path of the image</param>
        /// <returns>IMG tag string</returns>
        public static HtmlString Image(this HtmlHelper helper, string url)
        {
            return Image(helper, url, null);
        }

        /// <summary>
        /// Output an IMG tag
        /// </summary>
        /// <param name="helper">Class to extend</param>
        /// <param name="url">URL or path of the image</param>
        /// <param name="htmlAttributes">HTML attributes to append</param>
        /// <returns>IMG tag string</returns>
        public static HtmlString Image(this HtmlHelper helper, string url, object htmlAttributes = null)
        {
            var builder = new TagBuilder("img");

            builder.MergeAttribute("src", url);
            if (htmlAttributes != null)
            {
                builder.MergeAttributes(new RouteValueDictionary(htmlAttributes));
            }

            return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing));
        }

        /// <summary>
        /// Return an image tag base on ID 
        /// </summary>
        /// <param name="helper">Class to extend</param>
        /// <param name="url">URL or path for the anchor tag</param>
        /// <param name="imageUrl">URL or path of the image</param>
        /// <param name="htmlAttributes">HTML attributes to append</param>
        /// <returns>Anchor tag containing an IMG tag</returns>
        public static HtmlString LinkToImage(this HtmlHelper helper, string url, string imageUrl, object htmlAttributes = null)
        {
            var builder = new TagBuilder("a");

            builder.MergeAttribute("href", url);
            if (htmlAttributes != null)
            {
                builder.MergeAttributes(new RouteValueDictionary(htmlAttributes));
            }

            builder.InnerHtml = helper.Image(imageUrl).ToString();

            return MvcHtmlString.Create(builder.ToString());
        }

        /// <summary>
        /// Output an Thumb IMG tag base on ID passed 
        /// </summary>
        /// <param name="helper">Class to extend</param>
        /// <param name="id">id for the Thumb IMG tag</param>
        /// <param name="htmlAttributes">HTML attributes to append</param>
        /// <returns>IMG tag string</returns>
        public static HtmlString IdToThumbImage(this HtmlHelper helper, string id, object htmlAttributes = null)
        {
            var builder = new TagBuilder("img");
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);

            builder.MergeAttribute("src", urlHelper.Content(string.Format("{0}/{1}_thumb.jpg", ConfigurationManager.AppSettings["ThumbnailImageLocation"], id)));

            if (htmlAttributes != null)
            {
                builder.MergeAttributes(new RouteValueDictionary(htmlAttributes));
            }

            return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing));
        }

        /// <summary>
        /// Output an Thumb IMG tag base on ID passed 
        /// </summary>
        /// <param name="helper">Class to extend</param>
        /// <param name="id">id for the Thumb IMG tag</param>
        /// <param name="htmlAttributes">HTML attributes to append</param>
        /// <returns>IMG tag string</returns>
        public static HtmlString IdToProductImage(this HtmlHelper helper, string id, object htmlAttributes = null)
        {
            var builder = new TagBuilder("img");

            builder.MergeAttribute("src", string.Format("{0}/{1}.jpg", ConfigurationManager.AppSettings["ThumbnailImageLocation"], id));
            if (htmlAttributes != null)
            {
                builder.MergeAttributes(new RouteValueDictionary(htmlAttributes));
            }

            return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing));
        }


        public static HtmlString GetProductThumbImage(this HtmlHelper helper, string productCode)
        {
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);

            if (File.Exists(HttpContext.Current.Request.MapPath("~/Content/images/" + productCode + "_thumb.jpg")))
            {
                return MvcHtmlString.Create("<img  src=\"" + urlHelper.Content("~/Content/images/" + productCode + "_thumb.jpg") + "\"" + "/>");
            }
            return MvcHtmlString.Create("<img src=\"" + urlHelper.Content("~/Content/images/product_thumb.png") + "\"" + "/>");
        }

        public static HtmlString GetProductImage(this HtmlHelper helper, string productCode)
        {
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);

            if (File.Exists(HttpContext.Current.Request.MapPath("~/Content/images/" + productCode + ".jpg")))
            {

                return MvcHtmlString.Create("<img style=\" max-width:150px;max-height:150px;width:auto;height:auto;\"  src=\"" + urlHelper.Content("~/Content/images/" + productCode + ".jpg") + "\"" + "/> ");
            }
            return MvcHtmlString.Create("<img  src=\"" + urlHelper.Content("~/Content/images/product.png") + "\"" + "/>");
        }

        public static HtmlString GetCloseImage_SideBar(this HtmlHelper helper)
        {
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);

            return MvcHtmlString.Create("<img  class=\"close_sidebar\" src=\"" + urlHelper.Content("~/Content/images/close-window-icon-hover.png") + "\"" + "/>");
        }

        public static HtmlString GetCloseImage_Menu(this HtmlHelper helper)
        {
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);

            return MvcHtmlString.Create("<img  class=\"close_Menu\" src=\"" + urlHelper.Content("~/Content/images/close-window-icon-hover.png") + "\"" + "/>");
        }
    }
}
