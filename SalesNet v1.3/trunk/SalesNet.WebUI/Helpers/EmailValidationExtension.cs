﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;

namespace SalesNet.WebUI.Helpers
{
    public static class EmailValidationExtension
    {
        public static bool IsValidEmail(this string email)
        {
            return Regex.Match(email, @"\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b", RegexOptions.IgnoreCase).Success;
        }
    }
}