﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using SalesNet.WebUI.Classes;
using Ap21API.Entities.Order;
using SalesNet.WebUI.DataSetGenerators;
using System.Web.Script.Serialization;

namespace SalesNet.WebUI.Helpers
{
    public static class CurrentOrderHelper
    {
        private const string fromAddToOrderAction = "AddToOrder";
        private const string targetDeleteFinaliseAction = "DeleteOrdersFromFinalisePage";
        private const string targetRemoveAction = "RemoveOrder";
        private const string fromAction = "CurrentOrders";
        private const string fromFinaliseDetailAction = "FinaliseOrderDetail";
        private const string targetRemoveFinaliseAction = "RemoveOrderFromFinalisePage";

        public static HtmlString GetCurrentOrderDetailDivId(this HtmlHelper helper, string id)
        {
            return MvcHtmlString.Create("order_detail_" + id);
        }

        public static decimal GetPriceFrom(this HtmlHelper helper, OrderClrList clrList)
        {
            return (from clr in clrList
                    from sku in clr.SKUs.SKU
                    orderby sku.Price ascending
                    select sku.Price).First();
        }

        public static bool IsPriceDifferentBySku(this HtmlHelper helper, OrderClrList clrList)
        {
            return (from clr in clrList
                    from sku in clr.SKUs.SKU
                    where sku.Ordered.Quantity > 0
                    select sku.Price).Distinct().Count() > 1;
        }

        public static string GetOrderDetailsJson(this HtmlHelper helper, Order order)
        {
            JsonResult json = OrderDetailsGridDataSet.Arrange(order);
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Serialize(json.Data);
        }

        public static string GetOrderDetailIdsArray(this HtmlHelper helper, Order order)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Serialize(string.Join(",", (from details in order.OrderDetails.OrderDetail
             orderby details.Id
             select details.Id).Distinct().ToArray()));
        }

        public static HtmlString GetOrderItemDetailTable(this HtmlHelper helper, OrderClrList clrList, string from = null)
        {
            //Generates SKU table heading

            var myClrList = from clr in clrList
                            orderby clr.Sequence
                            select clr;

            var sb = new StringBuilder();

            var sizeCodeComparer = new SizeCodeComparer();
            List<string> sizelist = (from clr in clrList
                                     from sku in clr.SKUs.SKU
                                     orderby sku.Sequence ascending
                                     select sku.SizeCode).Distinct(sizeCodeComparer).ToList();

            //Generate prodcut table for less than 10 sizes
            if (sizelist.Count <= 10)
            {
                sb.AppendLine("<tr>");
                sb.AppendLine("<th class=\"size\"></th>");

                foreach (string size in sizelist)
                {
                    if (!String.IsNullOrEmpty(size))
                    {
                        sb.AppendLine("<th class=\"size\">" + size + "</th>");
                    }
                    else
                    {
                        sb.AppendLine("<th class=\"size\">Each</th>");
                    }

                }

                sb.AppendLine("</tr>");

                foreach (OrderClr clr in myClrList)
                {
                    var localClr = from myClr in clrList
                                   where myClr.Ordered.Quantity > 0 && myClr.Id == clr.Id
                                   select myClr;
                    if (!String.IsNullOrEmpty(from))
                    {
                        GenerateItemDetailTableRow(sb, clr, sizelist);
                    }
                    else
                    {
                        if (localClr.Count() > 0)
                            GenerateItemDetailTableRow(sb, clr, sizelist);
                    }
                }
            }
            //Generate prodcut table for more than 10 sizes
            else
            {
                foreach (OrderClr clr in myClrList)
                {
                    var localClr = from myClr in clrList
                                   where myClr.Ordered.Quantity > 0 && myClr.Id == clr.Id
                                   select myClr;

                    if (!String.IsNullOrEmpty(from))
                    {
                        GenerateItemDetailTableRow(sb, clr, sizelist, true);
                    }
                    else
                    {
                        if (localClr.Count() > 0)
                            GenerateItemDetailTableRow(sb, clr, sizelist, true);
                    }
                }
            }

            return MvcHtmlString.Create(sb.ToString());
        }

        private static void GenerateItemDetailTableRow(StringBuilder sb, OrderClr clr, List<string> sizelist, bool overSizes = false)
        {
            //Generate prodcut table row for less than 10 sizes
            if (!overSizes)
            {
                sb.AppendLine("<tr>");

                if (!String.IsNullOrEmpty(clr.ClrName))
                {
                    sb.AppendLine("<th class='color'>" + clr.ClrName.Left(15) + "</th>");
                }
                else
                {
                    sb.AppendLine("<th class='color'>Default</th>");
                }

                foreach (string size in sizelist)
                {
                    bool hasitem = false;

                    foreach (OrderSKU sku in clr.SKUs.SKU)
                    {
                        if (size == sku.SizeCode)
                        {
                            if (sku.Ordered.Quantity > 0)
                            {
                                sb.AppendLine("<td>" + sku.Ordered.Quantity + "</td>");
                            }
                            else
                            {
                                sb.AppendLine("<td></td>");
                            }
                            hasitem = true;
                        }
                    }

                    if (!hasitem)
                        sb.AppendLine("<td></td>");
                }

                sb.AppendLine("</tr>");
            }
            //Generate prodcut table for more than 10 sizes
            else
            {
                int numOfGroups = Convert.ToInt16(Math.Ceiling(Convert.ToDecimal(sizelist.Count) / Convert.ToDecimal(10)));

                for (int i = 0; i < numOfGroups; i++)
                {
                    //Generate size row
                    sb.AppendLine("<tr>");
                    sb.AppendLine("<th class=\"size\"></th>");
                    for (int j = i * 10; j < (i + 1) * 10; j++)
                    {
                        if (j < sizelist.Count && sizelist[j] != null)
                        {
                            sb.AppendLine("<th class=\"size\">" + sizelist[j] + "</th>");
                        }
                        else
                        {
                            sb.AppendLine("<th class=\"size\"></th>");
                        }
                    }
                    sb.AppendLine("</tr>");

                    //Generate Color row
                    sb.AppendLine("<tr>");

                    if (!String.IsNullOrEmpty(clr.ClrName))
                    {
                        sb.AppendLine("<th class='color'>" + clr.ClrName.Left(15) + "</th>");
                    }
                    else
                    {
                        sb.AppendLine("<th class='color'>Default</th>");
                    }

                    for (int j = i * 10; j < (i + 1) * 10; j++)
                    {
                        bool hasitem = false;

                        foreach (OrderSKU sku in clr.SKUs.SKU)
                        {
                            if (j < sizelist.Count && sizelist[j] == sku.SizeCode)
                            {
                                if (sku.Ordered.Quantity > 0)
                                {
                                    sb.AppendLine("<td>" + sku.Ordered.Quantity + "</td>");
                                }
                                else
                                {
                                    sb.AppendLine("<td></td>");
                                }

                                hasitem = true;
                            }
                        }

                        if (!hasitem)
                        {
                            sb.AppendLine("<td></td>");
                        }

                    }
                    sb.AppendLine("</tr>");
                }
            }
        }

        public static HtmlString GetAddProductsLink(this HtmlHelper helper, string linkText)
        {
            var virtualPathData = RouteTable.Routes.GetVirtualPath(helper.ViewContext.RequestContext, "Default", new RouteValueDictionary());

            if (virtualPathData != null)
            {
                return MvcHtmlString.Create(virtualPathData.VirtualPath.Replace(fromFinaliseDetailAction, "").Replace(targetRemoveFinaliseAction, "").Replace(fromAddToOrderAction, "").Replace("Order", "Product"));
                //return MvcHtmlString.Create(@"<a  style='width:110px;' id='addProduct' href='" + virtualPathData.VirtualPath.Replace(fromFinaliseDetailAction, "").Replace(targetRemoveFinaliseAction, "").Replace(fromAddToOrderAction, "").Replace("Order", "Product")
                //                    + "' data-remote='true' ><img src='@Url.Content('~/Content/images/framework/ContinueShopping.png')' alt='SalesNet.Logo' /></a>");
                //return MvcHtmlString.Create(@"<a class='btn_medium' style='width:150px' id='addProduct' href='" + virtualPathData.VirtualPath.Replace(fromFinaliseDetailAction, "").Replace(targetRemoveFinaliseAction, "").Replace(fromAddToOrderAction, "").Replace("Order", "Product")
                //                    + "' data-remote='true' >" + linkText + "</a>");
            }//src='@Url.Content('~/Content/images/framework/ContinueShopping.png')'

            return MvcHtmlString.Create(String.Empty);
        }

        public static HtmlString GetDeliveryAddress(this HtmlHelper helper, Ap21API.Entities.Order.Address address)
        {
            var sb = new StringBuilder();
            if (address != null)
            {
                sb.Append("<span id='delivery_address'>");

                if (!String.IsNullOrEmpty(address.AddressLine1))
                {
                    sb.Append(address.AddressLine1 + "<br/>");
                }

                if (!String.IsNullOrEmpty(address.AddressLine2))
                {
                    sb.Append(address.AddressLine2 + "<br/>");
                }

                if (!String.IsNullOrEmpty(address.City))
                {
                    sb.Append(address.City + "<br/>");
                }

                if (!String.IsNullOrEmpty(address.State))
                {
                    sb.Append(address.State + "<br/>");
                }
                if (!String.IsNullOrEmpty(address.Postcode))
                {
                    sb.Append(address.Postcode + "<br/>");
                }
                if (!String.IsNullOrEmpty(address.Country))
                {
                    sb.Append(address.Country + "<br/>");
                }
                sb.Append("</span>");
                return MvcHtmlString.Create(sb.ToString());
            }

            return MvcHtmlString.Create(String.Empty);
        }

        public static HtmlString GetOrderCreateDate(this HtmlHelper helper)
        {
            return MvcHtmlString.Create("<span>" + DateTime.Today.ToString("dd-MMM-yyyy") + "</span>");
        }

        public class SizeCodeComparer : IEqualityComparer<string>
        {
            public bool Equals(string x, string y)
            {
                if (x == null || y == null)
                    return false;
                else
                    return x == y;
            }

            public int GetHashCode(string obj)
            {
                return obj.GetHashCode();
            }
        }

        public static Matric GetOrderMatric(this HtmlHelper helper, Order order)
        {
            Matric m = new Matric();

            bool orderCancelled = (order.Outstanding.Quantity == 0) && (order.Outstanding.Value == 0) && (order.Invoiced.Quantity == 0);
            bool orderInvoiced = (order.Outstanding.Quantity == 0) && (order.Outstanding.Value == 0) && (order.Invoiced.Quantity > 0);

            if (orderCancelled)
            {
                m.Gross = order.Ordered.Gross;
                m.Discount = order.Ordered.Discount;
                m.Net = order.Ordered.Net;
                m.Quantity = order.Ordered.Quantity;
                m.Tax = order.Ordered.Tax;
                m.Value = order.Ordered.Value;
                return m;
            }

            m.Gross = 0;
            m.Discount = 0;
            m.Net = 0;
            m.Quantity = 0;
            m.Tax = 0;
            m.Value = 0;

            foreach (var detail in order.OrderDetails.OrderDetail)
            {
                foreach (var colour in detail.Clrs.Clr)
                {
                    if (!(colour.OrderState.Equals("Complete") || orderInvoiced))
                    {
                        m.Gross += (colour.Ordered.Gross - colour.Invoiced.Gross);
                        m.Discount += (colour.Ordered.Discount - colour.Invoiced.Discount);
                        m.Net += (colour.Ordered.Net - colour.Invoiced.Net);
                        m.Quantity += (colour.Ordered.Quantity - colour.Invoiced.Quantity);
                        m.Tax += (colour.Ordered.Tax - colour.Invoiced.Tax);
                        m.Value += (colour.Ordered.Value - colour.Invoiced.Value);
                    }
                }
            }

            return m;
        }
    }
}