﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Xml.Linq;

namespace SalesNet.WebUI.Helpers
{
    public class ProductNoteTypeHelper
    {
        public static string GetNote(string code)
        {
            var configfile = WebConfigurationManager.OpenWebConfiguration("~");
            var xmlfile = XElement.Load(configfile.FilePath);

            var producttypes = xmlfile.Descendants("ProductNoteTypes").Elements();
            var productype =
                producttypes.FirstOrDefault(x => x.FirstNode.ToString() == string.Format("<Code>{0}</Code>", code));

            if (productype != null)
            {
                if ((productype.LastNode as XElement) != null) return (productype.LastNode as XElement).Value;
                return string.Empty;
            }
            //foreach (var producttype in producttypes)
            //{
            //    if (producttype.FirstNode.ToString() == string.Format("<Code>{0}</Code>", code) )
            //    {
            //        if ((producttype.LastNode as XElement) != null) return (producttype.LastNode as XElement).Value;
            //            return string.Empty;  
            //    }
            //}
            //}

            return string.Empty;
        }
    }
}