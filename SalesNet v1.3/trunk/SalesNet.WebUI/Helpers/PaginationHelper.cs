﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using SalesNet.WebUI.Classes;

namespace SalesNet.WebUI.Helpers
{
    public static class PaginationHelper
    {
        #region HtmlHelper extensions

        public static HtmlString GetPageLink(this HtmlHelper htmlHelper, int pageSize = 0, int currentPage = 0, int totalItemCount = 0, object valuesDictionary = null, string updateTarget = null)
        {
            var routeValuesDictionary = (Dictionary<string, string>)valuesDictionary;

            var pager = new Pager(htmlHelper.ViewContext, pageSize, currentPage, totalItemCount, routeValuesDictionary);
            

            if (updateTarget != null)
            {
                return MvcHtmlString.Create(pager.RenderHtml(updateTarget));
            }

            return MvcHtmlString.Create(pager.RenderHtml());
        }

        public static HtmlString GetOriginalGetPageLink(this HtmlHelper htmlHelper, int pageSize = 0, int currentPage = 0, int totalItemCount = 0, object valuesDictionary = null, string updateTarget = null)
        {
            var routeValuesDictionary = (Dictionary<string, string>)valuesDictionary;

            var pager = new OrderHistoryPager(htmlHelper.ViewContext, pageSize, currentPage, totalItemCount, routeValuesDictionary);


            if (updateTarget != null)
            {
                return MvcHtmlString.Create(pager.RenderHtml(updateTarget));
            }

            return MvcHtmlString.Create(pager.RenderHtml());
        }

        #endregion

        #region IQueryable<T> extensions

        public static IPagedList<T> ToPagedList<T>(this IQueryable<T> source, int pageIndex = 0, int pageSize = 0, int totalCount = 0, object searchCriteria = null)
        {
            return new PagedList<T>(source, pageIndex, pageSize, totalCount, searchCriteria);
        }

        #endregion

        #region IEnumerable<T> extensions

        public static IPagedList<T> ToPagedList<T>(this IEnumerable<T> source, int pageIndex = 0, int pageSize = 0, int totalCount = 0, object searchCriteria = null)
        {
            return new PagedList<T>(source, pageIndex, pageSize, totalCount, searchCriteria);
        }

        #endregion
    }
}