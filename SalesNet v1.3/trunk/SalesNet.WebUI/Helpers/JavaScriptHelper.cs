﻿using System.Web;
using System.Web.Mvc;

namespace SalesNet.WebUI.Helpers
{
    /// <summary>
    /// JavaScript related helpers
    /// </summary>
    public static class JavaScriptHelper
    {
        /// <summary>
        /// Output a JavaScript tag which contains a given script
        /// </summary>
        /// <param name="helper">Class to extend</param>
        /// <param name="script">JavaScript</param>
        /// <returns>SCRIPT tag containing the given JavaScript</returns>
        public static HtmlString JavaScript(this HtmlHelper helper, string script)
        {
            var builder = new TagBuilder("script");

            builder.MergeAttribute("type", "text/javascript");
            builder.InnerHtml = string.Format("$(function() {{ {0} }});", script);

            return MvcHtmlString.Create(builder.ToString(TagRenderMode.Normal));
        }
    }
}
