﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.IO;

namespace SalesNet.WebUI.Helpers
{
    public static class PromoBannerHelper
    {
        public static HtmlString PromoBanner(this HtmlHelper helper, string url, object htmlAttributes = null)
        {
            var outbuilder = new TagBuilder("div");
            outbuilder.GenerateId("promo_banner");
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
            string relativePath = urlHelper.Content(url);
            if (File.Exists(HttpContext.Current.Request.MapPath(relativePath)))
            {
                var imagebuilder = new TagBuilder("img");
                imagebuilder.MergeAttribute("src", relativePath);
                if (htmlAttributes != null)
                {
                    imagebuilder.MergeAttributes(new RouteValueDictionary(htmlAttributes));
                }

                var topbuilder = new TagBuilder("div");
                topbuilder.AddCssClass("shadow top");

                var bottombuilder = new TagBuilder("div");
                bottombuilder.AddCssClass("shadow bottom");
                outbuilder.InnerHtml = topbuilder.ToString() + bottombuilder.ToString() + imagebuilder.ToString(TagRenderMode.SelfClosing);
            }
            return MvcHtmlString.Create(outbuilder.ToString());
        }
    }
}