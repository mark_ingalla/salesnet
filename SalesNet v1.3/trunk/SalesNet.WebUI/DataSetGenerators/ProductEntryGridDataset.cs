﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Ap21API.Entities.Order;
using Ap21API.Entities.Product;
using SalesNet.WebUI.Models;

namespace SalesNet.WebUI.DataSetGenerators
{
    public class ProductEntryGridDataSet
    {
        class OrderGridLine
        {
            public string LineType { get; set; }
            public string Colour { get; set; }
            public string Size1 { get; set; }
            public string Size2 { get; set; }
            public string Size3 { get; set; }
            public string Size4 { get; set; }
            public string Size5 { get; set; }
            public string Size6 { get; set; }
            public string Size7 { get; set; }
            public string Size8 { get; set; }
            public string Size9 { get; set; }
            public string Size10 { get; set; }
            public string Size11 { get; set; }
            public string Size12 { get; set; }
            public string Size13 { get; set; }
            public string Size14 { get; set; }
            public string Size15 { get; set; }
            public string Size16 { get; set; }
            public string Size17 { get; set; }
            public string Size18 { get; set; }
            public string Size19 { get; set; }
            public string Size20 { get; set; }
            public string Size21 { get; set; }
            public string Size22 { get; set; }
            public string Size23 { get; set; }
            public string Size24 { get; set; }
            public string Size25 { get; set; }
            public string Size26 { get; set; }
            public string Size27 { get; set; }
            public string Size28 { get; set; }
            public string Size29 { get; set; }
            public string Size30 { get; set; }
            public string Stock1 { get; set; }
            public string Stock2 { get; set; }
            public string Stock3 { get; set; }
            public string Stock4 { get; set; }
            public string Stock5 { get; set; }
            public string Stock6 { get; set; }
            public string Stock7 { get; set; }
            public string Stock8 { get; set; }
            public string Stock9 { get; set; }
            public string Stock10 { get; set; }
            public string Stock11 { get; set; }
            public string Stock12 { get; set; }
            public string Stock13 { get; set; }
            public string Stock14 { get; set; }
            public string Stock15 { get; set; }
            public string Stock16 { get; set; }
            public string Stock17 { get; set; }
            public string Stock18 { get; set; }
            public string Stock19 { get; set; }
            public string Stock20 { get; set; }
            public string Stock21 { get; set; }
            public string Stock22 { get; set; }
            public string Stock23 { get; set; }
            public string Stock24 { get; set; }
            public string Stock25 { get; set; }
            public string Stock26 { get; set; }
            public string Stock27 { get; set; }
            public string Stock28 { get; set; }
            public string Stock29 { get; set; }
            public string Stock30 { get; set; }
            public string Sku1 { get; set; }
            public string Sku2 { get; set; }
            public string Sku3 { get; set; }
            public string Sku4 { get; set; }
            public string Sku5 { get; set; }
            public string Sku6 { get; set; }
            public string Sku7 { get; set; }
            public string Sku8 { get; set; }
            public string Sku9 { get; set; }
            public string Sku10 { get; set; }
            public string Sku11 { get; set; }
            public string Sku12 { get; set; }
            public string Sku13 { get; set; }
            public string Sku14 { get; set; }
            public string Sku15 { get; set; }
            public string Sku16 { get; set; }
            public string Sku17 { get; set; }
            public string Sku18 { get; set; }
            public string Sku19 { get; set; }
            public string Sku20 { get; set; }
            public string Sku21 { get; set; }
            public string Sku22 { get; set; }
            public string Sku23 { get; set; }
            public string Sku24 { get; set; }
            public string Sku25 { get; set; }
            public string Sku26 { get; set; }
            public string Sku27 { get; set; }
            public string Sku28 { get; set; }
            public string Sku29 { get; set; }
            public string Sku30 { get; set; }
            public string Price1 { get; set; }
            public string Price2 { get; set; }
            public string Price3 { get; set; }
            public string Price4 { get; set; }
            public string Price5 { get; set; }
            public string Price6 { get; set; }
            public string Price7 { get; set; }
            public string Price8 { get; set; }
            public string Price9 { get; set; }
            public string Price10 { get; set; }
            public string Price11 { get; set; }
            public string Price12 { get; set; }
            public string Price13 { get; set; }
            public string Price14 { get; set; }
            public string Price15 { get; set; }
            public string Price16 { get; set; }
            public string Price17 { get; set; }
            public string Price18 { get; set; }
            public string Price19 { get; set; }
            public string Price20 { get; set; }
            public string Price21 { get; set; }
            public string Price22 { get; set; }
            public string Price23 { get; set; }
            public string Price24 { get; set; }
            public string Price25 { get; set; }
            public string Price26 { get; set; }
            public string Price27 { get; set; }
            public string Price28 { get; set; }
            public string Price29 { get; set; }
            public string Price30 { get; set; }
            public string OriginalPrice1 { get; set; }
            public string OriginalPrice2 { get; set; }
            public string OriginalPrice3 { get; set; }
            public string OriginalPrice4 { get; set; }
            public string OriginalPrice5 { get; set; }
            public string OriginalPrice6 { get; set; }
            public string OriginalPrice7 { get; set; }
            public string OriginalPrice8 { get; set; }
            public string OriginalPrice9 { get; set; }
            public string OriginalPrice10 { get; set; }
            public string OriginalPrice11 { get; set; }
            public string OriginalPrice12 { get; set; }
            public string OriginalPrice13 { get; set; }
            public string OriginalPrice14 { get; set; }
            public string OriginalPrice15 { get; set; }
            public string OriginalPrice16 { get; set; }
            public string OriginalPrice17 { get; set; }
            public string OriginalPrice18 { get; set; }
            public string OriginalPrice19 { get; set; }
            public string OriginalPrice20 { get; set; }
            public string OriginalPrice21 { get; set; }
            public string OriginalPrice22 { get; set; }
            public string OriginalPrice23 { get; set; }
            public string OriginalPrice24 { get; set; }
            public string OriginalPrice25 { get; set; }
            public string OriginalPrice26 { get; set; }
            public string OriginalPrice27 { get; set; }
            public string OriginalPrice28 { get; set; }
            public string OriginalPrice29 { get; set; }
            public string OriginalPrice30 { get; set; }
            public string RrpText { get; set; }
            public string RetailPrice1 { get; set; }
            public string RetailPrice2 { get; set; }
            public string RetailPrice3 { get; set; }
            public string RetailPrice4 { get; set; }
            public string RetailPrice5 { get; set; }
            public string RetailPrice6 { get; set; }
            public string RetailPrice7 { get; set; }
            public string RetailPrice8 { get; set; }
            public string RetailPrice9 { get; set; }
            public string RetailPrice10 { get; set; }
            public string RetailPrice11 { get; set; }
            public string RetailPrice12 { get; set; }
            public string RetailPrice13 { get; set; }
            public string RetailPrice14 { get; set; }
            public string RetailPrice15 { get; set; }
            public string RetailPrice16 { get; set; }
            public string RetailPrice17 { get; set; }
            public string RetailPrice18 { get; set; }
            public string RetailPrice19 { get; set; }
            public string RetailPrice20 { get; set; }
            public string RetailPrice21 { get; set; }
            public string RetailPrice22 { get; set; }
            public string RetailPrice23 { get; set; }
            public string RetailPrice24 { get; set; }
            public string RetailPrice25 { get; set; }
            public string RetailPrice26 { get; set; }
            public string RetailPrice27 { get; set; }
            public string RetailPrice28 { get; set; }
            public string RetailPrice29 { get; set; }
            public string RetailPrice30 { get; set; }
            public string Total { get; set; }

            //added by raju on 7 jan 2013
            //start

            public  string NextAvailable1 { get; set; }
            public  string NextAvailable2 { get; set; }
            public  string NextAvailable3 { get; set; }
            public  string NextAvailable4 { get; set; }
            public  string NextAvailable5 { get; set; }
            public  string NextAvailable6 { get; set; }
            public  string NextAvailable7 { get; set; }
            public  string NextAvailable8 { get; set; }
            public  string NextAvailable9 { get; set; }
            public  string NextAvailable10 { get; set; }
            public  string NextAvailable11 { get; set; }
            public  string NextAvailable12 { get; set; }
            public  string NextAvailable13 { get; set; }
            public  string NextAvailable14 { get; set; }
            public  string NextAvailable15 { get; set; }
            public  string NextAvailable16 { get; set; }
            public  string NextAvailable17 { get; set; }
            public  string NextAvailable18 { get; set; }
            public  string NextAvailable19 { get; set; }
            public  string NextAvailable20 { get; set; }
            public  string NextAvailable21 { get; set; }
            public  string NextAvailable22 { get; set; }
            public  string NextAvailable23 { get; set; }
            public  string NextAvailable24 { get; set; }
            public  string NextAvailable25 { get; set; }
            public  string NextAvailable26 { get; set; }
            public  string NextAvailable27 { get; set; }
            public  string NextAvailable28 { get; set; }
            public  string NextAvailable29 { get; set; }
            public  string NextAvailable30 { get; set; }

            //end

            public OrderGridLine(string lineType)
            {
                LineType = lineType;
            }

            public void SetSize(int index, string quantity, string skuidx, string price, string originalPrice, string stockAvailable, string retailPrice)
            {
                switch (index)
                {
                    case 0: Size1 = quantity; Sku1 = skuidx; Price1 = price; OriginalPrice1 = originalPrice; Stock1 = stockAvailable; RetailPrice1 = retailPrice; break;
                    case 1: Size2 = quantity; Sku2 = skuidx; Price2 = price; OriginalPrice2 = originalPrice; Stock2 = stockAvailable; RetailPrice2 = retailPrice; break;
                    case 2: Size3 = quantity; Sku3 = skuidx; Price3 = price; OriginalPrice3 = originalPrice; Stock3 = stockAvailable; RetailPrice3 = retailPrice; break;
                    case 3: Size4 = quantity; Sku4 = skuidx; Price4 = price; OriginalPrice4 = originalPrice; Stock4 = stockAvailable; RetailPrice4 = retailPrice; break;
                    case 4: Size5 = quantity; Sku5 = skuidx; Price5 = price; OriginalPrice5 = originalPrice; Stock5 = stockAvailable; RetailPrice5 = retailPrice; break;
                    case 5: Size6 = quantity; Sku6 = skuidx; Price6 = price; OriginalPrice6 = originalPrice; Stock6 = stockAvailable; RetailPrice6 = retailPrice; break;
                    case 6: Size7 = quantity; Sku7 = skuidx; Price7 = price; OriginalPrice7 = originalPrice; Stock7 = stockAvailable; RetailPrice7 = retailPrice; break;
                    case 7: Size8 = quantity; Sku8 = skuidx; Price8 = price; OriginalPrice8 = originalPrice; Stock8 = stockAvailable; RetailPrice8 = retailPrice; break;
                    case 8: Size9 = quantity; Sku9 = skuidx; Price9 = price; OriginalPrice9 = originalPrice; Stock9 = stockAvailable; RetailPrice9 = retailPrice; break;
                    case 9: Size10 = quantity; Sku10 = skuidx; Price10 = price; OriginalPrice10 = originalPrice; Stock10 = stockAvailable; RetailPrice10 = retailPrice; break;
                    case 10: Size11 = quantity; Sku11 = skuidx; Price11 = price; OriginalPrice11 = originalPrice; Stock11 = stockAvailable; RetailPrice11 = retailPrice; break;
                    case 11: Size12 = quantity; Sku12 = skuidx; Price12 = price; OriginalPrice12 = originalPrice; Stock12 = stockAvailable; RetailPrice12 = retailPrice; break;
                    case 12: Size13 = quantity; Sku13 = skuidx; Price13 = price; OriginalPrice13 = originalPrice; Stock13 = stockAvailable; RetailPrice13 = retailPrice; break;
                    case 13: Size14 = quantity; Sku14 = skuidx; Price14 = price; OriginalPrice14 = originalPrice; Stock14 = stockAvailable; RetailPrice14 = retailPrice; break;
                    case 14: Size15 = quantity; Sku15 = skuidx; Price15 = price; OriginalPrice15 = originalPrice; Stock15 = stockAvailable; RetailPrice15 = retailPrice; break;
                    case 15: Size16 = quantity; Sku16 = skuidx; Price16 = price; OriginalPrice16 = originalPrice; Stock16 = stockAvailable; RetailPrice16 = retailPrice; break;
                    case 16: Size17 = quantity; Sku17 = skuidx; Price17 = price; OriginalPrice17 = originalPrice; Stock17 = stockAvailable; RetailPrice17 = retailPrice; break;
                    case 17: Size18 = quantity; Sku18 = skuidx; Price18 = price; OriginalPrice18 = originalPrice; Stock18 = stockAvailable; RetailPrice18 = retailPrice; break;
                    case 18: Size19 = quantity; Sku19 = skuidx; Price19 = price; OriginalPrice19 = originalPrice; Stock19 = stockAvailable; RetailPrice19 = retailPrice; break;
                    case 19: Size20 = quantity; Sku20 = skuidx; Price20 = price; OriginalPrice20 = originalPrice; Stock20 = stockAvailable; RetailPrice20 = retailPrice; break;
                    case 20: Size21 = quantity; Sku21 = skuidx; Price21 = price; OriginalPrice21 = originalPrice; Stock21 = stockAvailable; RetailPrice21 = retailPrice; break;
                    case 21: Size22 = quantity; Sku22 = skuidx; Price22 = price; OriginalPrice22 = originalPrice; Stock22 = stockAvailable; RetailPrice22 = retailPrice; break;
                    case 22: Size23 = quantity; Sku23 = skuidx; Price23 = price; OriginalPrice23 = originalPrice; Stock23 = stockAvailable; RetailPrice23 = retailPrice; break;
                    case 23: Size24 = quantity; Sku24 = skuidx; Price24 = price; OriginalPrice24 = originalPrice; Stock24 = stockAvailable; RetailPrice24 = retailPrice; break;
                    case 24: Size25 = quantity; Sku25 = skuidx; Price25 = price; OriginalPrice25 = originalPrice; Stock25 = stockAvailable; RetailPrice25 = retailPrice; break;
                    case 25: Size26 = quantity; Sku26 = skuidx; Price26 = price; OriginalPrice26 = originalPrice; Stock26 = stockAvailable; RetailPrice26 = retailPrice; break;
                    case 26: Size27 = quantity; Sku27 = skuidx; Price27 = price; OriginalPrice27 = originalPrice; Stock27 = stockAvailable; RetailPrice27 = retailPrice; break;
                    case 27: Size28 = quantity; Sku28 = skuidx; Price28 = price; OriginalPrice28 = originalPrice; Stock28 = stockAvailable; RetailPrice28 = retailPrice; break;
                    case 28: Size29 = quantity; Sku29 = skuidx; Price29 = price; OriginalPrice29 = originalPrice; Stock29 = stockAvailable; RetailPrice29 = retailPrice; break;
                    case 29: Size30 = quantity; Sku30 = skuidx; Price30 = price; OriginalPrice30 = originalPrice; Stock30 = stockAvailable; RetailPrice30 = retailPrice; break;

                    default:
                        throw new Exception(string.Format("Size index {0} out of range", index));
                }
            }


            //start
            //public void SetSize(int index, string quantity, string skuidx, string price, string originalPrice, string stockAvailable, string retailPrice, string nextavailable)
            //{
            //    switch (index)
            //    {
            //        case 0: Size1 = quantity; Sku1 = skuidx; Price1 = price; OriginalPrice1 = originalPrice; Stock1 = stockAvailable; RetailPrice1 = retailPrice; NextAvailable1 = nextavailable; break;
            //        case 1: Size2 = quantity; Sku2 = skuidx; Price2 = price; OriginalPrice2 = originalPrice; Stock2 = stockAvailable; RetailPrice2 = retailPrice; NextAvailable2 = nextavailable; break;
            //        case 2: Size3 = quantity; Sku3 = skuidx; Price3 = price; OriginalPrice3 = originalPrice; Stock3 = stockAvailable; RetailPrice3 = retailPrice; NextAvailable3 = nextavailable; break;
            //        case 3: Size4 = quantity; Sku4 = skuidx; Price4 = price; OriginalPrice4 = originalPrice; Stock4 = stockAvailable; RetailPrice4 = retailPrice; NextAvailable4 = nextavailable; break;
            //        case 4: Size5 = quantity; Sku5 = skuidx; Price5 = price; OriginalPrice5 = originalPrice; Stock5 = stockAvailable; RetailPrice5 = retailPrice; NextAvailable5 = nextavailable; break;
            //        case 5: Size6 = quantity; Sku6 = skuidx; Price6 = price; OriginalPrice6 = originalPrice; Stock6 = stockAvailable; RetailPrice6 = retailPrice; NextAvailable6 = nextavailable; break;
            //        case 6: Size7 = quantity; Sku7 = skuidx; Price7 = price; OriginalPrice7 = originalPrice; Stock7 = stockAvailable; RetailPrice7 = retailPrice; NextAvailable7 = nextavailable; break;
            //        case 7: Size8 = quantity; Sku8 = skuidx; Price8 = price; OriginalPrice8 = originalPrice; Stock8 = stockAvailable; RetailPrice8 = retailPrice; NextAvailable8 = nextavailable; break;
            //        case 8: Size9 = quantity; Sku9 = skuidx; Price9 = price; OriginalPrice9 = originalPrice; Stock9 = stockAvailable; RetailPrice9 = retailPrice; NextAvailable9 = nextavailable; break;
            //        case 9: Size10 = quantity; Sku10 = skuidx; Price10 = price; OriginalPrice10 = originalPrice; Stock10 = stockAvailable; RetailPrice10 = retailPrice; NextAvailable10 = nextavailable; break;
            //        case 10: Size11 = quantity; Sku11 = skuidx; Price11 = price; OriginalPrice11 = originalPrice; Stock11 = stockAvailable; RetailPrice11 = retailPrice; NextAvailable11 = nextavailable; break;
            //        case 11: Size12 = quantity; Sku12 = skuidx; Price12 = price; OriginalPrice12 = originalPrice; Stock12 = stockAvailable; RetailPrice12 = retailPrice; NextAvailable12 = nextavailable; break;
            //        case 12: Size13 = quantity; Sku13 = skuidx; Price13 = price; OriginalPrice13 = originalPrice; Stock13 = stockAvailable; RetailPrice13 = retailPrice; NextAvailable13 = nextavailable; break;
            //        case 13: Size14 = quantity; Sku14 = skuidx; Price14 = price; OriginalPrice14 = originalPrice; Stock14 = stockAvailable; RetailPrice14 = retailPrice; NextAvailable14 = nextavailable; break;
            //        case 14: Size15 = quantity; Sku15 = skuidx; Price15 = price; OriginalPrice15 = originalPrice; Stock15 = stockAvailable; RetailPrice15 = retailPrice; NextAvailable15 = nextavailable; break;
            //        case 15: Size16 = quantity; Sku16 = skuidx; Price16 = price; OriginalPrice16 = originalPrice; Stock16 = stockAvailable; RetailPrice16 = retailPrice; NextAvailable16 = nextavailable; break;
            //        case 16: Size17 = quantity; Sku17 = skuidx; Price17 = price; OriginalPrice17 = originalPrice; Stock17 = stockAvailable; RetailPrice17 = retailPrice; NextAvailable17 = nextavailable; break;
            //        case 17: Size18 = quantity; Sku18 = skuidx; Price18 = price; OriginalPrice18 = originalPrice; Stock18 = stockAvailable; RetailPrice18 = retailPrice; NextAvailable18 = nextavailable; break;
            //        case 18: Size19 = quantity; Sku19 = skuidx; Price19 = price; OriginalPrice19 = originalPrice; Stock19 = stockAvailable; RetailPrice19 = retailPrice; NextAvailable19 = nextavailable; break;
            //        case 19: Size20 = quantity; Sku20 = skuidx; Price20 = price; OriginalPrice20 = originalPrice; Stock20 = stockAvailable; RetailPrice20 = retailPrice; NextAvailable20 = nextavailable; break;
            //        case 20: Size21 = quantity; Sku21 = skuidx; Price21 = price; OriginalPrice21 = originalPrice; Stock21 = stockAvailable; RetailPrice21 = retailPrice; NextAvailable21 = nextavailable; break;
            //        case 21: Size22 = quantity; Sku22 = skuidx; Price22 = price; OriginalPrice22 = originalPrice; Stock22 = stockAvailable; RetailPrice22 = retailPrice; NextAvailable22 = nextavailable; break;
            //        case 22: Size23 = quantity; Sku23 = skuidx; Price23 = price; OriginalPrice23 = originalPrice; Stock23 = stockAvailable; RetailPrice23 = retailPrice; NextAvailable23 = nextavailable; break;
            //        case 23: Size24 = quantity; Sku24 = skuidx; Price24 = price; OriginalPrice24 = originalPrice; Stock24 = stockAvailable; RetailPrice24 = retailPrice; NextAvailable24 = nextavailable; break;
            //        case 24: Size25 = quantity; Sku25 = skuidx; Price25 = price; OriginalPrice25 = originalPrice; Stock25 = stockAvailable; RetailPrice25 = retailPrice; NextAvailable25 = nextavailable; break;
            //        case 25: Size26 = quantity; Sku26 = skuidx; Price26 = price; OriginalPrice26 = originalPrice; Stock26 = stockAvailable; RetailPrice26 = retailPrice; NextAvailable26 = nextavailable; break;
            //        case 26: Size27 = quantity; Sku27 = skuidx; Price27 = price; OriginalPrice27 = originalPrice; Stock27 = stockAvailable; RetailPrice27 = retailPrice; NextAvailable27 = nextavailable; break;
            //        case 27: Size28 = quantity; Sku28 = skuidx; Price28 = price; OriginalPrice28 = originalPrice; Stock28 = stockAvailable; RetailPrice28 = retailPrice; NextAvailable28 = nextavailable; break;
            //        case 28: Size29 = quantity; Sku29 = skuidx; Price29 = price; OriginalPrice29 = originalPrice; Stock29 = stockAvailable; RetailPrice29 = retailPrice; NextAvailable29 = nextavailable; break;
            //        case 29: Size30 = quantity; Sku30 = skuidx; Price30 = price; OriginalPrice30 = originalPrice; Stock30 = stockAvailable; RetailPrice30 = retailPrice; NextAvailable30 = nextavailable; break;

            //        default:
            //            throw new Exception(string.Format("Size index {0} out of range", index));
            //    }
            //}
            //end
            //start
            public void nextavailable(int index, string nextavailable)
            { 
              switch (index)
              {
                  case 0: NextAvailable1 = nextavailable; break;
                  case 1: NextAvailable2 = nextavailable; break;
                  case 2: NextAvailable3 = nextavailable; break;
                  case 3: NextAvailable4 = nextavailable; break;
                  case 4: NextAvailable5 = nextavailable; break;
                  case 5: NextAvailable6 = nextavailable; break;
                  case 6: NextAvailable7 = nextavailable; break;
                  case 7: NextAvailable8 = nextavailable; break;
                  case 8: NextAvailable9 = nextavailable; break;
                  case 9: NextAvailable10 = nextavailable; break;
                  case 10: NextAvailable11 = nextavailable; break;
                  case 11: NextAvailable12 = nextavailable; break;
                  case 12: NextAvailable13 = nextavailable; break;
                  case 13: NextAvailable14 = nextavailable; break;
                  case 14: NextAvailable15 = nextavailable; break;
                  case 15: NextAvailable16 = nextavailable; break;
                  case 16: NextAvailable17 = nextavailable; break;
                  case 17: NextAvailable18 = nextavailable; break;
                  case 18: NextAvailable19 = nextavailable; break;
                  case 19: NextAvailable20 = nextavailable; break;
                  case 20: NextAvailable21 = nextavailable; break;
                  case 21: NextAvailable22 = nextavailable; break;
                  case 22: NextAvailable23 = nextavailable; break;
                  case 23: NextAvailable24 = nextavailable; break;
                  case 24: NextAvailable25 = nextavailable; break;
                  case 25: NextAvailable26 = nextavailable; break;
                  case 26: NextAvailable27 = nextavailable; break;
                  case 27: NextAvailable28 = nextavailable; break;
                  case 28: NextAvailable29 = nextavailable; break;
                  case 29: NextAvailable30 = nextavailable; break;
                  
                  default:

                        throw new Exception(string.Format("Size index {0} out of range", index));
              }
               
            }
            //end
        }

        public class ColorUnitNameComparer : IEqualityComparer<ColorUnit>
        {
            public bool Equals(ColorUnit x, ColorUnit y)
            {
                if (x == null || y == null)
                    return false;

                return x.Name == y.Name;
            }

            public int GetHashCode(ColorUnit obj)
            {
                return obj.Name.GetHashCode();
            }
        }

        private static List<String> sizeHeadings;

        private static OrderGridLine[] RenderGridLines(OrderDetailProductModel model)
        {
            var lines = new List<OrderGridLine>();
            sizeHeadings = new List<String>();

            var sizeHeading = new OrderGridLine("SIZEHEAD");

            var sizes = from clr in model.OrderDetailProduct.Product.Clrs.Clr
                        from sku in clr.SKUs.SKU
                        select new { sku.SizeCode, sku.Sequence };

            var maxSizes = 0;
            foreach (var size in sizes.Distinct().OrderBy(s => s.Sequence))
            {
                sizeHeadings.Add(String.IsNullOrEmpty(size.SizeCode) ? "Each" : size.SizeCode);
                sizeHeading.SetSize(maxSizes, String.IsNullOrEmpty(size.SizeCode) ? "Each" : size.SizeCode, null, null, null, null, null);
                maxSizes++;
            }

            lines.Add(sizeHeading);

            // Add one colour line at a time.
            var colours = from clr in model.Product.Clrs.Clr
                          select clr;

            foreach (var colour in colours.OrderBy(c => c.Sequence))
            {
                var colourLine = new OrderGridLine("COLOUR") { Colour = colour.Name };

                colourLine.RrpText = model.Product != null ? model.Product.RrpText : "";

                // See if this colour has been ordered 
                // Must make a local copy of the enumeration value, or it may use the wrong value.
                var localColour = colour;
                var orderedColour = model.OrderDetailProduct.OrderDetail == null ? null : model.OrderDetailProduct.OrderDetail.Clrs.Clr.Find(c => c.ClrId == localColour.Id);

                foreach (var sku in colour.SKUs.SKU)
                {
                    var position = sizeHeadings.IndexOf(sku.SizeCode);
                    decimal quantity = 0;

                    // set only once as text is same throughout product


                    // If this colour is ordered, check if this sku has been ordered.
                    if (orderedColour != null)
                    {
                        // Must make a local copy of the enumeration value, or it may use the wrong value.
                        var localSku = sku;
                        var orderedSku = orderedColour.SKUs.SKU.Find(s => s.SkuId == localSku.Id);
                        if (orderedSku != null)
                        {
                            quantity = orderedSku.Ordered.Quantity;
                        }
                    }

                    colourLine.SetSize(position, String.Format("{0}", quantity), sku.Id,
                                       Helpers.TextFormatHelper.FormatCurrency(sku.Price,
                                                                               model.OrderDetailProduct.Currency.Format),
                                       Helpers.TextFormatHelper.FormatCurrency(sku.OriginalPrice,
                                                                               model.OrderDetailProduct.Currency.Format),
                                       String.Format("{0}", sku.FreeStock),
                                       sku.RetailPrice.ToString());
                    if (sku.NextAvailable != null && sku.NextAvailable != "")
                    { 
                        string strNextAvail = DateTime.Parse(sku.NextAvailable).ToShortDateString().Replace("-", "/");
                        colourLine.nextavailable(position, strNextAvail);
                    }
                    else {
                        colourLine.nextavailable(position, sku.NextAvailable);
                    }

                    //end
                }

                lines.Add(colourLine);
            }
                return lines.ToArray();
        }


        internal static JsonResult Arrange(OrderDetailProductModel product)
        {
            var json = new JsonResult()
            {
               
                Data = RenderGridLines(product),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                ContentType = "text/json"
            };

            return json;
        }
    }
}
