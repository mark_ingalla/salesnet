﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Ap21API.Entities.Order;
using SalesNet.WebUI.Helpers;

namespace SalesNet.WebUI.DataSetGenerators
{
    public class OrderDetailsGridDataSet
    {
        struct Values
        {
            public Decimal Quantity;
            public Decimal Gross;
        }

        class ColourLine
        {
            private string[] skuidxs = new string[30];
            private Values[] values = new Values[30];
            public string[] sizeCode = new string[30];

            public void Add(int position, string skuidx, Decimal quantity, Decimal gross)
            {
                values[position].Quantity += quantity;
                values[position].Gross += gross;
                skuidxs[position] = skuidx;
            }

            /// <summary>
            /// Get all the values for the sizes from size[first] to size[last]
            /// </summary>
            /// <param name="first">Zero indexed position of first size to consider</param>
            /// <param name="last">Zero indexed position of last size to consider</param>
            /// <returns></returns>
            public Values[] GetValues(int first, int last)
            {
                var result = new Values[last - first + 1];

                int count = 0;
                for (int x = first; x <= last; x++)
                {
                    result[count] = values[x];
                    count++;
                }
                return result;
            }

            /// <summary>
            /// Gets a concatenated list of size codes which is used as a unique identifier to tell if we need to reprint the size heading.
            /// </summary>
            /// <param name="first"></param>
            /// <param name="last"></param>
            /// <returns></returns>
            public string GetSizeCodeString(int first, int last)
            {
                StringBuilder result = new StringBuilder();

                for (int x = first; x <= last; x++)
                {
                    result.Append(sizeCode[x]);
                    result.Append(",");
                }
                return result.ToString();
            }

            public ColourLine()
            {
                // Create 30 sizes and set them to blank
                for (int x = 0; x < 30; x++)
                {
                    skuidxs[x] = "";
                    values[x].Quantity = 0;
                    values[x].Gross = 0;
                    sizeCode[x] = "";
                }
            }
        }

        
     public  class OrderGridLine
        {
            public string LineType { get; set; }
            public string Id { get; set; }
            public string DetailId { get; set; }
            public string Product { get; set; }
            public string Description { get; set; }
            public string Colour { get; set; }
            public string Size1 { get; set; }
            public string Size2 { get; set; }
            public string Size3 { get; set; }
            public string Size4 { get; set; }
            public string Size5 { get; set; }
            public string Size6 { get; set; }
            public string Size7 { get; set; }
            public string Size8 { get; set; }
            public string Size9 { get; set; }
            public string Size10 { get; set; }
            public string UnitPrice { get; set; }
            public string Quantity { get; set; }
            public string Total { get; set; }

            public OrderGridLine(string lineType, ref int sequence, string detailId)
            {
                LineType = lineType;
                Id = String.Format("{0}", sequence);
                DetailId = detailId;
                sequence++;
            }

            public void SetSize(int index, string value)
            {
                switch (index)
                {
                    case 0: Size1 = value; break;
                    case 1: Size2 = value; break;
                    case 2: Size3 = value; break;
                    case 3: Size4 = value; break;
                    case 4: Size5 = value; break;
                    case 5: Size6 = value; break;
                    case 6: Size7 = value; break;
                    case 7: Size8 = value; break;
                    case 8: Size9 = value; break;
                    case 9: Size10 = value; break;
                    default:
                        throw new Exception(string.Format("Size index {0} out of range", index));
                }
            }
        }

        private static OrderGridLine[] RenderGridLines(Order order, Boolean orderedOnly = true)
        {
            List<OrderGridLine> lines = new List<OrderGridLine>();
            OrderGridLine productLine;
            OrderGridLine colourLine;
            OrderGridLine sizeHeading;
            ColourLine line;
           // TotalLine Tline;
            int sequence = 1;
            string lastSizeHeadingString;
            Values[] quantities;
            bool firstProduct = true;
            bool firstColourLine;
            var blank = new OrderGridLine("BLANK", ref  sequence, "0");
            Decimal lineQuantity;
            Decimal lineGross;
            int addTo;

            Decimal qtyTotal = 0;
            Decimal grossTotal = 0;
            Decimal discountTotal = 0;
            Decimal taxTotal = 0;
            bool orderCancelled = (order.Outstanding.Quantity == 0) && (order.Outstanding.Value == 0) && (order.Invoiced.Quantity == 0);
            bool orderInvoiced = (order.Outstanding.Quantity == 0) && (order.Outstanding.Value == 0) && (order.Invoiced.Quantity > 0);

            var details = from item in order.OrderDetails.OrderDetail
                          where ((!(orderedOnly)) || (item.Ordered.Quantity != 0))
                          select item;

            foreach (var detail in details)
            {
                /*
                 *  detail.Clrs[0].ClrName 
                    "Black"
                    detail.Clrs[0].Ordered.Quantity 
                    1
                 * 
                 * */
                lastSizeHeadingString = "";
                if (firstProduct)
                {
                    firstProduct = false;
                }
                else
                {
                    lines.Add(blank);
                }

                productLine = new OrderGridLine("PRODUCT", ref sequence, detail.Id);
                productLine.Product = detail.ProductCode;
                productLine.Description = detail.ProductName;
                lines.Add(productLine);

                // KK - get unique List of sizes for detail
                int maxSizes = 0;
                var distrinctList =
                    from c in detail.Clrs.Clr
                    from k in c.SKUs.SKU
                    select new {Code = k.SizeCode, Seq = k.Sequence};
                List<string> sizes = new List<string>();
                foreach (var item in distrinctList.Distinct().OrderBy(dl => dl.Seq))
                {
                    sizes.Add(String.IsNullOrEmpty(item.Code) ? "Each" : item.Code);
                    maxSizes++;
                }
                
                var clrs = from item in detail.Clrs.Clr
                           where ((!(orderedOnly)) || (item.Ordered.Quantity != 0))
                           select item;
                #region ColorLine

                foreach (var colour in clrs)
                {

                    line = new ColourLine();

                    var skus = (from sku in colour.SKUs.SKU
                                select sku).OrderBy(sku => sku.Sequence);

                    for (int i = 0; i < sizes.Count; i++)
                    {
                        line.sizeCode[i] = sizes[i];
                    }

                    foreach (var sku in skus)
                    {
                        addTo = 0;
                        for (int i = 0; i < maxSizes; i++)
                        {
                            if (sizes[i] == (String.IsNullOrEmpty(sku.SizeCode) ? "Each" : sku.SizeCode))
                            {
                                addTo = i;
                                break;
                            }
                        }

                        if (orderCancelled)
                        {
                            line.Add(addTo, sku.SkuId, (sku.Ordered.Quantity),
                                     (sku.Ordered.Gross));
                            qtyTotal += sku.Ordered.Quantity;
                            grossTotal += sku.Ordered.Gross;
                            discountTotal += sku.Ordered.Discount;
                            taxTotal += sku.Ordered.Tax;
                        }
                        else
                        {
                            // Don't show completed lines
                            if (colour.OrderState.Equals("Complete") || orderInvoiced)
                            {
                                line.Add(addTo, sku.SkuId, 0, 0);
                            }
                            else
                            {
                                line.Add(addTo, sku.SkuId, (sku.Ordered.Quantity - sku.Invoiced.Quantity),
                                         (sku.Ordered.Gross - sku.Invoiced.Gross));
                                qtyTotal += sku.Ordered.Quantity - sku.Invoiced.Quantity;
                                grossTotal += sku.Ordered.Gross - sku.Invoiced.Gross;
                                discountTotal += sku.Ordered.Discount - sku.Invoiced.Discount;
                                taxTotal += sku.Ordered.Tax - sku.Invoiced.Tax;
                            }
                        }
                    } 
                #endregion

                    firstColourLine = true;

                    for (int y = 0; ((y < 3) && ((y*10) + 1 <= maxSizes)); y++)
                    {
                        if (line.GetSizeCodeString(y*10, y*10 + 9) != lastSizeHeadingString)
                        {
                            // need a new size heading
                            sizeHeading = new OrderGridLine("SIZEHEAD", ref sequence, detail.Id);
                            sizeHeading.Description = "Type";
                            for (int i = 0; i < 10; i++)
                            {
                                sizeHeading.SetSize(i, line.sizeCode[y*10 + i]);
                            }
                            lines.Add(sizeHeading);

                            lastSizeHeadingString = line.GetSizeCodeString(y*10, y*10 + 9);
                        }

                        // show the values 
                        colourLine = new OrderGridLine("COLOUR", ref sequence, detail.Id);
                       
                        if (firstColourLine)
                        {
                            colourLine.Colour = colour.ClrName;
                            firstColourLine = false;
                        }

                        lineGross = 0;
                        lineQuantity = 0;

                        quantities = line.GetValues(y*10, y*10 + 9);
                        for (int i = 0; i < 10; i++)
                        {
                            if (quantities[i].Quantity != 0)
                            {
                                colourLine.SetSize(i, String.Format("{0}", quantities[i].Quantity));
                                lineGross += quantities[i].Gross;
                                lineQuantity += quantities[i].Quantity;
                            }
                        }

                        colourLine.Quantity = string.Format("{0}", lineQuantity);
                        if (lineQuantity != 0)
                            colourLine.UnitPrice = Helpers.TextFormatHelper.FormatCurrency(lineGross/lineQuantity,
                                                                                           order.Currency.Format);
                        colourLine.Total = Helpers.TextFormatHelper.FormatCurrency(lineGross,
                                                                                   order.Currency.Format);

                        lines.Add(colourLine);
                    }
                }
            }

            // Add the totals
            lines.Add(blank);

            var gross = new OrderGridLine("GROSS", ref sequence, "0");
            gross.UnitPrice = "Gross";
            gross.Quantity = String.Format("{0}", qtyTotal);
            gross.Total = Helpers.TextFormatHelper.FormatCurrency(grossTotal, order.Currency.Format);
            lines.Add(gross);

            //if (discountTotal > 0)
            {
                var discount = new OrderGridLine("DISCOUNT", ref sequence, "0");
                discount.UnitPrice = "Discount";
                discount.Total = Helpers.TextFormatHelper.FormatCurrency(discountTotal,
                                                                         order.Currency.Format);
                lines.Add(discount);
            }

            var tax = new OrderGridLine("TAX", ref sequence, "0");
            tax.UnitPrice = "Tax";
            tax.Total = Helpers.TextFormatHelper.FormatCurrency(taxTotal, order.Currency.Format);
            lines.Add(tax);

            var due = new OrderGridLine("DUE", ref sequence, "0");
            due.UnitPrice = "DUE";
            due.Total = Helpers.TextFormatHelper.FormatCurrency(grossTotal + taxTotal - discountTotal, order.Currency.Format);
            lines.Add(due);

            int lineNo = 0;
            OrderGridLine[] result = new OrderGridLine[lines.Count];
            foreach (var l in lines)
            {
                result[lineNo++] = l;
            }
            return result;

        }
        /// <summary>
        /// Returns the Arranged Order Grid Lines.
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public static OrderGridLine[] GetGridLines(Order order)
        {
         return   RenderGridLines(order, true);
        }
        /// <summary>
        /// Returns the Arranged Order Grid Lines.
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public static List<OrderDetiailsGrid> GetOrderDetiailsGridList(Order order)
        {
            
            SalesNet.WebUI.DataSetGenerators.OrderDetailsGridDataSet.OrderGridLine[] OrderGridRows =  RenderGridLines(order, true);;
            List<OrderDetiailsGrid> lstODG = new List<OrderDetiailsGrid>();
            OrderDetiailsGrid objOrderDetiailsGrid;
            SalesNet.WebUI.DataSetGenerators.OrderDetailsGridDataSet.OrderGridLine objOrderGridLine;
            for (int intLoop = 0; intLoop < OrderGridRows.Length; intLoop++)
            {
                
                    objOrderDetiailsGrid = new OrderDetiailsGrid();
                    objOrderGridLine = OrderGridRows[intLoop];
                    objOrderDetiailsGrid.LineType = objOrderGridLine.LineType;
                    objOrderDetiailsGrid.Colour = objOrderGridLine.Colour;
                    objOrderDetiailsGrid.Description = objOrderGridLine.Description;
                    objOrderDetiailsGrid.DetailId = objOrderGridLine.DetailId;

                    objOrderDetiailsGrid.Product = objOrderGridLine.Product;
                    objOrderDetiailsGrid.Quantity = objOrderGridLine.Quantity;
                    objOrderDetiailsGrid.Total = objOrderGridLine.Total;
                    objOrderDetiailsGrid.UnitPrice = objOrderGridLine.UnitPrice;
                    objOrderDetiailsGrid.Sizes = new List<string>();
                    if (objOrderDetiailsGrid.LineType == "PRODUCT")
                    {
                        

                        //Adding to main list (output list)
                        lstODG.Add(objOrderDetiailsGrid);

                        #region per Single Product

                        bool isSizeFirst = true;
                        int intSizeHead = 0;
                        bool isColorFirst = true;
                        int intColorIndx = 0;
                        string strLastColor = "";
                        for (int intInnerLoop = intLoop + 1; intInnerLoop < OrderGridRows.Length; intInnerLoop++)
                        {
                            objOrderGridLine = OrderGridRows[intInnerLoop];

                            //Filtering first start
                            if (objOrderGridLine.LineType == "SIZEHEAD")
                            {
                                if (isSizeFirst)
                                {
                                    objOrderDetiailsGrid = new OrderDetiailsGrid();

                                    objOrderDetiailsGrid.Colour = objOrderGridLine.Colour;
                                    objOrderDetiailsGrid.Description = objOrderGridLine.Description;
                                    objOrderDetiailsGrid.DetailId = objOrderGridLine.DetailId;
                                    objOrderDetiailsGrid.LineType = objOrderGridLine.LineType;
                                    objOrderDetiailsGrid.Product = objOrderGridLine.Product;
                                    objOrderDetiailsGrid.Quantity = objOrderGridLine.Quantity;
                                    objOrderDetiailsGrid.Total = objOrderGridLine.Total;
                                    objOrderDetiailsGrid.UnitPrice = objOrderGridLine.UnitPrice;
                                    objOrderDetiailsGrid.Sizes = new List<string>();
                                }
                                else
                                {
                                    objOrderDetiailsGrid = lstODG[intSizeHead];
                                }
                            }
                            else if (objOrderGridLine.LineType == "COLOUR")
                            {

                                if ((isColorFirst || strLastColor != objOrderGridLine.Colour)&& objOrderGridLine.Colour!=null)
                                {
                                    objOrderDetiailsGrid = new OrderDetiailsGrid();

                                    objOrderDetiailsGrid.Colour = objOrderGridLine.Colour;
                                    objOrderDetiailsGrid.Description = objOrderGridLine.Description;
                                    objOrderDetiailsGrid.DetailId = objOrderGridLine.DetailId;
                                    objOrderDetiailsGrid.LineType = objOrderGridLine.LineType;
                                    objOrderDetiailsGrid.Product = objOrderGridLine.Product;
                                    objOrderDetiailsGrid.Quantity = objOrderGridLine.Quantity;
                                    objOrderDetiailsGrid.Total = objOrderGridLine.Total;
                                    objOrderDetiailsGrid.UnitPrice = objOrderGridLine.UnitPrice;
                                    objOrderDetiailsGrid.Sizes = new List<string>();
                                }
                                else
                                {
                                    objOrderDetiailsGrid = lstODG[intColorIndx];
                                    //mobified by siva 18Jan2013 #109
                                    objOrderDetiailsGrid.Quantity = (int.Parse(objOrderDetiailsGrid.Quantity) + int.Parse(objOrderGridLine.Quantity)).ToString();
                                }
                            }
                            //Filtering first End

                            if (objOrderGridLine.LineType == "SIZEHEAD")
                            {
                                if (isSizeFirst == true)
                                {
                                    intSizeHead = lstODG.Count;
                                }

                                if(!objOrderDetiailsGrid.Sizes.Contains(objOrderGridLine.Size1 != null ? objOrderGridLine.Size1 : ""))
                                {
                                objOrderDetiailsGrid.Sizes.Add(objOrderGridLine.Size1 != null ? objOrderGridLine.Size1 : "");
                                }
                                if(!objOrderDetiailsGrid.Sizes.Contains(objOrderGridLine.Size2 != null ? objOrderGridLine.Size2 : ""))
                                {
                                objOrderDetiailsGrid.Sizes.Add(objOrderGridLine.Size2 != null ? objOrderGridLine.Size2 : "");
                                }
                                if(!objOrderDetiailsGrid.Sizes.Contains(objOrderGridLine.Size3 != null ? objOrderGridLine.Size3 : ""))
                                {
                                objOrderDetiailsGrid.Sizes.Add(objOrderGridLine.Size3 != null ? objOrderGridLine.Size3 : "");
                                }
                                if(!objOrderDetiailsGrid.Sizes.Contains(objOrderGridLine.Size4 != null ? objOrderGridLine.Size4 : ""))
                                {
                                objOrderDetiailsGrid.Sizes.Add(objOrderGridLine.Size4 != null ? objOrderGridLine.Size4 : "");
                                }
                                if(!objOrderDetiailsGrid.Sizes.Contains(objOrderGridLine.Size5 != null ? objOrderGridLine.Size5 : ""))
                                {
                                objOrderDetiailsGrid.Sizes.Add(objOrderGridLine.Size5 != null ? objOrderGridLine.Size5 : "");
                                }
                                if(!objOrderDetiailsGrid.Sizes.Contains(objOrderGridLine.Size6 != null ? objOrderGridLine.Size6 : ""))
                                {
                                objOrderDetiailsGrid.Sizes.Add(objOrderGridLine.Size6 != null ? objOrderGridLine.Size6 : "");
                                }
                                if(!objOrderDetiailsGrid.Sizes.Contains(objOrderGridLine.Size7 != null ? objOrderGridLine.Size7 : ""))
                                {
                                objOrderDetiailsGrid.Sizes.Add(objOrderGridLine.Size7 != null ? objOrderGridLine.Size7 : "");
                                }
                                if(!objOrderDetiailsGrid.Sizes.Contains(objOrderGridLine.Size8 != null ? objOrderGridLine.Size8 : ""))
                                {
                                objOrderDetiailsGrid.Sizes.Add(objOrderGridLine.Size8 != null ? objOrderGridLine.Size8 : "");
                                }
                                if(!objOrderDetiailsGrid.Sizes.Contains(objOrderGridLine.Size9 != null ? objOrderGridLine.Size9 : ""))
                                {
                                objOrderDetiailsGrid.Sizes.Add(objOrderGridLine.Size9 != null ? objOrderGridLine.Size9 : "");
                                }
                                if (!objOrderDetiailsGrid.Sizes.Contains(objOrderGridLine.Size10 != null ? objOrderGridLine.Size10 : ""))
                                {
                                objOrderDetiailsGrid.Sizes.Add(objOrderGridLine.Size10 != null ? objOrderGridLine.Size10 : "");
                                }

                                if (isSizeFirst == true)
                                {
                                    //Adding to main list (output list)
                                    lstODG.Add(objOrderDetiailsGrid);
                                    isSizeFirst = false;
                                }
                                intLoop = intInnerLoop;
                            }
                            else if (objOrderGridLine.LineType == "COLOUR")
                            {
                                if (isColorFirst == true || (objOrderGridLine.Colour!=null && strLastColor != objOrderGridLine.Colour))
                                {
                                    intColorIndx= lstODG.Count;
                                }

                             //   if (objOrderGridLine.Size1 != null)
                             //   {
                                    objOrderDetiailsGrid.Sizes.Add(objOrderGridLine.Size1);
                             //   }
                             //   if (objOrderGridLine.Size2 != null)
                             //   {
                                    objOrderDetiailsGrid.Sizes.Add(objOrderGridLine.Size2);
                              //  }
                        //        if (objOrderGridLine.Size3 != null)
                          //      {
                                    objOrderDetiailsGrid.Sizes.Add(objOrderGridLine.Size3);
                         //       }
                             //   if (objOrderGridLine.Size4 != null)
                            //    {
                                    objOrderDetiailsGrid.Sizes.Add(objOrderGridLine.Size4);
                            //    }
                            //    if (objOrderGridLine.Size5!= null)
                            //    {
                                    objOrderDetiailsGrid.Sizes.Add(objOrderGridLine.Size5);
                            //    }
                           //     if (objOrderGridLine.Size6 != null)
                            //    {
                                    objOrderDetiailsGrid.Sizes.Add(objOrderGridLine.Size6);
                             //   }
                            //    if (objOrderGridLine.Size7 != null)
                             //   {
                                    objOrderDetiailsGrid.Sizes.Add(objOrderGridLine.Size7);
                             //   }
                              //  if (objOrderGridLine.Size8 != null)
                               // {
                                    objOrderDetiailsGrid.Sizes.Add(objOrderGridLine.Size8);
                               // }
                                //if (objOrderGridLine.Size9 != null)
                                //{
                                    objOrderDetiailsGrid.Sizes.Add(objOrderGridLine.Size9);
                               // }
                                //if (objOrderGridLine.Size10 != null)
                                //{
                                    objOrderDetiailsGrid.Sizes.Add(objOrderGridLine.Size10);
                                //}


                                if ((isColorFirst == true || strLastColor != objOrderGridLine.Colour) && objOrderGridLine.Colour!=null)
                                {
                                    //Adding to main list (output list)
                                    lstODG.Add(objOrderDetiailsGrid);
                                    isColorFirst = false;
                                    strLastColor = objOrderGridLine.Colour;
                                }
                                intLoop = intInnerLoop;
                            }
                            


                            if (objOrderGridLine.LineType == "PRODUCT")
                            {
                                intInnerLoop = OrderGridRows.Length;
                               //for next product iteration
                            }
                        } 
                        #endregion

                        
                    }

                    else if (objOrderDetiailsGrid.LineType == "GROSS")
                    {
                        objOrderDetiailsGrid.Colour = objOrderGridLine.Colour;
                        objOrderDetiailsGrid.Description = objOrderGridLine.Description;
                        objOrderDetiailsGrid.DetailId = objOrderGridLine.DetailId;

                        objOrderDetiailsGrid.Product = objOrderGridLine.Product;
                        objOrderDetiailsGrid.Quantity = objOrderGridLine.Quantity;
                        objOrderDetiailsGrid.Total = objOrderGridLine.Total;
                        objOrderDetiailsGrid.UnitPrice = objOrderGridLine.UnitPrice;
                        objOrderDetiailsGrid.Sizes = new List<string>();

                        //Adding to main list (output list)
                        lstODG.Add(objOrderDetiailsGrid);
                    }
                    else if (objOrderDetiailsGrid.LineType == "TAX")
                    {
                       

                        //Adding to main list (output list)
                        lstODG.Add(objOrderDetiailsGrid);
                    }
                    else if (objOrderDetiailsGrid.LineType == "DUE")
                    {
                        

                        //Adding to main list (output list)
                        lstODG.Add(objOrderDetiailsGrid);
                    }

               //End of for loop
            }
            int intHighSizes=0;
            for (int intLoop = 0; intLoop < lstODG.Count; intLoop++)
            {
                if(lstODG[intLoop].Sizes.Count>intHighSizes)
                {
                    intHighSizes = lstODG[intLoop].Sizes.Count;
                }
            }
            for (int intLoop = 0; intLoop < lstODG.Count; intLoop++)
            {
               
                    for (int intInner = lstODG[intLoop].Sizes.Count; intInner < intHighSizes; intInner++)
                    {
                        lstODG[intLoop].Sizes.Add("");
                    }
               
            }
            return lstODG;
        }


        public static JsonResult Arrange(Order order, Boolean orderedOnly = true)
        {
            var json = new JsonResult()
            {
                Data = RenderGridLines(order, orderedOnly),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                ContentType = "text/json"
            };

            return json;
        }
    }
    public class OrderDetiailsGrid
        {
            public string LineType { get; set; }
            public string Id { get; set; }
            public string DetailId { get; set; }
            public string Product { get; set; }
            public string Description { get; set; }
            public string Colour { get; set; }
            public List<String> Sizes { get; set; }
            public string UnitPrice { get; set; }
            public string Quantity { get; set; }
            public string Total { get; set; }
        }
}
