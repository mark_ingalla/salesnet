﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Ap21API;
using SalesNet.WebUI.Classes;
using SalesNet.WebUI.Controllers;
using SalesNet.WebUI.Infrastructure;

namespace SalesNet.WebUI
{
    
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
			filters.Add(new SetupSessionAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Products",
                "Products",
                new {controller = "Product", action = "Index"});

            routes.MapRoute(
                "LogIn",
                "LogIn",
                new {controller = "Session", action = "LogIn"});

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new {controller = "Home", action = "Index", id = UrlParameter.Optional} // Parameter defaults
            );
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
            ControllerBuilder.Current.SetControllerFactory(new NinjectControllerFactory());
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            if (HttpContext.Current != null)
            {
                Exception ex = HttpContext.Current.Server.GetLastError();
                Server.ClearError();
                var routeData = new RouteData();
                routeData.Values["controller"] = "Error";
                routeData.Values["action"] = "Index";

                if (ex is AP21APIException)
                {
                    routeData.Values["errormsg"] = ((AP21APIException)ex).Message;
                    routeData.Values["errorcode"] = ((AP21APIException)ex).ErrorCode;                   
                }
                else
                {
                    if (!String.IsNullOrEmpty(AppSession.CurrentSession.Current.CurrentError))
                    {
                        routeData.Values["errormsg"] = AppSession.CurrentSession.Current.CurrentError;
                        AppSession.CurrentSession.Current.CurrentError = String.Empty;
                    }
                    else
                    {
                        if (ex != null) routeData.Values["errormsg"] = ex.Message;
                        if (ex.InnerException != null) routeData.Values["innererrormsg"] = ex.InnerException.Message;
                    }
                }

                IController errorController = new ErrorController();
                errorController.Execute(new RequestContext(new HttpContextWrapper(Context), routeData)); 
            }
        } 

    }
}