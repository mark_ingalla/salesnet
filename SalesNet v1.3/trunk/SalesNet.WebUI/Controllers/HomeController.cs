﻿using System.Web.Mvc;
using SalesNet.WebUI.Infrastructure;

namespace SalesNet.WebUI.Controllers
{

    [EnhancedAuthorizedAttribute]
    public class HomeController : BaseController
    {
        private readonly IAppSessionWrapper sessionWrapper;

        public HomeController(IAppSessionWrapper sessionWrapper)
        {
            this.sessionWrapper = sessionWrapper;
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}
