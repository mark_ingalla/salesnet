﻿
using System.Web.Mvc;

namespace SalesNet.WebUI.Controllers
{
    public class ErrorController : BaseController
    {
        public ActionResult Index(string errormsg, string errorcode)
        {
            TempData["errormsg"] = errormsg;
            TempData["errorcode"] = errorcode;
            TempData["innererrormsg"] = RouteData.Values["innererrormsg"];

            return View();
        }
    }
}
