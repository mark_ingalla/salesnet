﻿using System;
using System.Configuration;
using System.Net.Mail;
using System.Web.Mvc;
using Ap21API.Entities.Customer;
using SalesNet.Domain.Implementations;
using SalesNet.Domain.Interfaces;
using SalesNet.WebUI.Helpers;
using SalesNet.WebUI.Infrastructure;
using SalesNet.WebUI.Models;
using SalesNet.Domain.Entities;

namespace SalesNet.WebUI.Controllers
{

    public class AccountController : BaseController
    {
        private readonly IAppSessionWrapper sessionWrapper;
        private readonly IEmailHelper resetPasswordEmailServce;

        public AccountController(IAppSessionWrapper sessionWrapper, IEmailHelper emailHelper)
        {
            this.sessionWrapper = sessionWrapper;
            resetPasswordEmailServce = emailHelper;
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [EnhancedAuthorizedAttribute]
        public ActionResult Edit()
        {
            var user = Ap21API.Entities.Person.Person.Find(id: sessionWrapper.Current.Id);

            var viewModel = new EditAccountViewModel() { Email = user.Contacts.Email };

            return View(viewModel);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [EnhancedAuthorizedAttribute]
        public ActionResult Edit(string password)
        {
            var apiPerson = Ap21API.Entities.Person.Person.Find(id: sessionWrapper.Current.Id);
            var person = PersonFactory.Create(apiPerson);

            person.ChangePassword(password);

            FlashNotice("Password successfully updated");
            return RedirectToRoute("Default", new { controller = "Home", action = "Index" });
        }

        [HttpGet]
        [EnhancedAuthorizedAttribute(AllowAll = true)]
        public ActionResult RecoverPassword()
        {
            return View();
        }

        [HttpPost]
        [EnhancedAuthorizedAttribute(AllowAll = true)]
        public ActionResult RecoverPassword(string email)
        {
            const string RecoverModule = "RecoverPassword";
            if (email.IsValidEmail())
            {
                try
                {
                    resetPasswordEmailServce.ResetPassword(email);
                }
                catch (EmailRecipientCustomerAcctSuspendedException acctSuspendedException)
                {
                    FlashError(acctSuspendedException.Message, RecoverModule);
                    return View();
                }
                catch (EmailRecipientNotFoundException recipientNotFoundException)
                {
                    FlashError(recipientNotFoundException.Message, RecoverModule);
                    return View();
                }
                catch (ArgumentNullException)
                {
                    FlashError("There is no 'from' field specified. Should be noreply@SalesNet.com", RecoverModule);
                    return View();
                }
                catch (SmtpFailedRecipientException)
                {
                    FlashError("The specified email address could not be reached", RecoverModule);
                    return View();
                }
                catch (ConfigurationErrorsException e)
                {
                    FlashError(string.Format("An error has occurred : {0} ", e.BareMessage.Replace("'", "\"")), RecoverModule);
                    return View();
                }
                catch (Exception e)
                {
                    FlashError(string.Format("An error has occurred : {0} ", e.Message.Replace("'", "\"")), RecoverModule);
                    return View();
                }

                FlashNotice("We have sent you an email with the new password","Login");
                return RedirectToRoute(new { controller = "Session", action = "LogIn" });
            }

            FlashError("Invalid email address");
            return View();
        }

        public PartialViewResult RenderAccountDetails()
        {
            if (sessionWrapper.Current.Id != null)
            {
                var customer = Customers.Find(startRow: 1, pageRows: 9999);
                ViewBag.CustomerCount = customer.Count;
            }
            else
            {
                ViewBag.CustomerCount = 0;
            }
            return PartialView("_AccountDetails");
        }
    }
}
