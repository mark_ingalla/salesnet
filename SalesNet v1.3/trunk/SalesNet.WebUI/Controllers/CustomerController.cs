﻿using System;
using System.Web.Mvc;
using Ap21API.Entities.Customer;
using SalesNet.WebUI.Classes;
using SalesNet.WebUI.Infrastructure;
using System.Linq;
namespace SalesNet.WebUI.Controllers
{
    [NoCache]
    [EnhancedAuthorizedAttribute]
    public class CustomerController : BaseController
    {
        private readonly IAppSessionWrapper session;
        private Customer customer;
        private CustomerCreditStatus creditStatus;

        public CustomerController(IAppSessionWrapper sessionWrapper)
        {
            this.session = sessionWrapper;
        }

        public ActionResult Index()
        {
            var customers = Customers.Find(startRow: 1, pageRows: 9999);

            if (customers.Count > 0)
            {
                if (!session.Current.IsAgent)
                {
                    customer = customers[0];
                    session.Current.CustomerId = customer.Id;
                    session.Current.CustomerName = customer.Name;
                    session.Current.IsAgent = false;
                    creditStatus = new CustomerCreditStatus(customer);

                    if(!creditStatus.IsNormal)
                    {
                        if (!creditStatus.CanContinue)
                        {
                            if (TempData["PreviousController"] != null && TempData["PreviousController"].ToString() == "Session")
                            {
                                FlashError("Login unsuccessful: Account on permanent suspend","Login");
                                return RedirectToRoute(new { controller = "Session", action = "LogIn" });
                            }
                        }
                        
                        AlertCreditStatus();    
                    }

                    return RedirectToRoute(new {controller = "Home", action = "Index"});
                }
                else
                {
                    return View(customers);
                }
            }
            else
            {
                return RedirectToRoute(new { controller = "Error", action = "Index", errormsg = "No Customer set up in AP21 system", errorcode = "5046" });
            }
        }

        private void AlertCreditStatus()
        {
            
            FlashWarning(description: creditStatus.Message());
            //if (creditStatus.CanContinue)
            //{
            //    FlashNotice(description: creditStatus.Message());
            //}
            //else
            //{
            //    FlashWarning(description: creditStatus.Message());
            //}
        }

        [HttpGet]
        public PartialViewResult CustomerList(Customers customers)
        {
            if (customers == null)
                customers = Customers.Find(startRow: 1, pageRows: 9999);

            return PartialView(customers);
        }

        [HttpGet]
        public JsonResult SearchCustomerList(string searchtext)
        {
            
            var  customers = Customers.Find(startRow: 1, pageRows: 9999).Customer.Where(x=>x.Name.ToLower().Contains(searchtext.ToLower()));
            return Json(customers,JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult RenderCustomerList()
        {
            var customers = Customers.Find(startRow: 1, pageRows: 9999);
            ViewBag.CustomerId = session.Current.CustomerId;
            
            return PartialView("CustomerList", customers);
            
        }

        public ActionResult RedirectToHome(FormCollection fc)
        {
            session.Current.CustomerId = fc.Get("selectedCustomer");

            if(!String.IsNullOrEmpty(session.Current.CustomerId))
            {
                customer = Customer.Find(id: session.Current.CustomerId);
                session.Current.CustomerName = customer.Name;
                session.Current.IsAgent = true;

                creditStatus = new CustomerCreditStatus(customer);

                if (!creditStatus.IsNormal)
                {
                    AlertCreditStatus();
                }

                return RedirectToRoute(new { controller = "Home", action = "Index" });
            }
            else
            {
                FlashNotice("Please select a customer");
                return RedirectToAction("Index");
            }           
        }

        public ActionResult SelectCustomer(string id)
        {
            session.Current.CustomerId = id;

            if (!String.IsNullOrEmpty(session.Current.CustomerId))
            {
                customer = Customer.Find(id: session.Current.CustomerId);
                session.Current.CustomerName = customer.Name;
               // session.Current.IsAgent = true;

                creditStatus = new CustomerCreditStatus(customer);
                //added by raju for regarding #120 to prevent suspended accounts from login on 9 feb 2013 
                if (!session.Current.IsAgent)
                {
                    if (customer.CreditStatusFlag == 1)
                    {
                        if (TempData["PreviousController"] != null && TempData["PreviousController"].ToString() == "Session")
                        {
                            FlashError("Login unsuccessful: Account on permanent suspend", "Login");

                            return RedirectToRoute(new { controller = "Session", action = "LogIn" });
                        }
                    }
                }
                //end
                else if (!creditStatus.IsNormal)
                {
                    AlertCreditStatus();
                }

                var landingpage = System.Configuration.ConfigurationManager.AppSettings["LandingPage"];
                if (landingpage != null && landingpage == "*Products*")
                    return RedirectToRoute(new { controller = "Products", action = "Index" });

                return RedirectToAction("Index", "Order");

                //var customers = Customers.Find(startRow: 1, pageRows: 9999);
                //if (customers.Count == 1)
                //{
                //    ViewBag.CustomerId = customers[0].Id;
                //    return RedirectToAction("SelectCustomer", "Customer", new { id = ViewBag.CustomerId });

                //}

                //return RedirectToRoute(new { controller = "Home", action = "Index" });
            }
            else
            {
                FlashNotice("Please select a customer");
                return RedirectToAction("Index");
            }   
        }

        [HttpGet]
        public JsonResult Locations()
        {
            var customer = Customer.Find(session.Current.CustomerId);
            var locations = customer.Locations;

            return Json(locations, JsonRequestBehavior.AllowGet);
        }
    }
}
