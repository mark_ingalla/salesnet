﻿using System;
using System.Web;
using System.Web.Mvc;
using Ap21API;
using Ap21API.Entities.Customer;
using SalesNet.WebUI.Infrastructure;
using SalesNet.Domain.Interfaces;
using SalesNet.WebUI.Models;
using System.Web.Security;

namespace SalesNet.WebUI.Controllers
{
    [EnhancedAuthorized(AllowAll = true)]
    public class SessionController : BaseController
    {
        private readonly SalesNetMemberShipProvider customMembershipProvider;

        public SessionController(IEmailHelper emailHelper, MembershipProvider provider)
        {
            this.customMembershipProvider = (SalesNetMemberShipProvider)provider;
        }

        [HttpGet]
        public ActionResult LogIn()
        {
            try
            {
                if (TempData["lastUserLoggedIn"] == null)
                    ViewData["lastUserLoggedIn"] = Request.Cookies["lastUserLoggedIn"].Value;
                else ViewData["lastUserLoggedIn"] = TempData["lastUserLoggedIn"];
            }
            catch (NullReferenceException)
            {
                ViewData["lastUserLoggedIn"] = string.Empty;
            }

            return View();
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult LogIn(LogInViewModel loginModel)
        {
            try
            {
                if (customMembershipProvider.LogIn(loginModel.Email, loginModel.Password))
                {
                    Response.AppendCookie(new HttpCookie("lastUserLoggedIn", loginModel.Email) { Expires = DateTime.Today.AddDays(90.0) });
                    TempData["PreviousController"] = "Session";
                    //return RedirectToRoute(new { controller = "Customer", action = "Index" });
                    //var landingpage = System.Configuration.ConfigurationManager.AppSettings["LandingPage"];
                    //if (landingpage != null && landingpage == "*Products*")
                    //    return RedirectToRoute(new {controller = "Products", action = "Index"});


                    //var customers = Customers.Find(startRow: 1, pageRows: 9999);
                    //if (customers.Count == 1)
                    //{
                    //    ViewBag.CustomerId = customers[0].Id;
                    //    return RedirectToAction("SelectCustomer", "Customer", new {id = ViewBag.CustomerId});

                    //}
                   

                    return RedirectToRoute(new {controller = "Home", action = "Index"});
                }
            }
            catch (AP21APIException ex)
            {
                if(ex.ErrorCode == 5013)
                    FlashNotice("Login unsuccessful: This email address is set up for more than one person in Apparel 21", "Login");
                else
                    FlashError("Login unsuccessful: " + ex.Message,"Login");
                TempData["lastUserLoggedIn"] = loginModel.Email;
                return RedirectToAction("LogIn");
            }

            return RedirectToAction("LogIn");
        }


        [HttpGet]
        public ActionResult LogOut()
        {
            customMembershipProvider.LogOut();
            HttpCookie myCookie = new HttpCookie("MyCookie");
            myCookie.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(myCookie);
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("LogIn");
        }
    }
}
