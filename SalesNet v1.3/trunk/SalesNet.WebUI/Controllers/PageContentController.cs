﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SalesNet.WebUI.Infrastructure;
using SalesNet.WebUI.Classes;

namespace SalesNet.WebUI.Controllers
{
    public class PageContentController : BaseController
    {
        //
        // GET: /PageContent/
         private readonly IAppSessionWrapper sessionWrapper;

         public PageContentController(IAppSessionWrapper sessionWrapper)
        {
            this.sessionWrapper = sessionWrapper;
        }
        public ActionResult Index(int id)
        {
            TempData["ActivePage"] = "Page Content" + id; ;
            //int id = Convert.ToInt32(Request.QueryString["id"]);
            var varPageContent = SalesNet.Domain.Entities.WebPageContents.GetPageContents(int.Parse(AppSession.CurrentSession.Current.Id));
            //ViewBag.Content = varPageContent.WebPageContent[id - 1].Content;
            //ViewBag.Name = varPageContent.WebPageContent[id - 1].Name;
            return View(varPageContent.WebPageContent[id - 1]);
        }

    }
}
