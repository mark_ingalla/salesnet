﻿using System;
using System.Web.Mvc;
using Ap21API.Entities.Customer;
using Ap21API.Entities.Order;
using SalesNet.Domain.Entities;
using SalesNet.WebUI.DataSetGenerators;
using SalesNet.WebUI.Infrastructure;
using SalesNet.WebUI.Models;
using SalesNet.WebUI.Helpers;
using SalesNet.WebUI.Classes;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using Order = Ap21API.Entities.Order.Order;


namespace SalesNet.WebUI.Controllers
{  
    [NoCache]
    [EnhancedAuthorizedAttribute]
    public partial class OrderController : BaseController
    {
        private CustomerCreditStatus creditStatus;
        private readonly IAppSessionWrapper session;

        public OrderController(IAppSessionWrapper appSessionWrapper)
        {
            session = appSessionWrapper;
        }

        

        public ViewResult Index()
        {
            ViewBag.CustomerId = session.Current.CustomerId;
            TempData["ActivePage"] = "Order History";
            

            return View();
        }

        [ValidateInput(false)]
        public PartialViewResult HistoryIndex(string trans_num = null, string ref_num = null, string created_date_from = null,
            string created_date_to = null, string product_code_desc = null, string order_type = null)
        {
            var searchCriteria = new OrderSearchViewModel(trans_num, ref_num, created_date_from,
            created_date_to, product_code_desc, order_type);
            return PartialView(searchCriteria);
        }

        [ValidateInput(false)]
        public PartialViewResult HistoryItem(string trans_num = null, string ref_num = null, string created_date_from = null,
            string created_date_to = null, string product_code_desc = null, string order_type = "Orders", int page = 1)
        {
            
            const int pageRow = 20;
            int startRow = (page - 1) * pageRow + 1;	

            try
            {
                ValidateSearchCriteria(ref trans_num, ref ref_num, ref created_date_from, ref created_date_to, ref product_code_desc, ref order_type);
            }
            catch (System.FormatException)
            {
                return PartialView();
            }

            var searchCriteria = new OrderSearchViewModel(trans_num, ref_num, created_date_from, 
                created_date_to,product_code_desc, order_type);

            var orderRepository = Orders.Find(createdDateFrom: created_date_from, createdDateTo: created_date_to, customerReference: ref_num, pageRows: pageRow,
                                                 productFilter: product_code_desc, startRow: startRow,
                                                 transactionNumber: trans_num, transactionType: order_type);


            if (orderRepository!=null && orderRepository.Order.Count>0)
            {
                int PageCurrent = (int)Math.Ceiling((decimal)orderRepository.PageStartRow / pageRow);
                var viewModel = orderRepository.Order.ToPagedList(PageCurrent - 1, pageRow,
                                                                  orderRepository.TotalRows, searchCriteria.ToDictionary());
                return PartialView(viewModel); 
            }
            else
            {
                return PartialView();
            }
        }

        private void ValidateSearchCriteria(ref string trans_num, ref string ref_num, ref string created_date_from,
            ref string created_date_to, ref string product_code_desc, ref string order_type)
        {            
            if (trans_num == "" || trans_num == "Trans.#")
            {
                trans_num = null;
            }

            if (ref_num == "" || ref_num == "Ref #")
            {
                ref_num = null;
            }

            if (created_date_from == "" || created_date_from == "Created From")
            {
                created_date_from = null;
            }

            if (created_date_to == "" || created_date_to == "Created To")
            {
                created_date_to = null;
            }

            if (product_code_desc == "" || product_code_desc == "Product Code/Desc")
            {
                product_code_desc = null;
            }

            if (order_type == "")
            {
                order_type = null;
            }

            if (trans_num != null)
            {
                Regex regex = new Regex(@"^[0-9]+$");
                bool b = regex.IsMatch(trans_num);
                if (!b)
                    throw new System.FormatException();
            }

            if (created_date_from != null)
            {
                created_date_from = String.Format("{0:yyyy-MM-dd}", Convert.ToDateTime(created_date_from));
            }

            if (created_date_to != null)
            {
                created_date_to = String.Format("{0:yyyy-MM-dd}", Convert.ToDateTime(created_date_to));
            }
        }

        public PartialViewResult CurrentOrders()
        {
			PutCurrentOrderInViewBag();
			return PartialView();
        }

        [HttpPost]
        public ActionResult CurrentOrders(CurrentOrderViewModel model)
        {
            return RedirectToAction("Index", "Home");
        }

		public PartialViewResult CurrentOrdersDetail()
		{
			PutCurrentOrderInViewBag();
			return PartialView();
		}

        [HttpGet]
        public ActionResult DeleteOrders()
        {
            try
            {
                Domain.Entities.Order.DeleteCurrentOrder();
            }
            catch (Exception e)
            {
                FlashError(e.Message);
            }

			PutCurrentOrderInViewBag();
            return PartialView("CurrentOrders");
        }

        [HttpGet]
        public void DeleteOrdersFromFinalisePage()
        {
            try
            {
                Domain.Entities.Order.DeleteCurrentOrder();
            }
            catch (Exception e)
            {
                FlashError(e.Message);
            }
        }

        [HttpGet]
        public ActionResult RemoveOrder(string orderDetailId)
        {
            var model = new MessageModel();

            try
            {
                Domain.Entities.Order.DeleteCurrentSingleOrder(orderDetailId);
                model.MessageCode = 3;
            }
            catch (Exception e)
            {
                model.Error = e.Message;
            }

			PutCurrentOrderInViewBag();
            return PartialView("CurrentOrders", model);
        }

        [HttpGet]
        public void RemoveOrderFromFinalisePage(string orderDetailId)
        {
            var model = new FinaliseOrderViewModel();

            try
            {
                Domain.Entities.Order.DeleteCurrentSingleOrder(orderDetailId);
                model.MessageModel.MessageCode = 3;
            }
            catch (Exception e)
            {
                model.MessageModel.Error = e.Message;
            }
        }


        private void AlertCreditStatus()
        {
            // used to disable/enable the submit link
            TempData["EnableSubmit"] = creditStatus.CanContinue;

            if(creditStatus.CanContinue)
            {
                FlashWarning(description: creditStatus.Message()); 
            }else
            {
                FlashError(description: creditStatus.Message());
            }    
        }

        [ValidateInput(false)]
        public ActionResult savedelivery(string id, string customerReference, string deliveryInstructions, string specialInstructions)
        {
            try
            {

                Domain.Entities.Order.SaveDeliveryDetails(customerReference, deliveryInstructions, specialInstructions);
                
              //FlashNotice("Delivery Details Saved successfully");                
                //return RedirectToAction("FinaliseOrder");
              return RedirectToAction("Index", "Product");
            }
            catch (Exception e)
            {
                FlashError(e.Message);

                return RedirectToAction("FinaliseOrder");
            }
        }
        
        [ValidateInput(false)]
        
        public ActionResult SendOrder(string id, string customerReference, string deliveryInstructions, string specialInstructions)
        {
            try
            {
                Domain.Entities.Order.FinaliseCurrentOrder(customerReference, deliveryInstructions, specialInstructions);
                
                FlashNotice("Order has been sent to process successfully");

                return RedirectToAction("OrderDetails", new { id = id });
               
            }
            catch (Exception e)
            {
                FlashError(e.Message);

                return RedirectToAction("FinaliseOrder");
            }         
        }
        

        [HttpPut]
        public void ChangeDeliveryAddress(string id)
        {
            Domain.Entities.Order.ChangeDeliveryAddress(id);
        }

        [HttpGet]
        public ActionResult ChangeDeliveryAddress1(string id)
        {
            Domain.Entities.Order.ChangeDeliveryAddress(id);
            return RedirectToAction("FinaliseOrder");
        }
        [HttpGet]
        public ActionResult FinaliseOrder()
        {
            var model = Domain.Entities.Order.CurrentOrder ?? new Order();
            var customer = Customer.Find(AppSession.CurrentSession.CustomerId);
            creditStatus = new CustomerCreditStatus(customer);
            if (customer != null)
            {
                ViewBag.Locations = customer.Locations.Location;
            }
            if(!creditStatus.IsNormal)
            {
                AlertCreditStatus();
            }
            TempData["ActivePage"] = "ViewCart";
            return View(model); 
        }

        public PartialViewResult FinaliseOrderDetail()
        {
            TempData["customerReference"] = TempData["customerReference"] ?? String.Empty;
            TempData["deliveryInstructions"] = TempData["deliveryInstructions"] ?? String.Empty;
            TempData["specialInstructions"] = TempData["specialInstructions"] ?? String.Empty;

            FinaliseOrderViewModel model = new FinaliseOrderViewModel()
                                               {
                                                   OrderModel = Domain.Entities.Order.CurrentOrder ?? new Order(),
                                                   MessageModel = new MessageModel()
                                               };

            TempData["ActivePage"] = "ViewCart";
            return PartialView(model);
        }

        [HttpGet]
        public ActionResult OrderDetails(string id)
        {
            if (Request.QueryString["OrderHistory"] == "Visited")
            {
                TempData["OrderHistory"] = "Visited";
            }
            if (id.Contains(".json"))
            {
                id = id.Replace(".json", "");
            }

            var order = Order.Find(id);

            if (Request.IsAjaxRequest())
            {
                return OrderDetailsGridDataSet.Arrange(order);
            }

            SalesNet.WebUI.DataSetGenerators.OrderDetailsGridDataSet.OrderGridLine[] OrderGridRows = OrderDetailsGridDataSet.GetGridLines(order);
            

          //  ViewData["GridLines"] = OrderDetailsGridDataSet.GetGridLines(order);
           // ViewData["NewGridLines"] = OrderDetailsGridDataSet.GetOrderDetiailsGridList(order);
            return View(order);
        }
     
        

        [HttpPost]
        public ActionResult AddToOrder(string productId, string isFromFinalizeOrder, List<Domain.Entities.Order.ProductQuantity> productQuantities)
        {
            var messageModel = new MessageModel();

            try
            {    
               messageModel.MessageCode = this.UpdateCurrentOrder(productId, productQuantities);
            }
            catch (Exception ex)
            {
                messageModel.Error = ex.Message;
            }
            
            return Request.IsAjaxRequest() ? AjaxAddToOrderResult(isFromFinalizeOrder, messageModel) : AddToOrderResult(isFromFinalizeOrder, messageModel);
        }

        private ActionResult AjaxAddToOrderResult(string isFromFinalizeOrder, MessageModel messageModel)
        {
            if (isFromFinalizeOrder == "y")
            {
                var model = new FinaliseOrderViewModel()
                {
                    OrderModel = Domain.Entities.Order.CurrentOrder ?? new Order(),
                    MessageModel = new MessageModel()
                };

                return PartialView("FinaliseOrderDetail", model);
            }
            else
            {
                //PutCurrentOrderInViewBag();
                Order order = SalesNet.Domain.Entities.Order.CurrentOrder ?? new Order();
                AppSession.CurrentSession.CurrentOrderCurrencyFormat = order.Currency.Format;
                ViewBag.CurrentOrder = order;
                TextFormatHelper.FormatCurrency(order.Ordered.Discount, order.Currency.Format);
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
           
                sb.Append("<script type = 'text/javascript'>");
                sb.Append("$('#spanQuantity').html('( "+ order .Ordered.Quantity+" )');");
                sb.Append("$('#tdOrderQty').html('" + order.Ordered.Quantity + "');");
                sb.Append("$('#tdDiscount').html('" + TextFormatHelper.FormatCurrency(order.Ordered.Discount, order.Currency.Format) + "');");
                sb.Append("$('#tdTax').html('" + TextFormatHelper.FormatCurrency(order.Ordered.Tax, order.Currency.Format)+ "');");
                sb.Append("$('#tdDue').html('" + TextFormatHelper.FormatCurrency(order.Ordered.Value, order.Currency.Format) + "');");
                sb.Append("$('#tdGross').html('" + TextFormatHelper.FormatCurrency(order.Ordered.Gross, order.Currency.Format) + "');");
                if (order.Ordered.Quantity <= 0)
                {                    
                        sb.Append(@"$('#tdEditCart').html(""<a  href='#' style='border-bottom-style:none;color:White;cursor:not-allowed;'>View Cart</a>"")");
                }
                else
                {
                    var myurl = Url.Action("FinaliseOrder", "Order");
                    
                    sb.AppendFormat(
                        @"$('#tdEditCart').html(""<a  href='{0}' style='border-bottom-style:none;color:White;'>View Cart</a>"")",
                        myurl);

                }
                sb.Append("</script>");
                return JavaScript(sb.ToString());
               // return RedirectToAction("Index", "Product");
                //return PartialView("CurrentOrders", messageModel);
            }
        }

        private ActionResult AddToOrderResult(string isFromFinalizeOrder, MessageModel messageModel)
        {
            if (isFromFinalizeOrder == "1")
            {
                return PartialView("FinaliseOrder");
            }
            else
            {
                if (messageModel.Error != null)
                {
                    FlashError(messageModel.Error);
                }

                return RedirectToAction("Index", "Product");
            }
        }

        public int UpdateCurrentOrder(string productId, List<Domain.Entities.Order.ProductQuantity> productQuantities)
        { 
            //order detail + product + currency info
            var orderDetailProduct = new OrderDetailProduct(productId, true);

            return Domain.Entities.Order.UpdateCurrentOrder(orderDetailProduct, productQuantities);
        }

		private void PutCurrentOrderInViewBag()
		{
			Order order = SalesNet.Domain.Entities.Order.CurrentOrder ?? new Order();
			AppSession.CurrentSession.CurrentOrderCurrencyFormat = order.Currency.Format;
			ViewBag.CurrentOrder = order;
		}

        public PartialViewResult RenderSearch()
        {
            return PartialView("search");
        }

        [HttpGet]
        public JsonResult GetCurrentOrderQty()
        {
            Order myorder = SalesNet.Domain.Entities.Order.CurrentOrder ?? new Order();

            var model = new OrderedResult
                            {

                                Discount = myorder.Ordered.Discount.ToString("0.00") + " " + myorder.Currency.Code,
                                Gross = myorder.Ordered.Gross.ToString("0.00") + " " + myorder.Currency.Code,
                                Quantity = myorder.Ordered.Quantity.ToString(),
                                Tax = myorder.Ordered.Tax.ToString("0.00") + " " + myorder.Currency.Code,
                                Value = myorder.Ordered.Value.ToString("0.00") + " " + myorder.Currency.Code,
                            };
            
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}
