using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Caching;
using System.Web.Configuration;
using System.Web.Mvc;
using Ap21API.Entities.Product;
using Ap21API.Entities.Reference;
using SalesNet.WebUI.DataSetGenerators;
using SalesNet.WebUI.Helpers;
using SalesNet.WebUI.Infrastructure;
using SalesNet.WebUI.Models;
using SalesNet.WebUI.Classes;

namespace SalesNet.WebUI.Controllers
{
    [EnhancedAuthorized]
    [NoCache]
    public class ProductController : BaseController
    {
        private int defaultPageSize = Convert.ToInt16(ConfigurationManager.AppSettings["ProductsPageSize"]);
        private const int pagesToSkip = 1;
        private string searchQuery = String.Empty;       

        public ActionResult Index()
        {
            TempData["ActivePage"] = "Product";
            return View();
        }
       
        private ReferenceTree GetReferenceTree()
        {
            string currentCustomerId = AppSession.CurrentSession.Current.CustomerId;
            string previousCustomerId = (String)HttpRuntime.Cache.Get("customerId");
            ReferenceTree referenceTree = null;

            // check the cached customer id
            if (!String.IsNullOrEmpty(previousCustomerId) &&
                (String.Compare(currentCustomerId, previousCustomerId, true) == 0))
                referenceTree = (ReferenceTree)HttpRuntime.Cache.Get("styleReferenceTree");

            if (!String.IsNullOrEmpty(currentCustomerId) && 
                !(String.Compare(currentCustomerId, previousCustomerId, true) == 0))
                HttpRuntime.Cache.Insert("customerId", currentCustomerId);

            // retrieve the style levels
            if(referenceTree == null)
            {
                var styleLevel1 = ConfigurationManager.AppSettings["style_levels_1"];
                var styleLevel2 = ConfigurationManager.AppSettings["style_levels_2"];
                var styleLevel3 = ConfigurationManager.AppSettings["style_levels_3"];

                referenceTree = ReferenceTree.Find(styleLevel1, styleLevel2, styleLevel3);
                
                HttpRuntime.Cache.Insert("styleReferenceTree", referenceTree);
            }
            
            return referenceTree;
        }


        [HttpGet]
        public PartialViewResult Search()
        {
            var productSearchViewModel = new ProductCategoryViewModel(GetReferenceTree());

            var styleLevel1 = ConfigurationManager.AppSettings["style_levels_1"];
            var styleLevel2 = ConfigurationManager.AppSettings["style_levels_2"];
            var styleLevel3 = ConfigurationManager.AppSettings["style_levels_3"];

            // styleLevels are class variables
            ViewData["firstStyleLevelName"] = ReferenceType.Find(styleLevel1).Name; 
            if (!String.IsNullOrEmpty(styleLevel2))
            {
                 ViewData["secondStyleLevelName"] = ReferenceType.Find(styleLevel2).Name; 
                if (!String.IsNullOrEmpty(styleLevel3))
                {
                    ViewData["thirdStyleLevelName"] = "All";//Modified by Raju ReferenceType.Find(styleLevel3).Name;
                }
            }
            

            return PartialView(productSearchViewModel);
        }

        [ValidateInput(false)]
        public PartialViewResult ProductList(string product_desc, string category_season, string category_type, string category_product, int? page,string category_name)
        {
            if (product_desc == "" || product_desc == "Code/Description")
            {
                product_desc = null;
            }
            if (category_name!=null)
            {
                ViewData["category_name"] = category_name;
            }

            int currentPageIndex = page.HasValue ? page.Value - pagesToSkip : 0;
            Dictionary<string, string> searchCriteria = GenerateSearchCriteria(product_desc, category_season, category_type, category_product);
            searchQuery = FormatSearchQuery(searchQuery);

            bool showNoStockProducts = bool.Parse(WebConfigurationManager.AppSettings["ShowNoStockProducts"]);

            var products = ProductsSimple.Find(query: searchQuery, styleQuery: product_desc, orderBy: "{Code:asc}", startRow: defaultPageSize * currentPageIndex + 1, pageRows: defaultPageSize, showNoStockProducts: showNoStockProducts);
            var productsAll = products.ProductSimple.ToPagedList(currentPageIndex, defaultPageSize, products.TotalRows, searchCriteria);

            return PartialView(productsAll);
        }
        

        private Dictionary<string, string> GenerateSearchCriteria(string productDescription, string categorySeason, string categoryType, string categoryProduct)
        {
            Dictionary<string, string> searchCriteria = new Dictionary<string, string>();

            if (!string.IsNullOrEmpty(productDescription))
            {
                searchQuery += productDescription + ":";
                searchCriteria.Add("product_desc", productDescription);
            }

            if (!string.IsNullOrEmpty(categorySeason))
            {
                searchQuery += categorySeason + ":";
                searchCriteria.Add("category_season", categorySeason);
            }

            if (!string.IsNullOrEmpty(categoryType))
            {
                searchQuery += categoryType + ":";
                searchCriteria.Add("category_type", categoryType);
            }

            if (!string.IsNullOrEmpty(categoryProduct))
            {
                searchQuery += categoryProduct + ":";
                searchCriteria.Add("category_product", categoryProduct);
            }

            return searchCriteria;
        }

        private static string FormatSearchQuery(string searchQuery)
        {
            return searchQuery.Length > 0 ? searchQuery.Substring(0, searchQuery.Length - 1) : searchQuery;
        }

        public PartialViewResult ProductMenu()
        {
            var productCategoryViewModel = new ProductCategoryViewModel(GetReferenceTree());
            
            return PartialView(productCategoryViewModel);
        }

        [HandleError]
        [OutputCache(Duration = 300, VaryByParam = "id")]
        public PartialViewResult OrderDetail(string id, string isFromFinalizeOrder)
        {
            OrderDetailProductModel product = new OrderDetailProductModel(id, isFromFinalizeOrder);
            ViewBag.Note =  ProductNoteTypeHelper.GetNote(product.Product.Code);

            HttpRuntime.Cache.Insert("productId", id, null, DateTime.Now.AddMinutes(5.0), Cache.NoSlidingExpiration);
            HttpRuntime.Cache.Insert("product", product, null, DateTime.Now.AddMinutes(5.0), Cache.NoSlidingExpiration);
            
            return PartialView("OrderDetail", product);
        }

        [HandleError]
        public ActionResult Skus(string id)
        {
            if (id.Contains(".json")) id = id.Replace(".json", "");
            var product = (OrderDetailProductModel) HttpRuntime.Cache.Get("product");
            var productId = (String)HttpRuntime.Cache.Get("productId");

            if(id.Equals(productId) && product != null)
            {
                return ProductEntryGridDataSet.Arrange(product);
            }else
            {
                product = new OrderDetailProductModel(id, "false");
                return ProductEntryGridDataSet.Arrange(product);
            }
        }
    }
}