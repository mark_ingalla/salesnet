﻿using SalesNet.WebUI.Classes;
using System.Web.Mvc;
using SalesNet.WebUI.Filters;

namespace SalesNet.WebUI.Controllers
{
    //[HandleError(View = "Error")]
    [AP21ExceptionFilter]
    public class BaseController : Controller
    {        
      /// <summary>
      /// Set an error message to display on the page.
      /// </summary>
      /// <param name="message">Error message to display</param>
      /// <param name="description">Slightly more detailed description of the error</param>
      protected void FlashError(string message = "", string description = "")
      {
        TempData["error"] = new Flash() { Message = message, Description = description };
      }

      /// <summary>
      /// Set a message to display on the page.
      /// </summary>
      /// <param name="message">Message to display</param>
      /// <param name="description">Slightly more detailed description</param>
      /// <remarks>Error messages do not dissapear after being shown for a small amount of time.
      /// They stay on the screen until clicked away by the user.</remarks>
      protected void FlashNotice(string message = "", string description = "")
      {
            TempData["notice"] = new Flash() { Message = message, Description = description };
      }

      /// <summary>
      /// Set a warning message to display on the page.
      /// </summary>
      /// <param name="message">Warning message to display</param>
      /// <param name="description">Slightly more detailed description of the warning.</param>
      /// <remarks>Warnings are the same as errors, but shown in a different color to intimate increased seriousness.</remarks>
      protected void FlashWarning(string message = "", string description = "")
      {
        TempData["warning"] = new Flash() { Message = message, Description = description };
      }
    }
}
