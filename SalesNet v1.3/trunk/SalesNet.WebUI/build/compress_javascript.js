// Compress all the project JavaScript files to a single, compressed file

var fso = new ActiveXObject("Scripting.FileSystemObject");
var jsIncludeFile = fso.OpenTextFile("Views/Shared/_JavaScript.Debug.cshtml", 1, false);

// command line to run closure compiler
var closureCmd = "java -jar c:\\tools\\build\\closure_compiler.jar --js_output_file Content\\javascripts\\salesnet.min.js --warning_level=QUIET";

// we need to pull out the path and filenames of all the included JS files in the project
var rx = new RegExp(/\<script.*\@Url\.Content\(\"(.*)\"\).*type\=.*\<\/script\>/i);

while (!jsIncludeFile.AtEndOfStream) {
	var match = rx.exec(jsIncludeFile.ReadLine());

	if (match) {
		closureCmd += " --js " + match[1].replace(/^\~\//, "./")
	}
}

var shell = WScript.CreateObject("WScript.Shell");
var result = shell.Exec(closureCmd);

while (result.Status == 0) {
	WScript.Sleep(100);
}

// when there are errors, or just warnings and no errors, the compiler always returns a status code of 1.
// this even though we asked to show no warnings. so we check also check for an empty error message if all's OK.
var error = result.StdErr.ReadAll();

if (result.Status != 0 && error != "") {
	WScript.StdErr.WriteLine("JavaScript compression failed: " + error);
	WScript.StdErr.WriteLine("Return code: " + result.Status);
	WScript.Quit(result.Status);
}
else {
	WScript.Quit(0);
}
