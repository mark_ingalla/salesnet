﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ap21API.Entities.Product;

namespace SalesNet.WebUI.Models
{
    public class ColorSizeUnit
    {
        public decimal? Quantity { get; set; }
        public SKU Sku { get; set; }
        public int FreeStock
        {
            get
            {
                return Sku.FreeStock;
            }
        }

        public string ColorName { get; set; }
        public string ColorId { get; set; }
        public string Size
        {
            get
            {
                if (Sku.SizeCode == "-")
                {
                    return "Each";
                }
                else 
                {
                    return Sku.SizeCode;
                }
                
            }
        }
    }
}