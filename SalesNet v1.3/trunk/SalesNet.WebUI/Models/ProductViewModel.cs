﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalesNet.WebUI.Models
{
    public class ProductViewModel
    {
        public string Name { get; set; }
        public string Category { get; set; }
    }
}