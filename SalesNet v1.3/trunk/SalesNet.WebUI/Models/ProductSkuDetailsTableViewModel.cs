﻿
using System.Collections.Generic;
using System.Linq;
using Ap21API.Entities.Order;
using Ap21API.Entities;

namespace SalesNet.WebUI.Models
{
    public class ProductSkuDetailsTableViewModel
    {
        public readonly int SizesPerRow = 10;
        public string OrderState { get; set; }
        public Currency OrderCurrency { get; set; }

        public int NumberOfRows
        {
            get
            {
                if (AllUniqueSizes.Count < SizesPerRow)
                {
                    return 1;
                } 
                {
                    return (AllUniqueSizes.Count / SizesPerRow) + (AllUniqueSizes.Count % SizesPerRow != 0 ? 1 : 0);
                } 
            }
        }
        public OrderClrList Colours { get; set; }
        public List<string> AllUniqueSizes { get; set; }

        public ProductSkuDetailsTableViewModel(OrderClrList colours, List<string> sizes)
        {
            this.Colours = colours;
            this.AllUniqueSizes = sizes;
        }

        public List<string> GetSizesForCurrentRow(int currentRow)
        {
            List<string> sizes;

            if (NumberOfRows == 1)
            {
                return AllUniqueSizes;
            }
            sizes = currentRow == NumberOfRows
                ? AllUniqueSizes.GetRange((currentRow - 1) * SizesPerRow, AllUniqueSizes.Count % SizesPerRow == 0 ? SizesPerRow : AllUniqueSizes.Count % SizesPerRow).Select(x => x).ToList()
                        : AllUniqueSizes.GetRange((currentRow - 1)*SizesPerRow, SizesPerRow).Select(x => x).ToList();

            return sizes;
        }
        
        public bool IsFirstColour(OrderClr colour)
        {
            return Colours.IndexOf(colour) == 0;
        }
    }
}