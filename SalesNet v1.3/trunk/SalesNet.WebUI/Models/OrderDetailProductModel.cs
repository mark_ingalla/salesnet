﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ap21API.Entities.Product;
using SalesNet.Domain.Entities;

namespace SalesNet.WebUI.Models
{
    public class OrderDetailProductModel
    {
        public OrderDetailProduct OrderDetailProduct { get; set; }
        public Product Product { get; set; }
        public bool IsExistingOrderDetail { get; set; }
        public List<ColorUnit> Grid { get; set; }
        public decimal ProductPriceFrom { get; set; }
        public decimal ProductPriceTo { get; set; }
        public decimal RRPPriceFrom { get; set; }
        public decimal RRPPriceTo { get; set; }
        public string RrpText { get; set; }
        public decimal Discount { get; set; }
        public string IsFromFinalizeOrder { get; set; }

        public OrderDetailProductModel(string productId, string isFromFinalizeOrder)
        {
            OrderDetailProduct = new OrderDetailProduct(productId);

            if (OrderDetailProduct.Product == null)
            {
                throw new ApplicationException(String.Format("Product Id {0} cannot be Found", productId));
            }
            Product = OrderDetailProduct.Product;
            IsFromFinalizeOrder = isFromFinalizeOrder;

            RrpText = Product.RrpText;

            if (Product.Clrs.Clr.Count > 0)
            {

                var price = from clrs in Product.Clrs.Clr
                            from skus in clrs.SKUs.SKU
                            select skus.Price;
                ProductPriceFrom = price.Min();
                ProductPriceTo = price.Max();

                var rrpprice = from clrs in Product.Clrs.Clr
                            from skus in clrs.SKUs.SKU
                            select skus.RetailPrice;
                RRPPriceFrom = rrpprice.Min();
                RRPPriceTo = rrpprice.Max();
            }
            else
            {
                ProductPriceFrom = 0;
                ProductPriceTo = 0;
                RRPPriceFrom = 0;
                RRPPriceTo = 0;
            }

            #region populate grid

            // Use complex linq to object operations (nested select, left outter join) here to combine 
            // orderDetail and product objects and transform it into a List<List<ColorSizeUnit>> object
            var colorUnitNameComparer = new ColorUnitNameComparer();
            if (OrderDetailProduct.OrderDetail == null)
                // When the product is not in the current order, simply use the product object
                // to do the transformation and set Quantity=0
            {

                Discount = 0;
                IsExistingOrderDetail = false;
                Grid = (from clr in Product.Clrs.Clr
                        orderby clr.Sequence
                        select new ColorUnit
                                   {
                                       Name = (clr.Name=="-"? "Default" : clr.Name),
                                       Id = (clr.Id == "-" ? "Default" : clr.Id),
                                       SkuList = (from sku in clr.SKUs.SKU
                                                  select new ColorSizeUnit
                                                             {
                                                                 Quantity = 0,
                                                                 Sku = sku,
                                                                 ColorName = (clr.Name == "-" ? "Default" : clr.Name),
                                                                 ColorId = (clr.Id == "-" ? "Default" : clr.Id)
                                                             }).ToList()
                                   }).Distinct(colorUnitNameComparer).ToList();
            }

            else
                // When the product is already in the current order, populate the quantity with the 
                // orderDetail class.
            {
                Discount = OrderDetailProduct.OrderDetail.Ordered.Discount;
                IsExistingOrderDetail = true;
                Grid = (from clr in Product.Clrs.Clr
                        join oclr in OrderDetailProduct.OrderDetail.Clrs.Clr on clr.Id equals oclr.ClrId
                        orderby clr.Sequence
                        select new ColorUnit
                                   {
                                       Name = (clr.Name == "-" ? "Default" : clr.Name),
                                       Id = (clr.Id == "-" ? "Default" : clr.Id),
                                       SkuList =
                                           oclr == null
                                               ? (from sku in clr.SKUs.SKU
                                                  select new ColorSizeUnit
                                                             {
                                                                 Quantity = 0,
                                                                 Sku = sku,
                                                                 ColorName = (clr.Name == "-" ? "Default" : clr.Name),
                                                                 ColorId = (clr.Id == "-" ? "Default" : clr.Id )
                                                             }).ToList()
                                               : (from sku in clr.SKUs.SKU
                                                  join osku in oclr.SKUs.SKU on sku.Id equals osku.SkuId
                                                  select new ColorSizeUnit
                                                             {
                                                                 Quantity = osku == null ? 0 : osku.Ordered.Quantity,
                                                                 Sku = sku,
                                                                 ColorName = (clr.Name == "-" ? "Default" : clr.Name),
                                                                 ColorId = (clr.Id == "-" ? "Default" : clr.Id)
                                                             }).ToList()
                                   }).Distinct(colorUnitNameComparer).ToList();
            }


            #endregion
        }
    }

    public class ColorUnitNameComparer : IEqualityComparer<ColorUnit>
    {
        public bool Equals(ColorUnit x, ColorUnit y)
        {
            if (x == null || y == null)
                return false;
            else
                return x.Name == y.Name;
        }

        public int GetHashCode(ColorUnit obj)
        {
            return obj.Name.GetHashCode();
        }
    }

    public class ColorUnit
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public List<ColorSizeUnit> SkuList { get; set; }
    }
}