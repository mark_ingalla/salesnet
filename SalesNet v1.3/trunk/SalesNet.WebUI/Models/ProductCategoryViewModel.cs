﻿using System.Collections.Generic;
using System.Linq;
using Ap21API.Entities.Reference;

namespace SalesNet.WebUI.Models
{
    public class ProductCategoryViewModel
    {
        public List<Reference> Level1 { get; private set; }
        public List<Reference> Level2 { get; private set; }
        public List<Reference> Level3 { get; private set; }
        public string referenceLevel1Type { get; private set; }

        public bool HasProduct { get; private set; }

        public ProductCategoryViewModel(ReferenceTree referenceTree)
        {
            if (referenceTree != null && referenceTree.Reference.Count > 0)
            {
                var referenceNameComparer = new ReferenceNameComparer();

                Level1 = (from reference in referenceTree.Reference
                          orderby reference.ReferenceName
                          select reference).Distinct(referenceNameComparer).ToList();

                Level2 = (from reference1 in Level1
                          from reference2 in reference1.Children
                          orderby reference2.ReferenceName
                          select reference2).Distinct(referenceNameComparer).ToList();

                Level3 = (
                          from reference1 in Level1
                          from reference2 in reference1.Children
                          from reference3 in reference2.Children
                          orderby reference3.ReferenceName
                          select reference3).Distinct(referenceNameComparer).ToList();
            }
            else
            {
                Level1 = new List<Reference>();
                Level2 = new List<Reference>();
                Level3 = new List<Reference>();          
            }
        }
    }

    public class ReferenceNameComparer : IEqualityComparer<Reference>
    {
        public bool Equals(Reference x, Reference y)
        {
            if (x == null || y == null)
                return false;
            else
                return x.ReferenceName == y.ReferenceName;
        }

        public int GetHashCode(Reference obj)
        {
            return obj.ReferenceName.GetHashCode();
        }
    }
}