﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Ap21API.Entities.Order;

namespace SalesNet.WebUI.Models
{
    public class HistoryItemViewModel
    {
        public IList<Order> Orders { get; set; }
        public OrderSearchViewModel SearchCriteria { get; set; }
        public int TotalItemCount { get; set; }
        public int PageRows { get; set; }
        public int PageCurrent { get; set; }
    }
}