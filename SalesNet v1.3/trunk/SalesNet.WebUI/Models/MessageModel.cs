﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalesNet.WebUI.Models
{
    public class MessageModel
    {
        public string Error { get; set; }
        public int MessageCode { get; set; }

        public MessageModel()
        {
            Error = null;
            MessageCode = -2;
        }

        public string Notice
        {
            get 
            {
                switch (MessageCode)
                {
                    case -1:
                        return "You are trying to add an empty order detail.";
                    case 0:
                        return "Order Detail has zero total quantities and has been deleted.";
                    case 1:
                        return "Order Detail has been updated successfully.";
                    case 2:
                        return "Order Detail has been added successfully.";
                    case 3:
                        return "Order Detail has been removed successfully.";
                    case 4:
                        return "Order has been sent to process successfully.";
                    default:
                        return null;
                }
            }
        }
    }
}