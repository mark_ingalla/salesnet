﻿using System.Collections.Generic;
using System.Linq;
using Ap21API.Entities.Order;
using SalesNet.Domain.Extensions;
using Ap21API.Entities;

namespace SalesNet.WebUI.Models
{
    public class ColourAndQuantitiesViewModel
    {
        public string OrderState { get; set; }
        public OrderClr Colour { get; set; }
        private IEnumerable<OrderSKU> Skus { get { return Colour.SKUs.SKU; } }
        public List<string> CurrentRowOfSizes { get; set; }
        public int CurrentRow { get; set; }
        public Currency OrderCurrency { get; set; }

        public ColourAndQuantitiesViewModel(OrderClr colour, List<string> sizes, int currentRow)
        {
            this.Colour = colour;
            this.CurrentRowOfSizes = sizes;
            this.CurrentRow = currentRow;
            RemoveEmptySkus();
        }

        private void RemoveEmptySkus()
        {
            Colour.SKUs.SKU.RemoveAll(x => x.HasNoQuantity());
        }

        private IEnumerable<OrderSKU> SkusInThisRow()
        {
            return (from size in CurrentRowOfSizes
                    join sku in Skus on size equals sku.SizeCode
                    select sku);
        }

        private decimal TotalQuantityForRow()
        {
            return SkusInThisRow().Select(sku => sku.OrderStateBalance(OrderState).Quantity).DefaultIfEmpty().Sum();
        }

        private decimal TotalPriceForRow()
        {
            return SkusInThisRow().Select(sku => sku.OrderStateBalance(OrderState).Quantity*sku.Price).DefaultIfEmpty().Sum();
        }

        public decimal AveragePrice()
        {
            if (TotalQuantityForRow() < 1)
            {
                return 0;
            }

            return TotalPriceForRow()/TotalQuantityForRow();
        }
    }
}