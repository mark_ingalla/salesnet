﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

using Ap21API.Entities.Order;

namespace SalesNet.WebUI.Models
{
    [Serializable]
    public class CurrentOrderViewModel
    {
        public Matric OrderMatric { get; set; }
        public OrderDetails OrderDetails { get; set; }
    }
}
