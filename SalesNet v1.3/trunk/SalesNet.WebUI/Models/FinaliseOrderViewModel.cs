﻿namespace SalesNet.WebUI.Models
{
    public class FinaliseOrderViewModel
    {
        public MessageModel MessageModel { get; set; }
        public Ap21API.Entities.Order.Order OrderModel { get; set; }

        public FinaliseOrderViewModel()
        {
            MessageModel = new MessageModel();
            OrderModel = new Ap21API.Entities.Order.Order();
        }
    }
}