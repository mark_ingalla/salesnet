﻿using Ap21API.Entities.Order;
using Ap21API.Entities;

namespace SalesNet.WebUI.Models
{
    public class OrderDetailsProductsViewModel
    {
        public OrderDetailList OrderDetailList { get; set; }
        public string OrderState { get; set; }
        public Currency OrderCurrency { get; set; }
    }
}