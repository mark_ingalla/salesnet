﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalesNet.WebUI.Models
{
    public class OrderSearchViewModel
    {
        private string product_code_desc;
        private string trans_num;
        private string ref_num;
        private string order_type;
        private string created_date_from;
        private string created_date_to;

        public string OrderType { get; set; }

        public OrderSearchViewModel(string trans_num = null, string ref_num = null, string created_date_from = null,
            string created_date_to = null, string product_code_desc = null, string order_type = null)
        {
            this.product_code_desc = product_code_desc;
            this.trans_num = trans_num;
            this.ref_num = ref_num;
            this.order_type = order_type;
            OrderType = order_type;
            this.created_date_from = created_date_from;
            this.created_date_to = created_date_to;
        }

        public Dictionary<string, string> ToDictionary()
        {
            var searchCriteria = new Dictionary<string, string>();
            searchCriteria.Add("trans_num", trans_num);
            searchCriteria.Add("ref_num", ref_num);
            searchCriteria.Add("created_date_from", created_date_from);
            searchCriteria.Add("created_date_to", created_date_to);
            searchCriteria.Add("product_code_desc", product_code_desc);
            searchCriteria.Add("order_type", order_type);
            return searchCriteria;
        }
    }
}