﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalesNet.WebUI.Models
{
    public class OrderedResult
    {
        public string Discount { get; set; }
        public string Gross { get; set; }
        public string Quantity { get; set; }
        public string Tax { get; set; }
        public string Value { get; set; }
    }
}