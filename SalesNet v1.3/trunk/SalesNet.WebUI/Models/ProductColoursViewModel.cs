﻿
using System.Collections.Generic;
using Ap21API.Entities.Order;

namespace SalesNet.WebUI.Models
{
    public class ProductColoursViewModel
    {
        public OrderClrList Colours { get; set; }
        public List<string> UniqueOrderedSizes { get; set; }

        public ProductColoursViewModel(OrderClrList colours, List<string> sizes)
        {
            this.Colours = colours;
            this.UniqueOrderedSizes = sizes;
        }
    }
}