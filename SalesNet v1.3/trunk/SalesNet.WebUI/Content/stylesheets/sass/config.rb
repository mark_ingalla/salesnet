# Compass configuration
project_type = :stand_alone 
sass_dir = "."
css_dir = "."
sass_options = { :cache => true }
output_style = :compressed