//Ext.require([
//    'Ext.grid.*',
//    'Ext.data.*',
//    'Ext.util.*',
//    'Ext.tip.*',
//    'Ext.state.*'
//]);

Module("Product", function (m) {

    Class("ProductOrderEntry", {
        has: {
            productId: { is: 'rw' },
            grid: { is: 'rw' },
            store: { is: 'rw' },
            model: {
                isa: m.Model,
                is: 'rw'
            },                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
            column_configurer: {
                isa: m.ColumnConfigurer,
                is: 'rw'
            },
            ordered_quantity: {
                isa: m.QuantityValidator,
                is: 'rw'
            },
            archived_records: { is: 'rw' }
        },

        methods: {

            create_grid: function () {
                this.model.create_model();
                this.create_store();
                this.store.load();
            },

            updateTotals: function (edit_event) {
                this.updateColourTotal(edit_event);
                this.updateProductTotal();
            },

            updateColourTotal: function (edit_event) {
                var ordered_difference = (edit_event.value - edit_event.originalValue);
                var current_colour_total = edit_event.record.get("Total");

                edit_event.record.set("Total", current_colour_total + ordered_difference); //update total for colour
            },

            updateProductTotal: function () {
                var total = 0;

                var sumColourTotals = function (row) {
                    total += row.data["Total"] ? row.data["Total"] : 0;
                };
                _.each(this.store.data.items, sumColourTotals);

                $("#totalquantity").html(total);
            },

            populateColourTotals: function () {
                //could call updateProductTotal after

                var product_total = 0;

                var addColourTotals = function (row) {
                    var colour_total = row.data["Total"] * 1;

                    for (field in row.data) {
                        if (field.match(/Size\d{1,2}/)) {
                            var quantity = row.data[field] * 1;
                            colour_total += quantity;
                        }
                    }
                    row.data["Total"] = colour_total;
                    product_total += colour_total;
                };
                _.each(this.store.data.items, addColourTotals); // using underscore.js lib

                $("#totalquantity").html(product_total);
            },

            archive_sizes: function (store) {
                var archive = new Array();
                store.each(function (record) {
                    archive.push(record.copy());
                });
                this.archived_records = archive;
            },

            create_store: function () {

                var proxyUrl = host + 'Product/Skus/' + this.productId + '.json';


                var store = new Ext.data.Store({
                    model: 'ProductOrderQuantity',
                    autoLoad: false,
                    proxy: {
                        type: 'ajax',
                        url: proxyUrl,
                        model: 'ProductOrderQuantity',
                        reader: {
                            type: 'json'
                        }
                    }
                });

                // create load handler
                store.on({
                    'load': {
                        fn: function (store, records, successful, operation, eOpts) {
                            if (successful) {
                                this.archive_sizes(store);
                                this.configure_grid();
                            } else {
                                var fail = successful;
                            }
                        },
                        scope: this
                    }
                });

                Ext.apply(store, { hasStock: function (position) {
                    //return this.getAt(position.row).get("Stock" + (position.column - 1)) > 0; modified by raj there is no Stock-1 or Stock0

                    return this.getAt(position.row).get("Stock" + (position.column + 1)) > 0;
                }
                });

                // add a load mask
                new Ext.LoadMask('product-order-details',
                    {
                        store: store,
                        width: 605,
                        height: 94
                    }
                );

                this.store = store; // make it a class level variable
            },

            configure_grid: function () {

                var out_of_stock = function (record, col) {
                    if (!record) {
                        return false;
                    }
                    return Number(record.data["Stock" + col]) <= 0;
                };
                var next_available = function (record, col) {
                    if (!record) {
                        return false;

                    }
                    //return  record.data["NextAvailable" + col] != "" || record.data["NextAvailable" + col] != null;
                    return record.data["NextAvailable" + col] != "";


                };

                var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
                    clicksToEdit: 1
                });

                var cellModel = Ext.create('Ext.selection.CellModel', {});
                Ext.apply(cellModel, { onEditorTab: SalesNet.grid.onEditorTab });
                Ext.apply(cellModel, { onKeyTab: SalesNet.grid.onKeyTab });

                cellModel.on({
                    'select': {
                        fn: function (cell, record, row, col) {
                            // var size_number = col - 1;
                            var size_number = col + 1;
                            $('#unit_cost').hide();
                            $('#discount-msg').hide();
                            // $('#unit_cost').html("&nbsp"); // clear stock message
                            // $('#discount-msg').html("&nbsp"); // clear stock message

                            //                            if (col < 2 || out_of_stock(record, size_number)) {
                            // if (col < 0 || out_of_stock(record, size_number)) {
                            //                            if (col < 0) {
                            //                                return false;


                            //                            }
                            if (!next_available(record, size_number) && (out_of_stock(record, size_number)) || col < 0) {
                                return false;
                            }
                            else {
                                var stockMessage = new StockMessage(record, size_number, this.archived_records);

                                $('#unit_cost').show();
                                $('#discount-msg').show();
                                $('#unit_cost_list').html(stockMessage.show());
                                $('#discount-msg').html(stockMessage.discountshow());

                                //if (!out_of_stock(record, size_number) && next_available(record, size_number)) {
                                if (!next_available(record, size_number)) {
                                    $("#nextavailable").hide();
                                }
                                record.beginEdit();
                            }
                        },
                        scope: this
                    }
                });

                this.grid = Ext.create('Ext.grid.Panel', {
                    autoDestroy: true,
                    //width: '630px',
                    //                    height: '180px',
                    bodyBorder: false,
                    columnLines: true,
                    enableColumnResize: false,
                    frame: false,
                    layout: 'fit',
                    renderTo: 'product-order-details',
                    selModel: cellModel,
                    //  scroll: !unsupportedIE,
                    store: this.store,
                    id: 'tblProductOrderDetails',
                    title: null,
                    plugins: [cellEditing],
                    viewConfig: {
                        stripeRows: true,
                        listeners: {
                            refresh: function (gridview) {
                                //find the  tables then inject attributes
                                $('#product-order-details').find('.x-grid-view').attr('id', 'rightTbl');
                                $('#rightTbl').css({ 'height': 'auto' });
                                // $('#product-order-details').find('table').attr('id', 'rightTbl');
                                var rightTblHt = $('#rightTbl table').height();
                                if (rightTblHt <= 160) $('#scroller').hide();
                            }
                        }
                    },
                    columns: this.column_configurer.build_columns_from(this.store),
                    width: this.column_configurer.total_of_column_widths()
                    

                });

                //Modified by Sivakumar 31 Dec 2012 start 1
                this.grid.verticalScroller.onElScroll = CustomVerticalScroll;

                function CustomVerticalScroll(event, target) {
                    // debugger;
                    this.wasScrolled = true; // change flag -> show that listener is alive
                    myNewGrid.verticalScroller.fireEvent('bodyscroll', event, target);
                    this.fireEvent('bodyscroll', event, target);
                }
                //debugger;
                //Modified by Sivakumar 31 Dec 2012 end 1

                this.newgrid = Ext.create('Ext.grid.Panel', {
                    id: 'tblProductOrderList',
                    autoDestroy: true,
                    //height: '160px',
                    bodyBorder: false,
                    columnLines: true,
                    enableColumnResize: false,
                    frame: false,
                    layout: 'fit',
                    renderTo: 'product-order-list',
                    //  selModel: cellModel,
                    //  scroll: !unsupportedIE,
                    store: this.store,
                    title: null,
                    //  plugins: [cellEditing],
                    viewConfig: {
                        disableSelection: true,
                        stripeRows: true,
                        listeners: {
                            refresh: function (gridview) {
                                                            
                                $('#product-order-list').find('.x-grid-view').attr('id', 'leftTbl');
                                $('#leftTbl').css({ 'height':'auto' });
                            }
                        }
                    },
                    columns: this.column_configurer.build_columns_colorsize(this.store),
                    width: this.column_configurer.detail_total_of_column_widths()

                    //width: this.column_configurer.total_of_column_widths()
                });
                //Modified by Sivakumar 31 Dec 2012 start 2

                //                //debugger;
                //                if (this.grid.columns.length != undefined && this.grid.columns.length > 11) {
                //                    this.grid.setWidth(630);
                //                }
                //                //debugger;
                //                if (this.grid.store.data.length != undefined && this.grid.store.data.length > 5) {
                //                    // this.grid.setWidth((this.grid.columns.length*45));
                //                    this.newgrid.setHeight(160);
                //                    this.grid.setHeight(170);
                //                    var gridWidth = this.grid.columns.length * 50;
                //                    if (gridWidth < 630) {
                //                        this.grid.setWidth((gridWidth));
                //                    }
                //                    else {
                //                        this.grid.setWidth(630);
                //                    }
                //                }
                //                var myNewGrid = this.newgrid;
                //                myNewGrid.verticalScroller.hide();

                // debugger;
                //Modified by Sivakumar 31 Dec 2012 start 2
                this.grid.on({
                    'edit': {
                        fn: function (editor, edit_event, options) {

                            this.ordered_quantity = new m.QuantityValidator({ edit_event: edit_event, size_headings: _.first(this.archived_records) });

                            if (this.ordered_quantity.is_valid()) {

                                window.ordered_skus.updateSkuQuantity(this.ordered_quantity.sku.sku_id, this.ordered_quantity.ordered_quantity);
                                this.remove_unorderd_skus(edit_event);
                                this.updateTotals(edit_event);
                                editor.context.record.commit(); // we want to commit event unedited fields
                            } else {
                                edit_event.record.reject();
                            }

                            this.set_submit_button_state();

                        },
                        scope: this
                    }
                });

                this.store.clearFilter();
                this.store.filter('LineType', 'COLOUR');
                // debugger;
                this.populateColourTotals(); //these do not come populated from the api, it's a front end job
            },

            set_submit_button_state: function () {

                if (window.ordered_skus.length > 0) {
                    this.ordered_quantity.enable_submit_button();
                } else {
                    this.ordered_quantity.disable_submit_button();
                }
            },

            remove_unorderd_skus: function (edit_event) {
                // debugger;
                var current_value = edit_event.value;
                var edited_row = this.archived_records[(edit_event.rowIdx + 1)];
                var original_value = Number(edited_row.data[edit_event.field]);

                if (current_value === 0 && original_value === 0) {
                    var size_number = _.first(edit_event.field.match(/\d{1,2}/));
                    var sku_id = edit_event.record.get("Sku" + size_number);
                    var index_exists = window.ordered_skus.contains(sku_id);
                    if (index_exists) {
                        var index = Number(index_exists);
                        window.ordered_skus.splice(index, 1);
                    }
                }
            }
        },

        after: {
            initialize: function () {
                //debugger;
                this.column_configurer = new m.ColumnConfigurer();
                this.model = new m.Model();

                // assign helpful functions to the array
                window.ordered_skus = [];
                window.ordered_skus.contains = function (sku_id) {
                    for (var i = 0; i < window.ordered_skus.length; i++) {
                        if (window.ordered_skus[i].SkuId === sku_id) {
                            return String(i); // return string as 0 number is a falsey value
                        };
                    }
                    return null;
                };
                window.ordered_skus.updateSkuQuantity = function (sku_id, updated_ordered_quantity) {

                    var exists = window.ordered_skus.contains(sku_id);

                    if (exists) {
                        window.ordered_skus[exists].Quantity = updated_ordered_quantity; // update
                    } else {
                        window.ordered_skus.push({ SkuId: sku_id, Quantity: updated_ordered_quantity }); // add
                    }
                };
                this.create_grid();


            }
        }
    });
});

