
Module("Product", function (m) {

    Class("ColumnConfigurer", {
        has: {
            grid_width: { is: 'rw' },
            store: { is: 'rw' },
            first_size_not_supported: { is: 'rw' },
            size_heading_row: { is: 'rw' },
            colour_total_column_width: {
                is: 'rw',
                init: 45
            },
            colour_column_width: {
                is: 'rw',
                init: 96
            },
            size_column_width: {
                is: 'rw',
                init: 55
            }
        },

        methods: {

            unsupportedIE: function () {
                return Ext.isIE7 || Ext.isIE8;
            },

            sizes_not_available: function () {
                var errors = [];

                this.store.filter('LineType', 'SIZEHEAD');
                this.size_heading_row = this.store.first().data;

                this.store.each(function (record) {
                    errors.push(record.validate());
                });
                this.store.clearFilter();

                return errors;
            },

            get_first_size_not_supported: function () {
                var sizes_not_available = _.first(this.sizes_not_available()); // list of sizes not available
                var first_size_not_available = sizes_not_available && _.first(sizes_not_available.items); // first size not available
                var match = first_size_not_available ? first_size_not_available.field.match(/\d{1,2}/) : ["31"] // all sizes are available on this product
                return match && Number(_.first(match)); //we want the first and only result, and to convert that to a number
            },

            create_colour_total_column: function () {
                return {
                    align: 'center',
                    text: 'Total',
                    dataIndex: 'Total',
                    draggable: false,
                    height: 40,
                    fixed: this.unsupportedIE(),
                    menuDisabled: true,
                    resizeable: !this.unsupportedIE(),
                    sortable: false,
                    width: this.colour_total_column_width,
                    tdCls: 'noborder'

                }
            },

            create_colour_column: function () {
                return {
                    align: 'left',
                    //                    text: 'Type',
                    text: '',
                    dataIndex: 'Colour',
                    draggable: false,
                    height: 40,
                    //     width: 60,
                    fixed: this.unsupportedIE(),
                    menuDisabled: true,
                    resizeable: !this.unsupportedIE(),
                    sortable: false,
                    width: this.colour_column_width,
                    tdCls: 'noborder',
                    cls: 'noborder'
                }
            },

            hidden_create_colour_total_column: function () {
                return {
                    //   text: 'Total',
                    //  dataIndex: 'Total',
                    //    draggable: false,
                    //      height: 0,
                    //     hideable: false,
                    //  fixed: this.unsupportedIE(),
                    menuDisabled: true,
                    //   resizeable: !this.unsupportedIE(),
                    //    sortable: false,
                    //    width: 10,
                    tdCls: 'hiddenCol',
                    cls: 'hiddenCol',
                    hidden: true

                }
            },

            hidden_create_colour_column: function () {
                return {
                    //text: 'Type',
                    //dataIndex: 'Colour',
                    //    draggable: false,
                    //    height: 0,
                    //    hideable:false,
                    //  fixed: this.unsupportedIE(),
                    menuDisabled: true,
                    //   resizeable: !this.unsupportedIE(),
                    //     sortable: false,
                    //    width: 10,
                    tdCls: 'hiddenCol',
                    cls: 'hiddenCol',
                    hidden: true

                }
            },

            create_size_column: function (field_index, column_header) {

                return {
                    align: 'center',
                    dataIndex: field_index,
                    draggable: false,
                    height: 35,
                    fixed: this.unsupportedIE(), // deprecated in 4.0
                    menuDisabled: true,
                    text: column_header === '-' ? 'Each' : column_header,
                    resizeable: !this.unsupportedIE(),
                    renderer: this.stock_quantity_colour,
                    border: 0,
                    sortable: false,
                    width: this.size_column_width,
                    editor: {
                        xtype: 'numberfield',
                        minValue: 0,
                        maxValue: 999,
                        hideTrigger: true,
                        allowDecimals: false,
                        selectOnFocus: true
                    },
                    tdCls: 'hasborder',
                    cls: 'headhasborder'
                }
            },

            create_size_columns: function () {
                var size_columns = [];

                this.first_size_not_supported = this.get_first_size_not_supported();

                for (i = 1; i < this.first_size_not_supported; i++) {
                    var field_name = 'Size' + i;
                    var column_header = this.size_heading_row[field_name];

                    size_columns.push(this.create_size_column(field_name, column_header));
                }

                return size_columns;
            },
            detail_total_of_column_widths: function () {
                return this.colour_column_width +
                    this.colour_total_column_width;

            },

            total_of_column_widths: function () {
                //                return this.colour_column_width +
                //                        this.colour_total_column_width +
                //                        (this.size_column_width * (this.first_size_not_supported - 1)) +
                //                        4; //padding on the right
                return (this.size_column_width * (this.first_size_not_supported - 1)) +
                        4; //padding on the right
            },

            build_columns_from: function (_store) {

                //inititailise class variable
                this.store = _store;
                var columns = [];
               // columns.push(this.create_colour_column());
                //columns.push(this.create_colour_total_column());
                //  columns.push(this.hidden_create_colour_column());
                //   columns.push(this.hidden_create_colour_total_column());
                columns.push(this.create_size_columns());

                return columns;
            },
            build_columns_colorsize: function (_store) {

                //inititailise class variable
                this.store = _store;
                var columns = [];

                columns.push(this.create_colour_column());
                columns.push(this.create_colour_total_column());
                //  columns.push(this.create_size_columns());

                return columns;
            },

            stock_quantity_colour: function (value, meta, record, rowIndex, colIndex, store, view) {

                value = Number(value);
                // var column = colIndex - 1; // Our first size column in the grid is 2(0 index) but the sizes in the model start at 1 
                var column = colIndex + 1; // Our first size column in the grid is 2(0 index) but the sizes in the model start at 1 
                // only ordered amounts or decreased orders  
                var sku_id = record.get("Sku" + column);
                var exists = window.ordered_skus.contains(sku_id);
                if (exists) {
                    value = window.ordered_skus[exists].Quantity;
                } else if (value <= 0) {
                    value = '&nbsp';
                }

                // set stock available colours and tooltips
                //debugger;
                var stock_available = record.data["Stock" + column];
                var low_stock_threshold = $('#low-stock-threshold').attr('value') * 1;

                //added by raju 16 jan 2013
                var nextAvailable = record.get("NextAvailable" + column);
                //                if (nextAvailable != "" && nextAvailable != null && showIncomingStock != undefined && showIncomingStock != "" && showIncomingStock=="true") {
                //                    meta.style += "background-color: #D1F0FF;"; //blue 
                //                }
                var is_incoming_stock = nextAvailable != "" && nextAvailable != null && showIncomingStock != undefined && showIncomingStock != "" && showIncomingStock == "true";
                if (stock_available) {
                    stock_available = Number(stock_available);
                    //debugger;
                    if (stock_available <= 0) {
                        if (is_incoming_stock) {
                            meta.style += "background-color: #D1F0FF;";
                            //  return '<div data-qtip="' + stock_available + ' available"></div>';
                            return '<div data-qtip="' + stock_available + ' available">' + value + '</div>';
                        } else {
                            meta.style += "background-color: #A00201"; //pale red
                            // return '<div data-qtip="out of stock" ><font style="color:white;font-size:7px;font-weight:bold;">OUT OF</font><br><font style="color:white;font-size:7px;font-weight:bold;">STOCK</font></div>';
                            return '<div data-qtip="out of stock" style="color:white;font-size:8px;">OUT OF<br> STOCK</div>';
                        }

                    }
                    else if (stock_available <= low_stock_threshold) {
                        meta.style += "background-color: #E7B226"; //pale yellow
                        //return '<div data-qtip="' + stock_available + ' available">' + value + '</div>';
                        //  return '<div data-qtip="' + stock_available + ' available" ><span style="color:black;font-size:10px;font-weight:bold;">' + stock_available + ' </span><span style="color:black;font-size:7px;font-weight:bold;">IN STOCK</span>' + '</div>';
                        if (value > 0) {
                            return '<div data-qtip="' + stock_available + ' available">' + value + '</div>';
                        }
                        return '<div data-qtip="' + stock_available + ' available"  style="color:black;font-size:8px;">' + stock_available + '<br>IN STOCK' + '</div>';
                    }
                    else {

                        return '<div data-qtip="available">' + value + '</div>'; // higher than
                    }
                } else {
                    return '<div data-qtip="' + stock_available + ' available">NA</div>';
                    // return value = "N/A";

                }
            }
        }
    });

});