Class("Login", {
	isa : Form,

	before: {
		initialize: function() {
			this.options = {
				closeOnEscape: false,
				dialog: false,
				modal: false,
//				onOpen: function(form) {
//					var $email = $("#email", form.$form);
//					if ($email.val().length == 0) {
//						$email.focus();
//					}
//					else {
//						$("#password", form.$form).focus();
//					}
//				},
//				rules: {
//					email: {
//						required: true,
//						email: true
//					},
//					password: {
//						required: true
//					}
//				},
				selector: '#login_form',
				showProcessing: true
			}
		}
	}
});

$(document).ready(function () {
    $("#login_form").validate({
        onkeyup: false,
    onclick: false,
        rules: {
            password: { required: true },
            email: { required: true, email: true }
        },
        messages: {
            password: { required: "Password is required" },
            email: { required: "Email is required", email: "Invalid Email"}
        },
        errorLabelContainer: "#error-list",
        invalidHandler: function (form, validator) {
            $("#error-list").addClass('error-list');
            $("#error-list").removeClass('notify-error-list');
        }
       // wrapper: "span",
      
//        submitHandler: function (form) {
//            $("#error-list").hide();
//            

//            return false;
//        }
    });
});
