Class("OrderSidebar", {
    after: {
        initialize: function () {
            this.setup_popups();

            $('.products .mini_product_item  a.product_qty_popup_ordersidebar').each(function () {
                $(this).click(function (event) {
                    event.preventDefault();
                    $.get($(this).attr('href'), function (data) {
                        new ProductQty({ html: data, productId: this.url.match(/\d+$/)[0] });
                    });
                });
            });
        }
    },

    methods: {
        setup_popups: function () {
            $('#order_sidebar #mini_order .products .mini_product').live('mousemove', function () {
                var $this = $(this);
                var order_id = $this.data('order');

                $this.qtip({
                    id: order_id,
                    overwrite: false,
                    prerender: true,

                    position: {
                        at: 'left center',
                        my: 'right center',
                        container: $('#mini_order'),
                        target: $this
                    },

                    content: {
                        text: function () {
                            return $('#order_detail_' + order_id);
                        }                     
                    },

                    hide: {
                        event: 'mouseleave',
                        fixed: true,
                        delay: 100
                    },

                    show: {
                        ready: true,
                        solo: true,
                        delay: 0,
                        event: 'mousemove'
                    },

                    style: {
                        tip: true,
                        border: { width: 3, radius: 8, color: '#646358' },
                        classes: 'mini_product_item ui-tooltip-light ui-tooltip-shadow'

                    },

                    events: {
                        render: function (event, api) {

                            var element = document.getElementById('order-details' + order_id);
                            if (element.childElementCount == 0 || element.innerHTML == "") {
                                new OrderSidebarGrid({ current_order_id: order_id });
                            }

                            Ext.get('order-details' + order_id).focus();
                        }
                    }
                });

                // When we click on a link in the popup bubble, we want to hide it because we may popup a dialog when the link is clicked.
                $(".mini_product_item .actions a").click(function () {
                    var tip = $("#ui-tooltip-" + order_id).qtip();
                    if (tip !== undefined) tip.hide();
                });

                $('.close_sidebar').click(function () {
                    var tip = $("#ui-tooltip-" + order_id).qtip();
                    if (tip !== undefined) tip.hide();
                });
                
            });
        }
    }
});
