Module("SalesNet", function () {
	Class("Logger", {

		my : {
			has: {
				consoleAvailable: {
					is: 'ro',
					lazy : true,
					init: function() { return !!window.console	}
				}
			},

			methods: {
				message: function(contents) {
					if (this.getConsoleAvailable()) console.log(contents);
				},
				
				warn: function(contents) {
					if (this.getConsoleAvailable()) console.warn(contents);
				}
				
			}
		}
	});
});