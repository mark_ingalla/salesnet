// Generic helper functions.

Module("SalesNet", function () {
    Class("Helpers", {
        my : {
            methods: {
                // Block the UI for a given element
                // selector: the selector of the element to block
                // centerY: true if the message is to be centered in the container (selector).
                //          otherwise the message is closer to the top of the container.
                blockUI: function(selector, centerY) {
                    $(selector).block({
                        message: '<h2>Searching</h2>',
                        showOverlay: true,
                        centerY: centerY == undefined ? true : centerY
                    }); 
                },

                // remove the block UI from the display
                unblockUI: function(selector) {
                    $(selector).unblock();
                }
            }
        }
    });
});
