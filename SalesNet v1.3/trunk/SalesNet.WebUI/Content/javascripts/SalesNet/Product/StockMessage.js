
function StockMessage(record, skuOrder, archived_sizes) {
    "use strict";
    this.skuOrder = skuOrder;
    this.record = record;
	this.colour = record.get("Colour");

    // records only contain the colour info, the sizes are saved before the grid renders
	var size_raw = _.first(archived_sizes).get("Size" + skuOrder);
	this.size_parsed = size_raw === '-' ? 'Each' : size_raw;


	this.originalPriceWithCurrency = record.get("OriginalPrice" + skuOrder);
	this.originalPrice = Number(this.originalPriceWithCurrency.match(/[\d,?.?]+/));
	this.currency = this.originalPriceWithCurrency.replace(/[\d,?.?]+/, "{0}");

	this.priceWithCurrency = record.get("Price" + skuOrder);
	this.price = Number(this.priceWithCurrency.match(/^\d+.\d+/));

	//added by raju
//	this.nextavailable = record.get("NextAvailable" + skuOrder);
    //end raju
};




StockMessage.prototype.show = function () {
    "use strict";
   // if (this.price < this.originalPrice) {
    //    return this.markDownMessage();
   // } else {
        return this.regularPriceMessage();
   // }
    };

StockMessage.prototype.discountshow = function () {
        "use strict";
         if (this.price < this.originalPrice) {
            return this.markDownMessage();
         } else {
        return '';
         }
    };

String.prototype.withSpaceAfter = function () {
    return this.concat(" ");
}

StockMessage.prototype.GetQuantityAvailable = function () {

    var highStockThresholdRaw = $("#high-stock-threshold").attr("value");
    var highStockThreshold = Number(highStockThresholdRaw) || 0;
    var quantityAvailableMessage;

    if (highStockThreshold === 0) {
        quantityAvailableMessage = '';
    } else {
        var stock = Number(this.record.get("Stock" + this.skuOrder)) || 0;
        this.stockToDisplay = stock < highStockThreshold ? stock : highStockThreshold;
        quantityAvailableMessage = "<li id='quantity-avail'><label>In Stock :</label><label>" + this.stockToDisplay + "</label></li>"
    }

    return quantityAvailableMessage;
}

StockMessage.prototype.RrpText = function () {
    return this.record.get("RrpText"); ;
}

StockMessage.prototype.RetailPrice = function () {
    return Number(this.record.get("RetailPrice" + this.skuOrder)).toFixed(2) || 0.00; 
}

StockMessage.prototype.RetailPriceAndText = function () {
    if (this.RetailPrice()) {
        return '<li id="retailPrice"><label>' + this.RrpText() + ' :</label><label>' + this.currency.replace("{0}", this.RetailPrice()) + '</label></li>';
    }
}

StockMessage.prototype.markDownMessage = function () {
    //return "<span class='regular_price'>" + this.colour.withSpaceAfter()
	       // + this.size_parsed + ": "
	       // + "Was".withSpaceAfter() + this.originalPriceWithCurrency.strike()
	       // + "<span class='markdown_price'>Now " + this.priceWithCurrency + "</span></span>";
    //     + this.GetQuantityAvailable()
    //    + this.RetailPriceAndText();
    return "<span class='regular_price'>".withSpaceAfter() + this.originalPriceWithCurrency.strike()
     + "</span><span class='markdown_price'>Discount:  <strong>" + this.priceWithCurrency + "</strong></span>";

};
//added by raju
StockMessage.prototype.NextAvailable = function (record, skuOrder) {
    if (record.get("NextAvailable" + skuOrder) != undefined && record.get("NextAvailable" + skuOrder) != "" && showIncomingStock != undefined && showIncomingStock != "" && showIncomingStock =="true")
    {
        return "<li id='nextavailable' style='color:#007acc;'><label style='color:#007acc;font-weight:bold;'>Incoming Stock :</label><label>" + record.get("NextAvailable" + skuOrder) + "</label></li>"
    }
    else
    { return ""; }
}

//ended by raju
StockMessage.prototype.regularPriceMessage = function () {
    return "<li id='selectedcolor'>" + this.colour + "</li> "
            + this.NextAvailable(this.record, this.skuOrder)
            +"<li id='selectedsize'><label>Size :</label><label>" + this.size_parsed + "</label></li>"
            + this.GetQuantityAvailable()
	        + "<li id='selectedprice'><label>Price :</label><label>" + this.priceWithCurrency + "</label></li>"
            + this.RetailPriceAndText();
};