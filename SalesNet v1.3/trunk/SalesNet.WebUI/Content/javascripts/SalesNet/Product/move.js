Ext.namespace('SalesNet.grid');

SalesNet.grid.onEditorTab = function (editingPlugin, e) {
    var me = this,
             direction = e.shiftKey ? 'left' : 'right';

    Ext.apply(me, { move: SalesNet.grid.move });
    var position = me.move(direction, e);

    var hasStock = position && me.store.hasStock(position);

    if (position && hasStock) {
        editingPlugin.startEditByPosition(position);
        me.wasEditing = true;
    }
}

SalesNet.grid.onKeyTab = function (e, t) {
    var me = this,
            direction = e.shiftKey ? 'left' : 'right',
            editingPlugin = me.view.editingPlugin;

    Ext.apply(me, { move: SalesNet.grid.move });
    var position = me.move(direction, e);

    var hasStock = position && me.store.hasStock(position);
    //Look raj
    if (editingPlugin && position && hasStock) {
        editingPlugin.startEditByPosition(position);
    }
}

SalesNet.grid.move = function (dir, e) {
    var me = this;

    Ext.apply(me.primaryView, { walkCells: SalesNet.grid.walkCells });
    pos = me.primaryView.walkCells(me.getCurrentPosition(), dir, e, me.preventWrap);

    if (pos) {
        me.setCurrentPosition(pos);
    }
    return pos;
}

SalesNet.grid.walkCells = function (pos, direction, e, preventWrap, verifierFn, scope) {

    // 2 is the first column that has editable cells
    var me = this,
		row = pos.row,
		column = pos.column,
		rowCount = me.store.getCount(),
    //FIRST_COL = 2, modified by raj
    FIRST_COL = 0,
		lastCol = me.getLastVisibleColumnIndex(),
		newPos = { row: row, column: column },
		activeHeader = me.headerCt.getHeaderAtIndex(column);

    // no active header or its currently hidden
    if (!activeHeader || activeHeader.hidden) {
        return false;
    }


    e = e || {};
    direction = direction.toLowerCase();
    switch (direction) {
        case 'right':
            // has the potential to wrap if its last
            if (column === lastCol) {
                // if bottom row and last column, go to start of grid
                if (preventWrap || row === rowCount - 1) {
                    newPos.row = 0;
                    newPos.column = FIRST_COL;
                    return newPos;
                }
                if (!e.ctrlKey) {
                    // otherwise wrap to nextRow and FIRST_COL
                    newPos.row = row + 1;
                    newPos.column = FIRST_COL;
                }
                // go right
            } else {
                if (!e.ctrlKey) {
                    newPos.column = column + me.getRightGap(activeHeader);
                } else {
                    newPos.column = lastCol;
                }
            }
            break;

        case 'left':
            // has the potential to wrap
            if (column === FIRST_COL) {
                // if top row and first column, deny left
                if (preventWrap || row === 0) {
                    newPos.row = rowCount - 1;
                    newPos.column = lastCol;
                    return newPos;
                }
                if (!e.ctrlKey) {
                    // otherwise wrap to prevRow and lastCol
                    newPos.row = row - 1;
                    newPos.column = lastCol;
                }
                // go left
            } else {
                if (!e.ctrlKey) {
                    newPos.column = column + me.getLeftGap(activeHeader);
                } else {
                    newPos.column = FIRST_COL;
                }
            }
            break;

        case 'up':
            // if top row, deny up
            if (row === 0) {
                return false;
                // go up
            } else {
                if (!e.ctrlKey) {
                    newPos.row = row - 1;
                } else {
                    newPos.row = 0;
                }
            }
            break;

        case 'down':
            // if bottom row, deny down
            if (row === rowCount - 1) {
                return false;
                // go down
            } else {
                if (!e.ctrlKey) {
                    newPos.row = row + 1;
                } else {
                    newPos.row = rowCount - 1;
                }
            }
            break;
    }

    if (verifierFn && verifierFn.call(scope || window, newPos) !== true) {
        return false;
    } else {
        return newPos;
    }
}

