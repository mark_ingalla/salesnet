Class("OrderHistorySearchIE", {
    my: {
        methods: {
            setup: function () {
                $("#order_history :text").labelify({ labelledClass: "label_highlight" });
                $("#order_history .created_date:text").datepicker({
                    constrainInput: true,
                    dateFormat: 'dd/mm/yy',
                    onSelect: function () {
                        $(this).removeClass("label_highlight");
                    }
                });
            }
        }
    }
});
