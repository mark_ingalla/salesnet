Class("ProductList", {
    after: {
        initialize: function () {
            var label_element = $("#search_filter #product_desc:text");

            if (!label_element.labelify('hasLabel')) {
                label_element.labelify({ labelledClass: "label_highlight" }).blur();
            }

            $('#product_index .pagination a').click(function () {
                SalesNet.Helpers.my.blockUI("#product_index", false);
            });

            $('#product_search input[type=submit]').click(function () {
                SalesNet.Helpers.my.blockUI("#product_index", false);
            });

            $('#product_index .product .product_select').each(function () {
                $(this).click(function (event) {
                    $.blockUI({ message: null });
                    $.get($(this).data('link'), function (data) {
                        new ProductQty({ html: data, productId: this.url.match(/\d+$/)[0] });
                        $.unblockUI();
                        return false;
                    });
                    return false;
                });

                $(this).hover(
                    function () {
                        $(this).css("cursor", "pointer");
                    },

                    function () {
                        $(this).css("cursor", "default");
                    }
                );
            });
        }
    }
});
