﻿Ext.require([
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.util.*',
    'Ext.state.*'
]);

Class("OrderSidebarGrid", {
    has: {
        current_order_id: { is: 'r' },
        sizeHeight: { is: 'rw' }
    },

    after: {
        initialize: function () {
            Ext.QuickTips.init();

            //Model
            if (!(Ext.ClassManager.isCreated('ProductSummary'))) {
                Ext.define('ProductSummary', {
                    extend: 'Ext.data.Model',
                    fields: [
                        { name: 'DetailId' },
                        { name: 'LineType' },
			            { name: 'Colour' },
			            { name: 'Size1' },
			            { name: 'Size2' },
			            { name: 'Size3' },
			            { name: 'Size4' },
			            { name: 'Size5' },
			            { name: 'Size6' },
			            { name: 'Size7' },
			            { name: 'Size8' },
			            { name: 'Size9' },
			            { name: 'Size10' }
		            ]
                })
            };


            // create the data store
            var store = new Ext.data.Store({
                model: 'ProductSummary',
                data: detailData  // detailData is a global json string on page
            });

            function is_a_heading_that_needs_bold(record, colIndex) {
                return (record.data.LineType == "SIZEHEAD") || (record.data.LineType == "COLOUR" && colIndex == 0);
            }

            function renderRow(value, metadata, record, rowindex, colIndex) {
                if (is_a_heading_that_needs_bold(record, colIndex) && value != null) {
                    metadata.style += "font-weight: bold;";
                }
                return value;
            }

            function renderRowWordWrap(value, metadata, record, rowindex, colIndex) {
                if (value == null) {
                    return value;
                }

                value = '<div style="white-space:normal !important;">' + value + '</div>';

                if (is_a_heading_that_needs_bold(record, colIndex)) {
                    metadata.style += "font-weight: bold;";
                }
                return value;
            }

            // create the Grid

            var detailIdArray = detailIds.split(",");
            var arLen = detailIdArray.length;
            for (var i = 0; i < arLen; ++i) {
                if (this.current_order_id == detailIdArray[i]) {
                    createGrid(detailIdArray[i]);
                }
            }

            function createGrid(orderDetailId) {
                // filter the store to records for the required detailId
                store.clearFilter();
                store.filterBy(function (record, id) {
                    return ((record.get("DetailId") == orderDetailId) && ((record.get('LineType') == "SIZEHEAD") || (record.get('LineType') == "COLOUR")));
                });

                //clone the filtered data to a new store to be used by grid
                var recordsToCopy = [];
                var myStore = new Ext.data.Store({
                    model: store.model
                });
                store.each(
        	    function (r) {
        	        recordsToCopy.push(r.copy());
        	    });
                myStore.loadRecords(recordsToCopy);

                var sizeHeadings = new Array();
                var records = myStore.getRange();
                var number_of_size_headings = 10;

                // Load up first size headings into array
                for (var i = 0; i < records.length; i++) {
                    if (records[i].get("LineType") == "SIZEHEAD") {
                        sizeHeadings.push(records[i].data);
                        while (number_of_size_headings >= 0) {  // find max sizes to control width of grid
                            if (records[i].get("Size" + number_of_size_headings) != "") {
                                break;
                            }
                            number_of_size_headings--;
                        }
                        records[i].set("LineType", "BLANK"); // Make this line not display again
                        break;
                    }
                }

                myStore.filterBy(function (record, id) {
                    return ((record.get('LineType') == "SIZEHEAD") || (record.get('LineType') == "COLOUR"));
                });

                var colWidth = 50;
                if (number_of_size_headings > 7)
                    colWidth = 34;

                //Set max Height of Grid to 500
                this.sizeHeight = recordsToCopy.length * 24;
                if (this.sizeHeight > 500) {
                    this.sizeHeight = 500;
                }
                   
                var scrollbarWidth = function () {
                    var scrollbarWidth;

                    if (this.sizeHeight === 500) {
                        return scrollbarWidth = 21;
                    } else {
                        return scrollbarWidth = 0;
                    }
                }

                var colourColumnWidth = 120;
                var sizeWidth = (number_of_size_headings * colWidth) + colourColumnWidth + scrollbarWidth();

                var grid = Ext.create('Ext.grid.Panel', {
                    bodyBorder: false,
                    cls: 'product_sku_grid',
                    columnLines: true,
                    enableColumnResize: false,
                    frame: false,
                    height: sizeHeight,
                    layout: 'fit',
                    scroll: 'vertical',
                    renderTo: "order-details" + orderDetailId,
                    store: myStore,
                    title: null,
                    viewConfig: {
                        stripeRows: true
                    },
                    width: sizeWidth,
                    columns: [
		            {
		                dataIndex: 'Colour',
		                draggable: false,
		                menuDisabled: true,
		                renderer: renderRowWordWrap,
		                sortable: false,
		                fixed: true,
		                width: colourColumnWidth
		            },
		            {
		                align: 'center',
		                dataIndex: 'Size1',
		                text: sizeHeadings[0].Size1,
		                width: colWidth,
		                fixed: true,
		                menuDisabled: true
		            },
		            {
		                align: 'center',
		                dataIndex: 'Size2',
		                text: sizeHeadings[0].Size2,
		                width: colWidth,
		                fixed: true,
		                menuDisabled: true
		            },
		            {
		                align: 'center',
		                dataIndex: 'Size3',
		                text: sizeHeadings[0].Size3,
		                width: colWidth,
		                fixed: true,
		                menuDisabled: true
		            },
		            {
		                align: 'center',
		                dataIndex: 'Size4',
		                text: sizeHeadings[0].Size4,
		                width: colWidth,
		                fixed: true,
		                menuDisabled: true
		            },
		            {
		                align: 'center',
		                dataIndex: 'Size5',
		                text: sizeHeadings[0].Size5,
		                width: colWidth,
		                menuDisabled: true
		            },
		            {
		                align: 'center',
		                dataIndex: 'Size6',
		                text: sizeHeadings[0].Size6,
		                width: colWidth,
		                fixed: true,
		                menuDisabled: true
		            },
		            {
		                align: 'center',
		                dataIndex: 'Size7',
		                text: sizeHeadings[0].Size7,
		                width: colWidth,
		                fixed: true,
		                menuDisabled: true
		            },
		            {
		                align: 'center',
		                dataIndex: 'Size8',
		                text: sizeHeadings[0].Size8,
		                width: colWidth,
		                fixed: true,
		                menuDisabled: true
		            },
		            {
		                align: 'center',
		                dataIndex: 'Size9',
		                text: sizeHeadings[0].Size9,
		                width: colWidth,
		                fixed: true,
		                menuDisabled: true
		            },
		            {
		                align: 'center',
		                dataIndex: 'Size10',
		                text: sizeHeadings[0].Size10,
		                width: colWidth,
		                fixed: true,
		                menuDisabled: true
		            }]
                });

                var columnsCount = grid.columns.length; // This is supposed to be more efficient
                for (var i = number_of_size_headings + 1; i < columnsCount; i++) {
                    grid.columns[i].hidden = true;
                }
                for (var i = 1; i < columnsCount; i++) {
                    grid.columns[i].align = 'center';
                    grid.columns[i].sortable = false;
                    grid.columns[i].draggable = false;
                    grid.columns[i].renderer = renderRow;
                }
            }
        }
    }
});
