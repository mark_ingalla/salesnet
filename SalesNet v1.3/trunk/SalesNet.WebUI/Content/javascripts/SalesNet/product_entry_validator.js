Ext.require([
	'Ext.grid.*',
	'Ext.data.*',
	'Ext.util.*',
	'Ext.state.*'
]);

Module("Product", function (m) {

    Class("QuantityValidator", {

        has: {
            sku_number: { is: 'rw' },
            ordered_quantity: { is: 'rw' },
            previous_quantity: { is: 'rw' },
            original_quantity: { is: 'rw' },
            sku: { is: 'rw' },
            edit_event: { is: 'rw' },
            size_headings: { is: 'rw' },
            next_available: { is: 'rw' }
        },

        methods: {

            is_valid: function () {
                return ((this.ordered_quantity !== this.previous_quantity) && this.can_fulfill_order());
            },

            can_fulfill_order: function () {

                if (this.sku.stock_available <= 0 && !this.sku.next_available) {
                    return false;
                }

                if (this.stock_available_is_below_threshold() && !this.sku.next_available) {

                    return this.can_stock_fulfill_low_threshold_order();
                } else {

                    return this.can_stock_fulfill_high_threshold_order();
                }
            },

            stock_available_is_below_threshold: function () {
                var $low_stock_threshold = Number($('#low-stock-threshold').attr('value'));
                return (this.sku.stock_available <= $low_stock_threshold);
            },

            can_stock_fulfill_high_threshold_order: function () { // any amount is accepted as long as the stock is not below the low threshold
                this.clear_stock_message_error();
                return true;
            },

            can_stock_fulfill_low_threshold_order: function () {
                if (this.ordered_quantity <= this.sku.stock_available) {
                    this.clear_stock_message_error();
                    return true;
                } else {
                    this.set_not_enough_stock_message();
                    return false;
                }
            },

            set_not_enough_stock_message: function () {
                $('#low-stock-message').show();
                $('#low-stock-message > label.error').html('Only ' + this.sku.stock_available + ' available for size ' + this.size_headings.data["Size" + this.sku_number] + ' in ' + this.sku.colour);
            },

            clear_stock_message_error: function () {
                $('#low-stock-message').hide();
                $('#low-stock-message > label.error').html('');
            },

            enable_submit_button: function () {

                $("#addToOrder").attr('disabled', false);
                $("#addToOrder").removeClass('grey');
            },

            disable_submit_button: function () {

                $("#addToOrder").attr('disabled', true);
                $("#addToOrder").addClass('grey');
            },

            get_sku: function () {

                return sku = {
                    colour: this.edit_event.record.data["Colour"],
                    sku_id: this.edit_event.record.data["Sku" + this.sku_number],
                    sku_size: this.edit_event.record.data["Size" + this.sku_number],
                    stock_available: Number(this.edit_event.record.data["Stock" + this.sku_number]),
                    next_available: this.edit_event.record.data["NextAvailable" + this.sku_number] != ""
                };
            }
        },

        after: {
            initialize: function (props) {
                this.sku_number = this.edit_event.field.match(/\d{1,2}/)[0];
                this.ordered_quantity = Number(this.edit_event.value);
                this.previous_quantity = Number(this.edit_event.originalValue);
                this.original_quantity = Number(this.edit_event.record.modified["Size" + this.sku_number]);
                this.sku = this.get_sku();
            }
        }
    });
});