Class("Account", {
    isa: Form,

    before: {
        initialize: function () {
           


            this.options = {
                closeOnEscape: false,
                focus: "#password",
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    },
                    password_confirmation: {
                        required: true,
                        equalTo: "#password"
                    }
                },
                messages: {
                    password_confirmation: {
                        equalTo: "Your password entries must match"
                    }
                },

                selector: '#account_form',
                width: 400
            }
        }
    }
});

//debugger;
//$('#EditAccont').click(function (event) {
//    debugger;
//    var Url = '/Account/Edit';
//    $.blockUI({ message: null });
//    $.get(Url, '');
//    return false;
//});
