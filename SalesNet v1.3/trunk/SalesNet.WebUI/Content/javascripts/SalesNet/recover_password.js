Class("RecoverPassword", {
	isa : Form,

	before: {
		initialize: function() {
			this.options = {
				closeOnEscape: false,
				dialog: false,
				focus: "#email",
				modal: false,
//				rules: {
//					email: {
//						required: true,
//						email: true
//					}
//				},

				selector: '#recover_password_form'
			}
		}
	}
});

$(document).ready(function () {
    $("#recover_password_form").validate({
    onkeyup: false,
    onclick: false,
        rules: {
            email: { required: true, email: true }
        },
        messages: {
            email: { required: "Email is Required", email: "Invalid Email" }
        },
        errorLabelContainer: "#error-list"
        // wrapper: "span",

//        submitHandler: function (form) {
//            $("#error-list").hide();


//            return false;
//        }
    });
});
