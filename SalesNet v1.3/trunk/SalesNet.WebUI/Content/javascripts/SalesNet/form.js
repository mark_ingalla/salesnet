Class("Form", {
    after: {
        initialize: function (props) {
            if (props.html) {
                this.$form = $(props.html);
            }
            else {
                this.$form = $(this.options.selector);
            }

            this.setup_ui(this.$form);

            if (this.options.dialog) {
                this.$form.dialog({
                    buttons: this.options.buttons,
                    closeOnEscape: this.options.closeOnEscape,
                    draggable: false,
                    modal: this.options.modal,
                    dialogClass: (this.options.dialogClass || ""),
                    resizable: false,
                    title: this.options.title,
                    width: (this.options.width || 400),
                    autoOpen: this.options.autoOpen,
                    open: function (event, ui) {
                        $(".ui-dialog-titlebar-close", ui.form).hide();
                        if ($(this).dialog("option", "title") == "") {
                            $(".ui-dialog-titlebar", ui.form).hide();
                        }
                    },
                    close: function (event, ui) {
                        // Remove the dialog elements: http://www.bitsandpix.com/entry/jquery-removing-the-jqueryui-dialog-elements-on-close/
                        // Note: this will put the original div element in the dom
                        $(this).dialog("destroy");
                        // Remove the left over element (the original div element)
                        $(this).remove();
                    }
                });
            }
            else {

                if (this.options.width) {
                    this.$form.width(this.options.width);
                }

                if (this.options.focus) {
                    $(this.options.focus, this.$form).focus();
                }

                if (this.options.onOpen) {
                    this.options.onOpen(this);
                }
            }

            this.$form.validate({
                //errorPlacement: this.options.errorPlacement,
                //highlight: this.options.highlight,
                rules: this.options.rules,
                messages: this.options.messages,
                showProcessing: this.options.showProcessing,
                submitHandler: this.options.submitHandler || function (form) {
                    $(".processing", form).show();
                    $("input[type=submit]", form).addClass('grey');
                    form.submit();
                }
            });
        }
    },

    methods: {
        setup_ui: function (form) {
            $("input:password", form).attr('autocomplete', 'off');
            $(".close", form).click(function () {
                form.dialog('close');
            });
            $('#cancelAddToOrder', form).click(function () {
                form.dialog('close');
            });
            if (this.options.showProcessing) {
                $(".buttons", form).append('<span class="processing"></span>');
            }

            $('#product-order-entry-form-butons', form).append('<span class="processing"></span>');
        }
    }
});
