
function loadAlternateAddresses() {

	Ext.define('Location', {
		extend: 'Ext.data.Model',
		fields: ['Id', 'AddressType', 'ContactName', 'AddressLine1', 'AddressLine2', 'City', 'State', 'Postcode', 'Country']
	});

	var store =  Ext.create('Ext.data.Store', {
		model: 'Location',
		proxy: {
			type: 'ajax',
			url: host + 'Customer/Locations',
			reader: {
				type: 'json',
				root: 'Location'
			}
		}
	});

	store.load(function(records, operation, success) { 

		//create link
		if ( success & records.length > 1 ){
			$('#address_details').append('<a id="change_address" class="" href="#">Change</a>');

			$('#change_address').click(showAlternateAddresses);
		}

	});


	function showAlternateAddresses(){
	
		var addresses = Ext.create('Ext.window.Window', {
			id: 'address-window',
			height: 375,
			width: 280,
			title: 'Select alternate address',
			resizable: false,
			layout: 'fit',
			buttons: [{ 
				text: 'Select'
			}],
			buttonAlign: 'center'
		});

		addresses.down('button').addListener('click', clickHandler, addresses);

		var alternateAddresses = renderAlternateAddresses(store);

		addresses.add(alternateAddresses);

		addresses.show();	
	}


	function clickHandler(button, e){

		// Fixes anonymous callback seeing 'this' as DOMwindow
		var that = this;

			// get selected address
		var selectedAddress = _.first(Ext.ComponentQuery.query('radiogroup')[0].getChecked()),
			selectedAddressId = selectedAddress.getId();

		// update order with new address
		Ext.Ajax.request({
			url: host + 'Order/ChangeDeliveryAddress/' + selectedAddressId,
			method: 'PUT',
			success: function() {

				// update address in the view, and close
				$('#delivery_address').html(selectedAddress.fieldLabel);
				that.close();
			}
		});
	}


	function renderAlternateAddresses(store){

		var addressTemplate = new Ext.Template([
			"{AddressLine1}</br>",
			"{AddressLine2}</br>",
			"{City}</br>",
			"{State}</br>",
			"{Postcode}</br>",
			"{Country}"]
		);
						
		var locations =	Ext.create('Ext.form.RadioGroup',{
				name: 'locations',
				autoScroll: true,
				scroll: 'vertical',
				layout: {
					type: 'fit'
				},
				defaultType: 'radiofield',
				columns: 1,
				vertical: true
			});
					
		store.each(function(record) {
				
			locations.add( Ext.create('Ext.form.field.Radio',{
					name: 'locations',
					fieldLabel: addressTemplate.apply(record.data),
					labelSeparator: '',
					labelPad: 20,
					labelWidth: 150,
					margin: '15 0 0 0',
					maxHeight: 87,
					maxWidth: 183,
					inputValue: record.get("Id"),
					id: record.get("Id")
				})
			);
		});

		return locations;
	}
}