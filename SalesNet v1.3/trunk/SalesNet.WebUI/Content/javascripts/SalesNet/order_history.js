Class("OrderHistory", {
    after: {
        initialize: function () {
            $('#order_history .show_order a').each(function () {
                var order_details_id = '#order_details_' + $(this).data('order-id');

                $(this).css("cursor", "pointer").click({ id: order_details_id }, function (event) {
                    $(this).toggleClass("arrowdn");
                    $(this).toggleClass("arrowup");
                    $(event.data.id).toggle();
                });

                $(order_details_id).find('a.close').css("cursor", "pointer").click({ id: order_details_id }, function (event) {
                    $(event.data.id).hide();
                })
            });

            $('#order_history .pagination a').click(function () {
                SalesNet.Helpers.my.blockUI("#order_history");
            })
    
            SalesNet.Helpers.my.unblockUI("#order_history");
        }
    }
});
