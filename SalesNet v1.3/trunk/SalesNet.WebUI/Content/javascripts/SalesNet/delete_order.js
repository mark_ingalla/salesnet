Class("DeleteOrder", {
	isa : Form,

	before: {
		initialize: function() {
			this.options = {
				buttons: [
					{
						text: "Delete Order",
						click: function() { $(this).dialog("close"); }
					},
					{
						text: "Cancel",
						click: function() { $(this).dialog("close"); }
					}
				],
				dialog: true,
				modal: true,
				resizable: false,
				selector: '#delete_order_form'
			}
		}
	}
});
