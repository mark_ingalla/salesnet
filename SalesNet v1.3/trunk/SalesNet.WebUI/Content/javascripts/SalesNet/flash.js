Module("SalesNet", function () {
    Class("Flash", {

        my: {
            methods: {
                // Setup window events to show and hide flash messages
                init: function () {
                    $(window).bind('show_flash_message', function (event, options, callbacks) {
                        if (options.description == 'Login') {
                            var validator = $("#login_form").validate();
                            validator.showErrors({ "email": options.message });
                            if (options.type == "notice") {
                                $("#error-list").toggleClass('error-list');
                                $("#error-list").toggleClass('notify-error-list');
                                
                            } else {
                               
                            }

                        }
                        else if (options.description == 'RecoverPassword') {
                            var validator = $("#recover_password_form").validate();
                            validator.showErrors({ "email": options.message });
                            if (options.type == "notice") {
                                $("#error-list").toggleClass('error-list');
                                $("#error-list").toggleClass('notify-error-list');

                            } else {

                            }

                        }
                        else {

                            if (options.type === "error") {
                                SalesNet.Flash.error(options.message, options.description);
                            }
                            else if (options.type === "warning") {
                                SalesNet.Flash.warning(options.message, options.description);
                            }
                            else {
                                SalesNet.Flash.message(options.message, options.description);
                            }
                        }
                    });
                },

                uiColour: function (state) {

                    switch (state) {
                        case "warning":
                            return "#FF9900";

                        case "error":
                            return "#BB1F1F";

                        case "info":
                            return "#19892F";

                        default:
                            return "#777";
                    }
                },

                // Show a notice (regular) message
                message: function (message, description) {
                    SalesNet.Flash._show(message, description, "info");
                },

                // Show an error message
                error: function (message, description) {
                    SalesNet.Flash._show(message, description, "error");
                },

                // Show a warning message
                warning: function (message, description) {
                    SalesNet.Flash._show(message, description, "warning");
                },

                _show: function (message, description, state) {
                    var template = "<div id='growlUI'><h1><span class='ui-icon ui-icon-{{state}}' />{{message}}</h1><h2>{{description}}</h2></div>";
                    var html = Mustache.to_html(template, { message: message, description: description, state: state });

                    $('#growlUI').length == 0 ? $('body').append(html) : $('#growlUI').replace(html);

                    $.blockUI({
                        message: $('#growlUI'),
                        fadeIn: 700,
                        fadeOut: 1000,
                        timeout: 0,
                        showOverlay: false,
                        centerY: false,
                        css: {
                            width: '98%',
                            top: '10px',
                            left: '',
                            right: '10px',
                            border: 'none',
                            padding: '5px',
                            backgroundColor: this.uiColour(state),
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: 0.9,
                            color: '#fff'
                        }
                    });

                    $('.blockUI').click(function () {
                        $(this).fadeOut();
                    });
                }
            }
        }
    });
});
