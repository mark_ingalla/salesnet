﻿Ext.require([
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.util.*',
    'Ext.state.*',
    'Ext.tip.*'
]);

Class("OrderFinaliseDetails", {

    after: {
        initialize: function () {

            //Model
            if (!(Ext.ClassManager.isCreated('ProductSummary'))) {
                Ext.define('ProductSummary', {
                    extend: 'Ext.data.Model',
                    fields: [
					{ name: 'DetailId' },
					{ name: 'LineType' },
					{ name: 'Colour' },
					{ name: 'Size1' },
					{ name: 'Size2' },
					{ name: 'Size3' },
					{ name: 'Size4' },
					{ name: 'Size5' },
					{ name: 'Size6' },
					{ name: 'Size7' },
					{ name: 'Size8' },
					{ name: 'Size9' },
					{ name: 'Size10' }
				]
                });
            };

            // create the data store
            var store = new Ext.data.Store({
                model: 'ProductSummary',
                data: detailData  // detailData is a global json string on page
            });

            function is_a_heading_that_needs_bold(record, colIndex) {
                return (record.data.LineType == "SIZEHEAD") || (record.data.LineType == "COLOUR" && colIndex == 0);
            }

            function renderRow(value, metadata, record, rowindex, colIndex) {
                if (is_a_heading_that_needs_bold(record, colIndex) && value != null) {
                    metadata.style += "font-weight: bold;";
                }
                return value;
            }

            function renderRowWordWrap(value, metadata, record, rowindex, colIndex) {
                if (value == null) {
                    return value;
                }

                value = '<div style="white-space:normal !important;">' + value + '</div>';

                if (is_a_heading_that_needs_bold(record, colIndex)) {
                    metadata.style += "font-weight: bold;";
                }
                return value;
            }


            var detailIdArray = detailIds.split(",");
            var arLen = detailIdArray.length;
            for (var i = 0; i < arLen; ++i) {
                createGrid(detailIdArray[i]);
            }

            function length_of_longest_colour(_store) {
                _store.filter('LineType', "COLOUR");
                _store.sort('Colour.length', 'DESC');

                if (_store.first(false) == null || (typeof _store.first(false) === "undefined")) {
                    return 0;
                }
                var longest_colour = _store.first(false).get('Colour').length;

                _store.clearFilter();

                return longest_colour;
                
            }

            function createGrid(orderDetailId) {
                // filter the store to records for the required detailId
                store.clearFilter();
                store.filterBy(function (record, id) {
                    return ((record.get("DetailId") == orderDetailId) && ((record.get('LineType') == "SIZEHEAD") || (record.get('LineType') == "COLOUR")));
                });


                //clone the filtered data to a new store to be used by grid
                var recordsToCopy = [];
                var myStore = new Ext.data.Store({
                    model: store.model
                });
                store.each(
        	    function (r) {
        	        recordsToCopy.push(r.copy());
        	    });
                myStore.loadRecords(recordsToCopy);

                var sizeHeadings = new Array();
                var records = myStore.getRange();
                var maxSizeHeadingsPerRow = 10;

                // Load up first size headings into array
                for (var i = 0; i < records.length; i++) {
                    if (records[i].get("LineType") == "SIZEHEAD") {
                        sizeHeadings.push(records[i].data);
                        while (maxSizeHeadingsPerRow >= 0) {  // find max sizes to control width of grid
                            if (records[i].get("Size" + maxSizeHeadingsPerRow) != "") {
                                break;
                            }
                            maxSizeHeadingsPerRow--;
                        }
                        records[i].set("LineType", "BLANK"); // Make this line not display again
                        break;
                    }
                }

                var length_of_longest_colour_word = length_of_longest_colour(myStore);

                var colourColumnWidth = 0;
                if (length_of_longest_colour_word > 10)
                    colourColumnWidth = (length_of_longest_colour_word * 7) + 20;
                else
                    colourColumnWidth = (length_of_longest_colour_word * 10) + 20;


                var colWidth = 57;
                if (maxSizeHeadingsPerRow > 7)
                    colWidth = 40;

                var sizeWidth = 57;
                if (maxSizeHeadingsPerRow > 7)
                    sizeWidth = 48;

                sizeWidth = (maxSizeHeadingsPerRow * colWidth) + colourColumnWidth;

                myStore.filterBy(function (record, id) {
                    return ((record.get('LineType') == "SIZEHEAD") || (record.get('LineType') == "COLOUR"));
                });

                var grid = Ext.create('Ext.grid.Panel', {
                    bodyBorder: false,
                    cls: 'product_sku_grid',
                    scroll: false,
                    enableColumnResize: false,
                    columnLines: true,
                    columns: [
		            {
		                dataIndex: 'Colour',
		                draggable: false,
		                menuDisabled: true,
		                text: 'Type',
		                renderer: renderRow,
		                sortable: false,
		                sizable: false,
		                width: colourColumnWidth
		            },
                    {//Raju Added the new column
                        dataIndex: 'Quantity',
                        text: 'Total',
                        width: colWidth,
                        menuDisabled: true
                    },
		            {
		                dataIndex: 'Size1',
		                text: (typeof sizeHeadings[0] === "undefined") ? "" : sizeHeadings[0].Size1,
		                width: colWidth,
		                menuDisabled: true
		            },
		            {
		                dataIndex: 'Size2',
		                text: (typeof sizeHeadings[0] === "undefined") ? "" : sizeHeadings[0].Size2,
		                width: colWidth,
		                menuDisabled: true
		            },
		            {
		                dataIndex: 'Size3',
		                text: (typeof sizeHeadings[0] === "undefined") ? "" : sizeHeadings[0].Size3,
		                width: colWidth,
		                menuDisabled: true
		            },
		            {
		                dataIndex: 'Size4',
		                text: (typeof sizeHeadings[0] === "undefined") ? "" : sizeHeadings[0].Size4,
		                width: colWidth,
		                menuDisabled: true
		            },
		            {
		                dataIndex: 'Size5',
		                text: (typeof sizeHeadings[0] === "undefined") ? "" : sizeHeadings[0].Size5,
		                width: colWidth,
		                menuDisabled: true
		            },
		            {
		                dataIndex: 'Size6',
		                text: (typeof sizeHeadings[0] === "undefined") ? "" : sizeHeadings[0].Size6,
		                width: colWidth,
		                menuDisabled: true
		            },
		            {
		                dataIndex: 'Size7',
		                text: (typeof sizeHeadings[0] === "undefined") ? "" : sizeHeadings[0].Size7,
		                width: colWidth,
		                menuDisabled: true
		            },
		            {
		                dataIndex: 'Size8',
		                text: (typeof sizeHeadings[0] === "undefined") ? "" : sizeHeadings[0].Size8,
		                width: colWidth,
		                menuDisabled: true
		            },
		            {
		                dataIndex: 'Size9',
		                text: (typeof sizeHeadings[0] === "undefined") ? "" : sizeHeadings[0].Size9,
		                width: colWidth,
		                menuDisabled: true
		            },
		            {
		                dataIndex: 'Size10',
		                text: (typeof sizeHeadings[0] === "undefined") ? "" : sizeHeadings[0].Size10,
		                width: colWidth,
		                menuDisabled: true
		            }],
                    frame: false,
                    layout: 'auto',
                    renderTo: "order-details" + orderDetailId,
                    store: myStore,
                    title: null,
                    enableColumnResize: false,
                    viewConfig: {
                        stripeRows: true
                    },
                    width: sizeWidth //(maxSizeHeadingsPerRow * sizeWidth) + 112  //width of grid depends on max sizes on first size heading line
                });

                var columnsCount = grid.columns.length; // This is supposed to be more efficient
                for (var i = maxSizeHeadingsPerRow + 1; i < columnsCount; i++) {
                    grid.columns[i].hidden = true;
                }
                for (var i = 1; i < columnsCount; i++) {
                    grid.columns[i].align = 'center';
                    grid.columns[i].sortable = false;
                    grid.columns[i].draggable = false;
                    grid.columns[i].renderer = renderRow;
                }

            }
        }
    }
});
