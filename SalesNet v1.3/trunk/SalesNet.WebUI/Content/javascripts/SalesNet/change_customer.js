﻿Class("ChangeCustomer", {
    isa: Form,
    has: {
        customerId: {
            is: 'rw'
        }
    },

    methods: {
        setup_grid: function () {

        },

        search_customer: function (txtsearch) {
            
           // debugger;
            var url = host + 'Customer/SearchCustomerList';
            $.getJSON(url, { searchtext: txtsearch }, function (data) {

                var items = [];
                $.each(data, function (val) {
                    //debugger;
                 
                    if (varSelectedCustomer != undefined && varSelectedCustomer == parseInt(this.Id)) {

                        items.push('<tr class="row"><td><span>' + this.Name + '</span></td><td><label class="current-customer">Currently&nbsp;Selected</label></td></tr>');
                    } else {
                        items.push('<tr class="row"><td><span>' + this.Name + '</span></td><td><a href="Customer/SelectCustomer/' + this.Id + '" class="btn btn-inverse">Select</a></td></tr>');
                    }

                });

                $('.customerlist').html(items.join(''));

                var windowheight = $(window).height();
                var modalheight = $('.customerlist').height();
                //Change Overlay Height
                if (modalheight > windowheight) {
                    $(".ui-widget-overlay").css('height', modalheight + 200);
                }
            });
        },

        openDialog: function () {
            $(this).dialog("open");
        }
    },

    before: {
        initialize: function (props) {
            this.options = {
                dialog: true,
                modal: true,
                dialogClass: 'change_customer modal-dialog',
                selector: '#change_customer_form',
                //    autoOpen: false,
                width: 500,
                submitHandler: function (form) {
                    $.ajax({
                        url: $("#customer_list_form").attr('action'),
                        data: JSON.stringify(window.ordered_skus),
                        success: function (response, status, xhr) {
                            $(form).dialog('close');
                            //   $("#CurrentOrder").html(response);
                        },
                        type: 'POST',
                        contentType: 'application/json, charset=utf-8',
                        dataType: 'html'
                    });

                    return false;
                }
            };

        }
    },

    after: {
        initialize: function () {
            if ($.browser.msie) {
                $('.scroll', this.$form).css({ 'padding-bottom': '20px' });
            }
            var myform = this;
            //  this.setup_grid();

            $("#txtcustomersearch").keyup(function () {
                // var txtsearch = $(this);
                // if ($("#txtcustomersearch").val().length >= 3) {
                myform.search_customer($("#txtcustomersearch").val());
                //  }
            });

            var windowheight = $(window).height();
            var modalheight = $('.customerlist').height();
            //Change Overlay Height
            if (modalheight > windowheight) {
                $(".ui-widget-overlay").css('height', modalheight + 200);
            }
        }
    },

    override: {
        setup_ui: function (form) {
            this.SUPER(form);
            //initially no orders have been made so do not enable posting the grid
            var $buttons = $("input[type=submit]", this.$form);
            $buttons.addClass('grey');
            $buttons.attr("disabled", true);
        }
    }
});