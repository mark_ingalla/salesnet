Module("SalesNet", function () {
    Class("Navigation", {

        my: {
            methods: {
                init: function () {
                    $('#navigation_products > ul > li').each(function () {
                        var $this = $(this);
                        var this_id = '#' + $this.attr('id');

                        $this.qtip({
                            position: {
                                at: 'right center',
                                my: 'left center',
                                container: $('#navigation_products'),
                                target: $(this_id)
                            },

                            style: {
                                classes: 'navigation_items ui-tooltip-light ui-tooltip-shadow',
                                tip: {
                                    corner: false
                                }
                            },

                            content: {
                                text: function () {
                                    if ($(this_id + '_item > ul > li').length)
                                        return $(this_id + '_item').html();
                                }
                            },

                            hide: {
                                delay: 30,
                                event: 'mouseleave',
                                fixed: true
                            },

                            show: {
                                delay: 0,
                                event: 'click mouseenter',
                                solo: true
                            },
                            events: {
                                // when showing/hiding the popup, maintain the highlight of the navigation parent item in the sidebar
                                show: function (event, api) {
                                    $(this_id).addClass('nav_select');
                                },
                                hide: function (event, api) {
                                    $(this_id).removeClass('nav_select');
                                },
                                onFocus: function (event, api) {
                                    $(this_id).addClass('nav_select');
                                },
                                render: function (event, api) {
                                    $(this).hover(
                                        function () {
                                            $(this_id).addClass("nav_select");
                                        },
                                        function () {
                                            $(this_id).removeClass("nav_select");
                                        });
                                    // Hide the popup when clicking on an item within it
                                    api.elements.tooltip.click(function (event, api) {
                                        $(this_id).removeClass('nav_select');
                                        $(this).hide();
                                    });
                                }
                            }

                        });
                    });
                }
            }
        }
    });
});