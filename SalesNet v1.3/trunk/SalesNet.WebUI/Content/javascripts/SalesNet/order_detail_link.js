Class("orderDetailLink", {
    after: {
        initialize: function () {
            $('#mini_order .products  a.order_detail').each(function () {
                $(this).click(function (event) {
                    event.preventDefault();
                    $.get($(this).attr('href'), function (data) {
                        new ProductQty({ html: data });
                    });
                });
            });
        }
    }
});
