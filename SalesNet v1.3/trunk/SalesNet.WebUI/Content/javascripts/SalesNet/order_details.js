Ext.require([
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.util.*',
    'Ext.state.*'
]);

Class("OrderDetails", {
    has: {
        order_id: { is: 'r' }
    },
    
    after: {
        initialize: function () {
            Ext.QuickTips.init();

            //Model
            Ext.define('ProductSummary', {
                extend: 'Ext.data.Model',
                fields: [
                        { name: 'LineType' },
			            { name: 'Product' },
			            { name: 'Description' },
			            { name: 'Colour' },
			            { name: 'Size1' },
			            { name: 'Size2' },
			            { name: 'Size3' },
			            { name: 'Size4' },
			            { name: 'Size5' },
			            { name: 'Size6' },
			            { name: 'Size7' },
			            { name: 'Size8' },
			            { name: 'Size9' },
			            { name: 'Size10' },
			            { name: 'UnitPrice' },
			            { name: 'Quantity' },
			            { name: 'Total' }
		            ]
            });

            
            // create the data store
            var store = new Ext.data.Store({
                autoLoad: true,
                listeners: {
                    'load': function (store, records) {
                    
                        // move size headings up to be on the same row as the product code and description
                        var count = store.count();

                        for (var i = count - 1; i >= 0; i--) {
                            var record = store.getAt(i);

                            if (record.data.LineType == "SIZEHEAD" && i - 1 >= 0) {
                                var prevRecord = store.getAt(i - 1);

                                if (prevRecord.data.LineType == "PRODUCT") {
                                    for (var size = 1; size <= 10; size++) {
                                        var sizeAttr = 'Size' + size;
                                        prevRecord.data[sizeAttr] = record.data[sizeAttr];
                                    }
                                    store.removeAt(i);
                                }
                            }
                        }
                    },
                    scope: this
                },
                model: 'ProductSummary',
                proxy: {
                    type: 'ajax',
                    url: this.order_id + '.json',
                    reader: {
                        type: 'json'
                    }
                }
            });


            function is_a_heading_that_needs_bold(record, colIndex) {
                return record.data.LineType == "SIZEHEAD" ||
                record.data.LineType == "BLANK" ||
                (record.data.LineType == "COLOUR" && colIndex == 2) ||
                record.data.LineType == "GROSS" ||
                record.data.LineType == "DISCOUNT" ||
                record.data.LineType == "TAX" ||
                record.data.LineType == "DUE" ||
                record.data.LineType == "PRODUCT";
            }

            function renderRow(value, metadata, record, rowindex, colIndex) {
                if (is_a_heading_that_needs_bold(record, colIndex) && value != null) {
                    metadata.style += "font-weight: bold;";
                }
                return value;
            }

            function renderRowWordWrap(value, metadata, record, rowindex, colIndex) {
                if (value == null) {
                    return value;
                }

                value = '<div style="white-space:normal !important;">' + value + '</div>';

                if (is_a_heading_that_needs_bold(record, colIndex)) {
                    metadata.style += "font-weight: bold;";
                }
                return value;
            }

            // create the Grid
            var unsupportedIE = Ext.isIE7 || Ext.isIE8;

            var grid = Ext.create('Ext.grid.Panel', {
                autoScroll: !unsupportedIE,
                bodyBorder: false,
                enableColumnResize: false,
                columnLines: true,
                columns: [
                    {
                        dataIndex: 'Product',
                        draggable: false,
                        fixed: unsupportedIE,
                        menuDisabled: true,
                        renderer: renderRow,
                        resizeable: !unsupportedIE,
                        sortable: false,
                        text: 'Product',
                        width: 80
                    },
		            {
		                dataIndex: 'Description',
		                draggable: false,
		                fixed: unsupportedIE,
		                menuDisabled: true,
		                resizeable: !unsupportedIE,
		                sortable: false,
		                text: 'Description',
		                width: 150
		            },
		            {
		                dataIndex: 'Colour',
		                draggable: false,
		                fixed: unsupportedIE,
		                menuDisabled: true,
		                renderer: renderRowWordWrap,
		                resizeable: !unsupportedIE,
		                sortable: false,
		                width: 70
		            },
		            {
		                align: 'right',
		                dataIndex: 'Size1',
		                draggable: false,
		                fixed: unsupportedIE,
		                menuDisabled: true,
		                renderer: renderRow,
		                resizeable: !unsupportedIE,
		                sortable: false,
		                width: 40
		            },
		            {
		                align: 'right',
		                dataIndex: 'Size2',
		                draggable: false,
		                fixed: unsupportedIE,
		                menuDisabled: true,
		                renderer: renderRow,
		                resizeable: !unsupportedIE,
		                sortable: false,
		                width: 40
		            },
		            {
		                align: 'right',
		                dataIndex: 'Size3',
		                draggable: false,
		                fixed: unsupportedIE,
		                menuDisabled: true,
		                renderer: renderRow,
		                resizeable: !unsupportedIE,
		                sortable: false,
		                width: 40
		            },
		            {
		                align: 'right',
		                dataIndex: 'Size4',
		                draggable: false,
		                fixed: unsupportedIE,
		                menuDisabled: true,
		                renderer: renderRow,
		                resizeable: !unsupportedIE,
		                sortable: false,
		                width: 40
		            },
		            {
		                align: 'right',
		                dataIndex: 'Size5',
		                draggable: false,
		                fixed: unsupportedIE,
		                menuDisabled: true,
		                renderer: renderRow,
		                resizeable: !unsupportedIE,
		                sortable: false,
		                width: 40
		            },
		            {
		                align: 'right',
		                dataIndex: 'Size6',
		                draggable: false,
		                fixed: unsupportedIE,
		                menuDisabled: true,
		                renderer: renderRow,
		                resizeable: !unsupportedIE,
		                sortable: false,
		                width: 40

		            },
		            {
		                align: 'right',
		                dataIndex: 'Size7',
		                draggable: false,
		                fixed: unsupportedIE,
		                menuDisabled: true,
		                renderer: renderRow,
		                resizeable: !unsupportedIE,
		                sortable: false,
		                width: 40
		            },
		            {
		                align: 'right',
		                dataIndex: 'Size8',
		                draggable: false,
		                fixed: unsupportedIE,
		                menuDisabled: true,
		                renderer: renderRow,
		                resizeable: !unsupportedIE,
		                sortable: false,
		                width: 40
		            },
		            {
		                align: 'right',
		                dataIndex: 'Size9',
		                draggable: false,
		                fixed: unsupportedIE,
		                menuDisabled: true,
		                renderer: renderRow,
		                resizeable: !unsupportedIE,
		                sortable: false,
		                width: 40
		            },
		            {
		                align: 'right',
		                dataIndex: 'Size10',
		                draggable: false,
		                fixed: unsupportedIE,
		                menuDisabled: true,
		                renderer: renderRow,
		                resizeable: !unsupportedIE,
		                sortable: false,
		                width: 40
		            },
		            {
		                align: 'right',
		                dataIndex: 'UnitPrice',
		                draggable: false,
		                fixed: unsupportedIE,
		                menuDisabled: true,
		                renderer: renderRow,
		                resizeable: !unsupportedIE,
		                sortable: false,
		                text: 'Unit Price',
		                width: 85
		            },
		            {
		                align: 'right',
		                dataIndex: 'Quantity',
		                draggable: false,
		                fixed: unsupportedIE,
		                menuDisabled: true,
		                resizeable: !unsupportedIE,
		                sortable: false,
		                text: 'Quantity',
		                width: 62
		            },
		            {
		                align: 'right',
		                dataIndex: 'Total',
		                draggable: false,
		                fixed: unsupportedIE,
		                menuDisabled: true,
		                resizeable: !unsupportedIE,
		                sortable: false,
		                text: 'Total',
		                width: 110
		            }],
                frame: false,
                layout: 'auto',
                renderTo: 'order-details',
                scroll: !unsupportedIE,
                store: store,
                title: null,
                viewConfig: {
                    stripeRows: true
                },
                width: 959
            });
        }
    }
});
