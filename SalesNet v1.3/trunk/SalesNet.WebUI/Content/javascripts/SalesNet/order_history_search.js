Class("OrderHistorySearch", {
    my: {
        methods: {
            setup: function () {
                $("#order_history :text").labelify({ labelledClass: "label_highlight" });
                $("#order_history .created_date:text").datepicker({
                    constrainInput: true,
                    dateFormat: 'dd/mm/yy',
                    onSelect: function () {
                        $(this).removeClass("label_highlight");
                    }
                });

                $("#order_history form").live("submit", function () {
                    //commented by raju 18 dec 2012 enable search button
                    //$("#Search_Orders").attr("disabled", true);
                    SalesNet.Helpers.my.blockUI("#order_history");
                    return true;
                });
                //commented by raju 18 dec 2012 enable search button
                // $("#Search_Orders").attr("disabled", false);
                SalesNet.Helpers.my.unblockUI("#order_history");

                //                $('#lnkchangeCustomer').unbind('click');
                //                $('#lnkchangeCustomer').click(function (e) {

                //                    OrderHistorySearch.my.displayCustomerChange();
                //                      e.preventDefault();
                //                    // ChangeCustomer.prototype.openDialog();


                //                  });


            },

            displayCustomerChange: function () {

                $.blockUI({ message: null });
               
                var varUrl = 'Customer/RenderCustomerList/';
                if (document.getElementById("aChangeCustomerUrl") != undefined) {
                    varUrl = document.getElementById("aChangeCustomerUrl").href;
                }
                $.get(varUrl, function (data) {

                    new ChangeCustomer({ html: data });
                    $.unblockUI();
                    return false;
                });


            }


        }
    }
});
