﻿Class("FinaliseOrder",
     {

         after: {
             initialize: function () {
                 //            $('.products .mini_product_item  a.product_qty_popup_finalise').each(function () {
                 //                $(this).click(function (event) {
                 //                    event.preventDefault();
                 //                    $.blockUI({ message: null });
                 //                    $.get(this.href, function (data) {
                 //                        new ProductQty({ html: data, productId: this.url.match(/OrderDetail\/(\d+)/)[1] });
                 //                        $.unblockUI();
                 //                        return false;
                 //                    });
                 //                    return false;
                 //                });
                 //            });



                 $('.products .mini_product_item  a.product_qty_finalise').each(function () {
                     var myhref = this;
                     $(this).click(function (event) {
                         $.blockUI({ message: null });
                         $.get(this.href, function (data) {
                           //  new ProductQty({ html: data, productId: this.url.match(/\d+$/)[0] });
                             new ProductQty({ html: data, productId: $(myhref).attr('data-product') });
                             $.unblockUI();
                             return false;
                         });
                         return false;
                     });

                     $(this).hover(
                    function () {
                        $(this).css("cursor", "pointer");
                    },

                    function () {
                        $(this).css("cursor", "default");
                    }
                );
                 });
             }
         }
     });
