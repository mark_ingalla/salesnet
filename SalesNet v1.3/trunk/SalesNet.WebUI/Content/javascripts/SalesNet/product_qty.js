Class("ProductQty", {
    isa: Form,
    has: {
        productId: {
            is: 'rw'
        },
        modaldialog: {
            is: 'rw'

        }
    },

    methods: {
        setup_grid: function () {
            new Product.ProductOrderEntry({ productId: this.productId });
        }
        ,
        close: function () {
           
            $(this.modaldialog).dialog('close');
           
        }
    },

    before: {
        initialize: function (props) {
            var mydialog = this;
            this.options = {
                dialog: true,
                modal: true,
                dialogClass: 'product_quantity modal-dialog',
                selector: '#product_qty_form',
                width: 850,
                submitHandler: function (form) {

                    $.ajax({
                        url: $("#product_qty_form").attr('action'),
                        data: JSON.stringify(window.ordered_skus),
                        success: function (response, status, xhr) {
                            //  $(form).dialog('close');
                            mydialog.close();
                            $("#CurrentOrder").html(response);
                        },
                        type: 'POST',
                        contentType: 'application/json, charset=utf-8',
                        dataType: 'html'
                    });

                    return false;
                }
            };
        }
    },

    after: {
        initialize: function () {
            if ($.browser.msie) {
                $('.scroll', this.$form).css({ 'padding-bottom': '20px' });
            }
            this.modaldialog = this.$form;
            this.setup_grid();
        }
    },

    override: {
        setup_ui: function (form) {
            this.SUPER(form);

            //initially no orders have been made so do not enable posting the grid
            var $buttons = $("input[type=submit]", this.$form);
            $buttons.addClass('grey');
            $buttons.attr("disabled", true);
        }
    }
});
