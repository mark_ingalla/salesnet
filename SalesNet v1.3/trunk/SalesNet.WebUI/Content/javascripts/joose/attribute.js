//Role('JooseX.Attribute.Delegated', {
//    
////    have : {
////        trigger        : null
////    }, 
////
////    
////    before : {
////        computeValue : function() {
////            if (this.trigger) this.is = 'rw'
////        }
////    },
////    
////    
////    after : {
////        
////        computeValue : function() {
////            if (this.trigger) {
////                var after = {}    
////                
////                after[this.setterName] = this.trigger    
////                
////                this.role.meta.extend({ after : after })
////            }
////        }
////        
////    }
//    
//});
Role('JooseX.Attribute.Trigger', {
    
    have : {
        trigger        : null
    }, 

    
    after : {
        initialize : function() {
            if (this.trigger) this.writeable = this.hasSetter = true
        }
    },
    
    
    override : {
        
        getSetter : function() {
            var original    = this.SUPER()
            var trigger     = this.trigger
            
            if (!trigger) return original
            
            var me = this
            
            return function () {
                var res = original.apply(this, arguments)
                
                trigger.call(this, me.getValueFrom(this))
                
                return res
            }
        }
    }
})    


/**

Name
====


JooseX.Attribute.Trigger - call a function after attribute has been changed via setter call


SYNOPSIS
========

            Class('Some.Class', { 
                has : {
                    someAttribuute : {
                        init : 'foo',
                        
                        trigger : function (newValue) {
                            this.triggerCalled = true
                        } 
                    }
                }
            })
            
            var instance = new Some.Class()
            
            instance.triggerCalled == false //trigger wasn't called yet
            
            instance.setSomeAttibute('bar')
            
            instance.triggerCalled == true //trigger was called


DESCRIPTION
===========

`JooseX.Attribute.Trigger` is a role, which triggers a function call, after an attribute was changed via setter call.

It is called as a method (in the scope of the instance), and receives the new value as argument. 

Trigger is also called when an attribute's value is passed to the constructor.

**Note**, that trigger will not be called for the value of attribute's `init` option, as it only translates the value to the prototype of the class 


SEE ALSO
========

[Main documentation page](../Attribute.html)



AUTHORS
=======

Nickolay Platonov <nplatonov@cpan.org>



COPYRIGHT AND LICENSE
=====================

Copyright (c) 2009, Nickolay Platonov

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of Nickolay Platonov nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 


*/;
Role('JooseX.Attribute.Lazy', {
    
    
    have : {
        lazy        : null
    }, 
    
    
    before : {
        computeValue : function() {
            if (typeof this.init == 'function' && this.lazy) {
                this.lazy = this.init    
                delete this.init
            }
        }
    },


    after : {
        initialize : function() {
            if (this.lazy) this.readable = this.hasGetter = true
        }
    },


    override : {

        getGetter : function() {
            var original    = this.SUPER()
            var lazy        = this.lazy

            if (!lazy) return original

            var me      = this
            var name    = this.name

            return function () {
                var lazyPredicates = this.__LAZY_PREDICATES__ = this.__LAZY_PREDICATES__ || {}

                if (!lazyPredicates[name]) {
                    lazyPredicates[name] = true

                    var init = lazy.call(this, me)

                    me.setValueTo(this, init)
                }

                return original.call(this)
            }

        }

    }

})


/**

Name
====


JooseX.Attribute.Lazy - a role, deferring the attribute initialization untill first call to getter


SYNOPSIS
========

            Class('Some.Class', {
                has : {
                    lazyAttribute : {
                        init : function () {
                            return this.someCostlyComputation()
                        },

                        lazy : true
                    },

                    // -or-

                    lazyAttribute : {
                        lazy : function () {
                            return this.someCostlyComputation()
                        }
                    }

                }
            })

            var instance = new Some.Class()

            instance.lazyAttribute == undefined // true, initializer of lazy attribute wasn't called yet

            var lazy = instance.getLazyAttibute()

            instance.lazyAttribute != undefined // true, initializer of lazy attribute was called


DESCRIPTION
===========

Joose lets you defer attribute population by making an attribute lazy (see the [Synopsis][] for syntax)

When `lazy` flag is set, the default is not generated until the getter method is called, rather than at object construction time.
There are several reasons you might choose to do this.

First, if the default value for this attribute depends on some other attributes, then the attribute must be lazy.
During object construction, defaults are not generated in a predictable order, so you cannot count on some other attribute being populated when generating a default.

Second, there's often no reason to calculate a default before it's needed. Making an attribute lazy lets you defer the cost until the attribute is needed.
If the attribute is never needed, you save some CPU time.


SEE ALSO
========

[Main documentation page](../Attribute.html)



AUTHORS
=======

Nickolay Platonov <nplatonov@cpan.org>



COPYRIGHT AND LICENSE
=====================

Copyright (c) 2009, Nickolay Platonov

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of Nickolay Platonov nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


*/
;
Role('JooseX.Attribute.Accessor.Combined', {


    have : {
        isCombined        : false
    },


    after : {
        initialize : function() {
            this.isCombined = this.isCombined || /..c/i.test(this.is)

            if (this.isCombined) {
                this.slot = '$$' + this.name

                this.hasGetter = true
                this.hasSetter = false

                this.setterName = this.getterName = this.publicName
            }
        }
    },


    override : {

        getGetter : function() {
            var getter    = this.SUPER()
            var setter    = this.getSetter()

            var me = this

            return function () {

                if (!arguments.length) {
                    if (me.readable) return getter.call(this)
                    throw "Call to getter of unreadable attribute: [" + me.name + "]"
                }

                if (me.writeable) return setter.apply(this, arguments)

                throw "Call to setter of read-only attribute: [" + me.name + "]"
            }
        }
    }

})


/**

Name
====


JooseX.Attribute.Accessor.Combined - a role, combining setter and getter methods into one, ala perl


SYNOPSIS
========

            Class('Some.Class', {
                has : {
                    attr : {
                        is : 'rwc',

                        init : 'some init value'
                    }
                }
            })

            var instance = new Some.Class()

            instance.attr() == 'some init value'  // call to combined accessor as getter

            instance.attr('some other value')     // call to combined accessor as setter

            instance.attr() == 'some other value' // attribute was changed


DESCRIPTION
===========

This role combines the getter and setter methods into one, having the same name as attribute itself.

Call this method without arguments will be directed to the actual getter call.

Call with some arguments provided will be directed to setter call.

Its safe to combine this role with Lazy and Trigger, but it should be applied after them.


USAGE
=====

To enable this role provide the trailing `c` character in attribute's `is` option:

            has : {
                attr : {
                    is : 'rwc',

                    init : 'some init value'
                }
            }

Alternatively, explicitly specify `isCombined` option with some `true` value:

            has : {
                attr : {
                    is : 'rw',

                    isCombined : true,

                    init : 'some init value'
                }
            }


SEE ALSO
========

[Main documentation page](../Attribute.html)



AUTHORS
=======

Nickolay Platonov <nplatonov@cpan.org>



COPYRIGHT AND LICENSE
=====================

Copyright (c) 2009, Nickolay Platonov

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of Nickolay Platonov nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


*/
;
Joose.Managed.Attribute.meta.extend({
    does : [ JooseX.Attribute.Trigger, JooseX.Attribute.Lazy, JooseX.Attribute.Accessor.Combined ]
})


/**

Name
====


JooseX.Attribute - Additional features for Joose attributes


SYNOPSIS
========

        <!-- Joose  -->
        <script type="text/javascript" src="/jsan/Task/Joose/Core.js"></script>

        <!-- Bootstraping Joose attributes with additional features -->
        <script type="text/javascript" src="/jsan/Task/JooseX/Attribute/Bootstrap.js"></script>


DESCRIPTION
===========

`JooseX.Attribute` is a collection of meta-roles for Joose attributes, which enhance them with advanced features.

To use the new features, add the bootstraping source file as in Synopsis.




Please refer to documentation of each role for details:

> - [JooseX.Attribute.Trigger](Attribute/Trigger.html)

> - [JooseX.Attribute.Lazy](Attribute/Lazy.html)

> - [JooseX.Attribute.Accessor.Combined](Attribute/Accessor/Combined.html)


GETTING HELP
============

This extension is supported via github issues tracker: <http://github.com/SamuraiJack/JooseX-Attribute/issues>

For general Joose questions you can also visit #joose on irc.freenode.org or the forum at: <http://joose.it/forum>



SEE ALSO
========

Web page of this module: <http://github.com/SamuraiJack/JooseX-Attribute/>

General documentation for Joose: <http://openjsan.org/go/?l=Joose>


BUGS
====

All complex software has bugs lurking in it, and this module is no exception.

Please report any bugs through the web interface at <http://github.com/SamuraiJack/JooseX-Attribute/issues>



AUTHORS
=======

Nickolay Platonov <nplatonov@cpan.org>



COPYRIGHT AND LICENSE
=====================

Copyright (c) 2009, Nickolay Platonov

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of Nickolay Platonov nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 


*/
;
