Ext.require('Ext.tip.QuickTipManager');
/*
	This file is placeholder for developing the JS. When things are actually up and running, these modules will be created in partials.
*/
$(document).ready(function(event) {
	SalesNet.Flash.init();
    $.blockUI.defaults.css = {};
});


$(document).ready(function () {

    var myForm = $("#SendOrderForm");

    $(".print_button").click(function () {
        window.print();
    });

    $("#sendorder").click(function () {
        myForm.submit();
        return false;
    });

    if (customerCount > 1) {
        $('.lnkchangeCustomer').unbind('click');
        $('.lnkchangeCustomer').click(function(e) {

            OrderHistorySearch.my.displayCustomerChange();
            e.preventDefault();
            // ChangeCustomer.prototype.openDialog();


        });
    }
});

// when the server returns an error, show the error page which is returned in the response
$(document).ajaxError(function (event, request, settings, error) {
	try{	
		handle(request.status);
	}
	catch(ex){
		//don't have specific error handling for these so write the error to the page
		if (request.status >= 402 && request.status <= 599) {
			document.write(request.responseText);
		}
	}
});


Ext.onReady(function () {
    // initialize the quicktips
    Ext.tip.QuickTipManager.init();
})
