﻿using System;
using System.Configuration;
using System.Web.Mvc;
using Ninject.Modules;
using Ninject;
using System.Web.Routing;
using SalesNet.Domain.Implementations;
using SalesNet.Domain.Interfaces;
using System.Web.Security;
using SalesNet.WebUI.Classes;
using Ap21API.Resources;

namespace SalesNet.WebUI.Infrastructure
{
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private IKernel kernel = new StandardKernel(new SalesNetServices());

        protected override IController GetControllerInstance(RequestContext context,
        Type controllerType)
        {
            if (controllerType == null)
                return null;
            return (IController)kernel.Get(controllerType);
        }
        
        private class SalesNetServices : NinjectModule
        {
            public override void Load()
            {
                Bind<IAppSessionWrapper>().To<AppSession>();
				Bind<IResourceProviderConfiguration>().To<AppSession>();
                Bind<IFormsAuthWrapper>().To<FormsAuthWrapper>();
                Bind<IEmailHelper>().To<ResetPasswordEmailService>();
                Bind<MembershipProvider>().To<SalesNetMemberShipProvider>();
            }
        }
    }


}