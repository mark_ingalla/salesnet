﻿using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace SalesNet.WebUI.Infrastructure
{
    public class EnhancedAuthorizedAttribute : AuthorizeAttribute
    {
        public bool AllowAll;

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {

            return AllowAll || base.AuthorizeCore(httpContext);
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated && filterContext.HttpContext.Request.IsAjaxRequest())
            {
                filterContext.HttpContext.Response.Clear();
                filterContext.HttpContext.Response.Status = "401 Unauthorized";
                filterContext.HttpContext.Response.StatusCode = 401;
                filterContext.HttpContext.Response.AddHeader("Content-Type", "application/json ");
                filterContext.HttpContext.Response.Write(Json.Encode(new { login_url = "Login" }));
                filterContext.HttpContext.Response.End();
            }
        }
    }
}