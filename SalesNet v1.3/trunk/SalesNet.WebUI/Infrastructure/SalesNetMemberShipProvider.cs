﻿using System;
using System.Web;
using System.Web.Security;
using Ap21API.Resources;
using SalesNet.Domain.Entities;
using SalesNet.Domain.Implementations;

namespace SalesNet.WebUI.Infrastructure
{
    public class SalesNetMemberShipProvider : MembershipProvider
    {
        private Person user;
        private readonly IFormsAuthWrapper formsAuth;
        private readonly IAppSessionWrapper session;
        public SalesNetMemberShipProvider(IFormsAuthWrapper formsAuthWrapper, IAppSessionWrapper sessionWrapper)
        {
            this.formsAuth = formsAuthWrapper;
            this.session = sessionWrapper;
        }

        # region "inherited members"
        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override bool EnablePasswordReset
        {
            get { throw new NotImplementedException(); }
        }

        public override bool EnablePasswordRetrieval
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { throw new NotImplementedException(); }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new NotImplementedException(); }
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        #endregion

        public bool LogIn(string email, string password)
        {
            if (ValidateUser(email: email, password: password))
            {
                SetAuthCookie();
                SetSessionData();
                return true;
            }
            return false;
        }

        public override bool ValidateUser(string email, string password)
        {
            var person = Persons.Find(email: email, password: EncryptionHelper.CreateHash(password));
            if (person != null)
            {
                user = person;
                return true;
            }
            return false;
        }

        private void SetAuthCookie()
        {
            formsAuth.SetAuthCookie(user.Contacts.Email, false);
        }

        private void SetSessionData()
        {
            session.Current.Fullname = user.FullName;
            session.Current.Id = user.Id;
            session.Current.IsAgent = user.IsAgent;
            //added by raju
            session.Current.CurrentOrderCurrencyFormat = user.Currency.Format;
            HttpContext.Current.Session["Currency"] = session.Current.CurrentOrderCurrencyFormat;
            //end
        }

        public void LogOut()
        {
            formsAuth.SignOut();
        }
    }
}