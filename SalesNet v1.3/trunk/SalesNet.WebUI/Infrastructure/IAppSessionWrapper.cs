﻿namespace SalesNet.WebUI.Infrastructure
{
    public interface IAppSessionWrapper
    {
        string Fullname { get; set; }
        string Id { get; set; }
        string CustomerId { get; set; }
        string CustomerName { get; set; }
        bool IsAgent { get; set; }
        string CurrentOrderCurrencyFormat { get; set; }
        string CurrentError { get; set; }
        IAppSessionWrapper Current { get; }
    }
}