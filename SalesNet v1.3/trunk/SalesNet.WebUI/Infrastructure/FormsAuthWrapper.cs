﻿using System.Web.Security;

namespace SalesNet.WebUI.Infrastructure
{
    public class FormsAuthWrapper : IFormsAuthWrapper
    {
        public void SetAuthCookie(string userName, bool createPersistentCookie)
        {
            FormsAuthentication.SetAuthCookie(userName, createPersistentCookie);
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }
    }
}