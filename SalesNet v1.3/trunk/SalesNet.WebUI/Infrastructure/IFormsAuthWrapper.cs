﻿
namespace SalesNet.WebUI.Infrastructure
{
    public interface IFormsAuthWrapper
    {
        void SetAuthCookie(string userName, bool createPersistentCookie);
        void SignOut();
    }
}
