﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using Ap21API.Resources;
using SalesNet.WebUI.Classes;

namespace SalesNet.WebUI.Infrastructure
{
    public class SetupSessionAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                if (!IsLogInRequest(filterContext.RouteData))
                {
                    var session = filterContext.HttpContext.ApplicationInstance.Session[0];
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Session", action = "LogIn" }));
            }
            catch (Exception ex)
            {
                String msg = ex.Message;
            }

            ResourceProvider.Configuration = (IResourceProviderConfiguration)AppSession.CurrentSession.Current;
        }

        private static bool IsLogInRequest(RouteData routeData)
        {
            try
            {
                var actionPart = (string)routeData.Values["action"];
                var controllerPart = (string)routeData.Values["controller"];

                var isSessionController = string.Compare(controllerPart, "Session", true).Equals(0);
                var isLogInAction = string.Compare(actionPart, "Login", true).Equals(0);

                return isSessionController && isLogInAction;
            }
            catch (NullReferenceException)
            {
                return false;
            }

        }
    }
}