﻿using System.Web.Mvc;
using Ap21API;
using SalesNet.WebUI.Classes;



namespace SalesNet.WebUI.Filters
{
    public class AP21ExceptionFilterAttribute : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            // Don't interfere if the exception is already handled
            if (filterContext.ExceptionHandled)
            {
                return;
            }
            if (filterContext.Exception is AP21APIException)
            {
                //Set up custom response in XML for the exception
                AP21APIException ap21Exception = filterContext.Exception as AP21APIException;
                AppSession.CurrentSession.Current.CurrentError = ap21Exception.Message;
            }
        }
    }
}

