﻿
using System.Configuration;
using Ap21API.Entities.Customer;

namespace SalesNet.WebUI.Classes
{
    // Retrieves a credit status message from the web.config based on a customer's credit status
    public class CustomerCreditStatus
    {

        private readonly Customer customer;
        public bool CanContinue
        {
            get
            {
                return customer.CreditStatusFlag != 1; // Suspended Permanent 
            }
        }

        public bool IsNormal
        {
            get
            {
                return customer.CreditStatusFlag == 0; // Normal
            }
        }

        public string Level
        {
            get
            {
                return customer.CreditStatus;
            }
        }

        public CustomerCreditStatus(Customer customer)
        {
            this.customer = customer;
        }


        public string Message()
        {
            return ConfigurationManager.AppSettings["CreditStatus" + customer.CreditStatusFlag];
        }

    }
}