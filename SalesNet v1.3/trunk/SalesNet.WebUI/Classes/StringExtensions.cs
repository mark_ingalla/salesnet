﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalesNet.WebUI.Classes
{
	public static class StringExtensions
	{
		/// <summary>
		/// Return the first lenght number of characters from the string
		/// </summary>
		/// <param name="str">String instance</param>
		/// <param name="length">Number of characters to return</param>
		/// <returns>String containing the first length characters of the string.</returns>
		public static string Left(this String str, int length)
		{
			return str.Substring(0, Math.Min(str.Length, length));
		}
	}
}