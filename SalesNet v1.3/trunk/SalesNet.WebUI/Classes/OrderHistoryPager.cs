﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SalesNet.WebUI.Classes
{
    public class OrderHistoryPager : OriginalPager
    {
        public OrderHistoryPager(ViewContext viewContext, int pageSize, int currentPage, int totalItemCount, Dictionary<string, string> valuesDictionary) : base(viewContext, pageSize, currentPage, totalItemCount, valuesDictionary)
        {
            this.viewContext = viewContext;
            this.pageSize = pageSize;
            this.currentPage = currentPage;
            this.totalItemCount = totalItemCount;
            linkWithoutPageValuesDictionary = valuesDictionary;
        }

        public override string RenderHtml(string updateTarget = null)
        {
            bool isAjax = updateTarget != null;

            var pageCount = (int)Math.Ceiling(this.totalItemCount / (double)this.pageSize);

            var from = ((this.currentPage - 1)*this.pageSize) + 1;
            var to = (this.currentPage) * this.pageSize >= this.totalItemCount ? this.totalItemCount : (this.currentPage) * this.pageSize;
            var total = this.totalItemCount;



            var html = new StringBuilder("<div class=\"pagination-wrapper\">");


            //showing...
            html.AppendFormat("<span  class=\"pagination-label-total\">{0}</span>", "Showing " + from + " - " + to + " of " + total + " " + linkWithoutPageValuesDictionary["order_type"]);
            html.Append("<span class=\"pager_nav\">");
            if (pageCount > 1)
            {
                

                // Previous
                html.Append(this.currentPage > 1
                                ? GeneratePageLink(
                                    "<span class=\"previous\">&laquo; Previous</span>",
                                    this.currentPage - 1, updateTarget)
                                : "<span class=\"previous disabled\">&laquo; Previous</span>");

                html.Append(" | Page : ");
                int start = 1;

                int end = pageCount;

                if (pageCount > numOfPagesToDisplay)
                {
                    int middle = (int) Math.Ceiling(numOfPagesToDisplay/2d) - 1;
                    int below = this.currentPage - middle;
                    int above = this.currentPage + middle;

                    if (below < 4)
                    {
                        above = numOfPagesToDisplay;
                        below = 1;
                    }
                    else if (above > (pageCount - 4))
                    {
                        above = pageCount;
                        below = (pageCount - numOfPagesToDisplay);
                    }

                    start = below;
                    end = above;
                }

                if (start > 3)
                {
                    html.Append(GeneratePageLink("1", 1, updateTarget));
                    html.Append(GeneratePageLink("2", 2, updateTarget));
                    html.Append("...");
                }

                for (int i = start; i <= end; i++)
                {
                    if (i == this.currentPage)
                    {
                        html.AppendFormat("<span class=\"current\">{0}</span>", i);
                    }
                    else
                    {
                        html.Append(GeneratePageLink(i.ToString(), i, updateTarget));
                    }
                }

                if (end < (pageCount - 3))
                {
                    html.Append("...");
                    html.Append(GeneratePageLink((pageCount - 1).ToString(), pageCount - 1, updateTarget));
                    html.Append(GeneratePageLink(pageCount.ToString(), pageCount, updateTarget));
                }

                // Next
                html.Append(this.currentPage < pageCount
                                ? GeneratePageLink(" | <span class=\"next\">Next &raquo;</span>",
                                                   (this.currentPage + 1), updateTarget)
                                : "<span class=\"next disabled\">Next &raquo;</span>");

                
            }
            html.Append("</span>");
            html.Append("</div>");
            return html.ToString();
        }
    }
}