﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace SalesNet.WebUI.Classes
{
    public class PagedList<T> : List<T>, IPagedList<T>
    {
        #region IPagedList Members

        public int PageCount { get; private set; }
        public int TotalItemCount { get; private set; }
        public int PageIndex { get; private set; }
        public int PageNumber { get { return PageIndex + 1; } }
        public int PageSize { get; private set; }
        public Dictionary<string, string> SearchCriteria { get; private set; }

        #endregion

        #region Constructors

        public PagedList(IEnumerable<T> source, int index = 0, int pageSize = 0, int totalCount = 0, object searchCriteria = null)
        {
            Initialize(source.AsQueryable(), index, pageSize, totalCount, searchCriteria);
        }

        public PagedList(IQueryable<T> source, int index = 0, int pageSize = 0, int totalCount = 0, object searchCriteria = null)
        {
            Initialize(source, index, pageSize, totalCount, searchCriteria);
        }

        #endregion


        protected void Initialize(IQueryable<T> source, int index, int pageSize, int totalCount, object searchCriteria)
        {
            if (searchCriteria != null)
            {
                SearchCriteria = (Dictionary<string, string>)searchCriteria;
            }

            //argument checking
            if (index >= 0)
            {
                PageIndex = index; 
            }
            else
            {
                throw new ArgumentOutOfRangeException("PageIndex cannot be below 0.");
            }

            if (pageSize >= 1)
            {
                PageSize = pageSize;
            }
            else
            {
                throw new ArgumentOutOfRangeException("PageSize cannot be less than 1.");
            }

            if (totalCount >= 0)
            {
                TotalItemCount = totalCount;
            }
            else
            {
                throw new ArgumentOutOfRangeException("TotalItemCount cannot be below 0.");
            }

            //set source to blank list if source is null to prevent exceptions
            if (source == null)
            {
                source = new List<T>().AsQueryable();
            }                         

            if (TotalItemCount > 0)
            {
                PageCount = (int)Math.Ceiling(TotalItemCount / (double)PageSize);
                AddRange(source.ToList());
            }
            else
            {
                PageCount = 0;
            }
        }
    }
}