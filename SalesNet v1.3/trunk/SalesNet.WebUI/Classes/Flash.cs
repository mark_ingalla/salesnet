﻿using System;

namespace SalesNet.WebUI.Classes
{
    class Flash
    {
        public string Description { get; set; }
        public string Message { get; set; }
    }
}
