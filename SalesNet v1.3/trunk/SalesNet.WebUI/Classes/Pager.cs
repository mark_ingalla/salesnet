﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SalesNet.WebUI.Classes
{
    public class Pager
    {
        private ViewContext viewContext;
        private readonly int numOfPagesToDisplay = 10;
        private readonly int pageSize;
        private readonly int currentPage;
        private readonly int totalItemCount;
        private readonly Dictionary<string, string> linkWithoutPageValuesDictionary;

        public Pager(ViewContext viewContext, int pageSize, int currentPage, int totalItemCount, Dictionary<string,string> valuesDictionary)
        {
            this.viewContext = viewContext;
            this.pageSize = pageSize;
            this.currentPage = currentPage;
            this.totalItemCount = totalItemCount;
            this.linkWithoutPageValuesDictionary = valuesDictionary;
        }
        

        public string RenderHtml(string updateTarget = null)
        {
            bool isAjax = updateTarget != null;

            var pageCount = (int) Math.Ceiling(this.totalItemCount/(double) this.pageSize);

            if (pageCount <= 1)
            {
                return String.Empty;
            }

            var html = new StringBuilder();

            // Previous
            //html.Append(this.currentPage > 1
            //                ? GeneratePageLink("&lt;", this.currentPage - 1, updateTarget)
            //                : "<span class=\"disabled\">&lt;</span>");
            html.Append(this.currentPage > 1
                            ? GeneratePageLink("&lt;&lt;Previous ", this.currentPage - 1, updateTarget)
                            : "<span class=\"disabled\">&lt;&lt;Previous</span>");

            int start = 1;

            int end = pageCount;

            //if (pageCount > numOfPagesToDisplay)
            //{
            //    int middle = (int) Math.Ceiling(numOfPagesToDisplay/2d) - 1;
            //    int below = this.currentPage - middle;
            //    int above = this.currentPage + middle;

            //    if (below < 4)
            //    {
            //        above = numOfPagesToDisplay;
            //        below = 1;
            //    }
            //    else if (above > (pageCount - 4))
            //    {
            //        above = pageCount;
            //        below = (pageCount - numOfPagesToDisplay);
            //    }

            //    start = below;
            //    end = above;
            //}
            
            //if (start > 3)
            //{
               
            //    html.Append(GeneratePageLink("1", 1, updateTarget));
            //    html.Append(GeneratePageLink("2", 2, updateTarget));
            //    html.Append("...");
            //}
           // end = 3;  //Modified raju for product screen

            //if (this.currentPage % 3 == 0)
            //{
                start = this.currentPage;
                
            //}
            //else
            //{
            //    start = (this.currentPage + 1) % 3 == 0 ? this.currentPage - 1 : this.currentPage;
                
            //}
            //commented by raju 13dec 2012
            //end = start + 2;

            //modified by raju 13 dec 2012
            //start
            if (pageCount > 2)
            {
                //modified by siva 9-jan-2013
                //start
                if (pageCount == this.currentPage)
                {

                    end = start;
                    start = currentPage - 2;
                }
                else if(pageCount==this.currentPage+1)
                {
                    end = start + 1;
                    start = currentPage - 1;
                   
                }
                else if (pageCount == this.currentPage + 2)
                {
                    end = start + 2;
                }
                //else
                //{
                // //end
                //    end = start + 2;
                //}
                else if (pageCount >= 3)
                {
                    end = this.numOfPagesToDisplay; //this end is no need

                    end = currentPage + 2;
                }
            }
            else
            {
                end = start + 1;
            }
            //end
            //by siva 11 feb 2013
            //if (!(end <= this.numOfPagesToDisplay))
            //{
            //    end = this.numOfPagesToDisplay;
            //}

            html.Append("| Page: ");
            for (int i = start; i <= end; i++)
            {
                if (i == this.currentPage)
                {
                    html.AppendFormat("<span class=\"current\">{0}</span>", i);
                }
                else
                {
                    html.Append(GeneratePageLink(i.ToString(), i, updateTarget));
                }
            }

            if (end < (pageCount - 3))
            {
                html.Append("...");
                //Modified raju for product screen
                //html.Append(GeneratePageLink((pageCount - 1).ToString(), pageCount - 1, updateTarget));
                //html.Append(GeneratePageLink(pageCount.ToString(), pageCount, updateTarget));
            }

            html.Append(" | ");
            // Next
            //Modified raju for product screen
            //html.Append(this.currentPage < pageCount
            //                ? GeneratePageLink("&gt;", (this.currentPage + 1), updateTarget)
            //                : "<span class=\"disabled\">&gt;</span>");
            html.Append(this.currentPage < pageCount
                           ? GeneratePageLink("Next &gt;&gt;", (this.currentPage + 1), updateTarget)
                           : "<span class=\"disabled\">Next &gt;&gt;</span>");

            return html.ToString();
        }


     


    
        private string GeneratePageLink(string linkText, int pageNumber, string updateTarget)
        {
            var pageLinkValueDictionary = new RouteValueDictionary();

            if (linkWithoutPageValuesDictionary != null && linkWithoutPageValuesDictionary.Count > 0)
            {
                foreach (KeyValuePair<string, string> pair in linkWithoutPageValuesDictionary)
                {
                    if (pair.Key != null && pair.Value != null)
                        pageLinkValueDictionary.Add(pair.Key, pair.Value);
                }

            }

            pageLinkValueDictionary.Add("page", pageNumber);

            var virtualPathData = RouteTable.Routes.GetVirtualPath(this.viewContext.RequestContext, "Default", pageLinkValueDictionary);

            if (virtualPathData != null)
            {
                if (updateTarget != null)
                {
                    return String.Format("<a data-ajax=\"true\" data-ajax-mode=\"replace\" data-ajax-update=\"" +
                                        updateTarget + "\" href=\"{0}\">{1}</a>", virtualPathData.VirtualPath, linkText);
                }

                return String.Format("<a href=\"{0}\">{1}</a>", virtualPathData.VirtualPath, linkText);
            }

            return String.Empty;
        }

    }
}