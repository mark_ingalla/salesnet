﻿using System.Web;
using SalesNet.WebUI.Infrastructure;
using Ap21API.Resources;
using System.Configuration;

namespace SalesNet.WebUI.Classes
{
    public class AppSession : IAppSessionWrapper, IResourceProviderConfiguration
    {
        public bool IsEmpty()
        {
            return  Fullname == null ||
                    Id == null;
        }

        
        public IAppSessionWrapper Current
        {
            get
            {
                var session = new AppSession();
                if (HttpContext.Current.Session != null)
                {
                     session = (AppSession)HttpContext.Current.Session["CurrentSession"];

                    if (session == null)
                    {
                        session = new AppSession();
                        HttpContext.Current.Session["CurrentSession"] = session;
                    }
                }
              
                return session;
            }
        }

        public static IAppSessionWrapper CurrentSession
        {
            get
            {
                var session=new AppSession();
                if (HttpContext.Current.Session != null)
                {
                     session = (AppSession)HttpContext.Current.Session["CurrentSession"];

                    if (session == null)
                    {
                        session = new AppSession();
                        HttpContext.Current.Session["CurrentSession"] = session;
                    }
                }
                

                return session;
            }
        }

        public string Fullname { get; set; }
        public string Id { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public bool IsAgent { get; set; }
        public string CurrentOrderCurrencyFormat { get; set; }
        public string CurrentError { get; set; }

        #region IResourceProviderConfiguration Implementation

        public string RequestPersonId
        {
            get
            {
                return Id;
            }
        }

        public string RequestCustomerId
        {
            get
            {
                return CustomerId;
            }
        }

        public string Site
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["AP21API"].ConnectionString;
            }
        }

        #endregion
    }
}

