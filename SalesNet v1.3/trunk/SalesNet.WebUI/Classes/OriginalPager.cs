﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SalesNet.WebUI.Classes
{
    public class OriginalPager
    {
        protected ViewContext viewContext;
        protected int numOfPagesToDisplay = 10;
        protected  int pageSize;
        protected  int currentPage;
        protected  int totalItemCount;
        protected  Dictionary<string, string> linkWithoutPageValuesDictionary;

        public OriginalPager(ViewContext viewContext, int pageSize, int currentPage, int totalItemCount, Dictionary<string, string> valuesDictionary)
        {
            this.viewContext = viewContext;
            this.pageSize = pageSize;
            this.currentPage = currentPage;
            this.totalItemCount = totalItemCount;
            this.linkWithoutPageValuesDictionary = valuesDictionary;
        }

        public virtual string RenderHtml(string updateTarget = null)
        {
            bool isAjax = updateTarget != null;

            var pageCount = (int)Math.Ceiling(this.totalItemCount / (double)this.pageSize);

            if (pageCount <= 1)
            {
                return String.Empty;
            }

            var html = new StringBuilder();

            // Previous
            html.Append(this.currentPage > 1
                            ? GeneratePageLink("&lt;", this.currentPage - 1, updateTarget)
                            : "<span class=\"disabled\">&lt;</span>");

            int start = 1;

            int end = pageCount;

            if (pageCount > numOfPagesToDisplay)
            {
                int middle = (int)Math.Ceiling(numOfPagesToDisplay / 2d) - 1;
                int below = this.currentPage - middle;
                int above = this.currentPage + middle;

                if (below < 4)
                {
                    above = numOfPagesToDisplay;
                    below = 1;
                }
                else if (above > (pageCount - 4))
                {
                    above = pageCount;
                    below = (pageCount - numOfPagesToDisplay);
                }

                start = below;
                end = above;
            }

            if (start > 3)
            {
                html.Append(GeneratePageLink("1", 1, updateTarget));
                html.Append(GeneratePageLink("2", 2, updateTarget));
                html.Append("...");
            }

            for (int i = start; i <= end; i++)
            {
                if (i == this.currentPage)
                {
                    html.AppendFormat("<span class=\"current\">{0}</span>", i);
                }
                else
                {
                    html.Append(GeneratePageLink(i.ToString(), i, updateTarget));
                }
            }

            if (end < (pageCount - 3))
            {
                html.Append("...");
                html.Append(GeneratePageLink((pageCount - 1).ToString(), pageCount - 1, updateTarget));
                html.Append(GeneratePageLink(pageCount.ToString(), pageCount, updateTarget));
            }

            // Next
            html.Append(this.currentPage < pageCount
                            ? GeneratePageLink("&gt;", (this.currentPage + 1), updateTarget)
                            : "<span class=\"disabled\">&gt;</span>");

            return html.ToString();
        }


        protected string GeneratePageLink(string linkText, int pageNumber, string updateTarget)
        {
            var pageLinkValueDictionary = new RouteValueDictionary();

            if (linkWithoutPageValuesDictionary != null && linkWithoutPageValuesDictionary.Count > 0)
            {
                foreach (KeyValuePair<string, string> pair in linkWithoutPageValuesDictionary)
                {
                    if (pair.Key != null && pair.Value != null)
                        pageLinkValueDictionary.Add(pair.Key, pair.Value);
                }

            }

            pageLinkValueDictionary.Add("page", pageNumber);

            var virtualPathData = RouteTable.Routes.GetVirtualPath(this.viewContext.RequestContext, "Default", pageLinkValueDictionary);

            if (virtualPathData != null)
            {
                if (updateTarget != null)
                {
                    return String.Format("<a data-ajax=\"true\" data-ajax-mode=\"replace\" data-ajax-update=\"" +
                                        updateTarget + "\" href=\"{0}\">{1}</a>", virtualPathData.VirtualPath, linkText);
                }

                return String.Format("<a href=\"{0}\">{1}</a>", virtualPathData.VirtualPath, linkText);
            }

            return String.Empty;
        }

    }
}