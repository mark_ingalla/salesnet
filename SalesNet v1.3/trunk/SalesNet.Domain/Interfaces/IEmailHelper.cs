﻿namespace SalesNet.Domain.Interfaces
{
    public interface IEmailHelper
    {
        void ResetPassword(string userEmail);
    }
}
