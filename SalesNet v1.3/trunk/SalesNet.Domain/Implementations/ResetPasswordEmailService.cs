﻿using System;
using System.Configuration;
using System.Net.Mail;
using System.Security.Policy;
using System.Text;
using Ap21API;
using SalesNet.Domain.Interfaces;
using SalesNet.Domain.Entities;


namespace SalesNet.Domain.Implementations
{
    public class ResetPasswordEmailService : IEmailHelper
    {
        private readonly Url logInUrl;
        private readonly string mailFrom;
        private Person userToReset;

        public ResetPasswordEmailService()
        {
            var host = ConfigurationManager.AppSettings["Host"];
            var url = string.Format("http://{0}/Session/LogIn", host);
            logInUrl = new Url (url);
            mailFrom = ConfigurationManager.AppSettings["MailFrom"];
        }

        public void ResetPassword(string userEmail)
        {
            try
            {
                if(FindPersonByEmail(userEmail))
                {
                    ResetPassword();
                    SendEmail();
                }
            }
            catch (AP21APIException exception)
            {
                // '5008 No data found' means there was no matching email 'addy'
                if(exception.ErrorCode == 5008)
                {
                    throw new EmailRecipientNotFoundException();
                }
                
                if (exception.ErrorCode == 5054)
                {
                    throw new EmailRecipientCustomerAcctSuspendedException();
                }
                
                throw exception;
            }
        }

        private bool FindPersonByEmail(string email)
        {
            var person = Persons.Find(email: email);
            if(person != null)
            {
                userToReset = person;
                return true;
            }
            return false;
        }

        private void ResetPassword()
        {
            userToReset.GenerateNewPassword();
        }

        private void SendEmail()
        {
            var enableSSL = bool.Parse( ConfigurationManager.AppSettings["enableSSL"] );

            using (var smtpClient = new SmtpClient(){EnableSsl = enableSSL })
            {
                using (var mailMessage = BuildMailMessage())
                    {
                        smtpClient.Send(mailMessage);
                    }
            }
        }

        private MailMessage BuildMailMessage()
        {
            var body = new StringBuilder();
            body.AppendLine("Hi " + userToReset.FullName);
            body.AppendLine("<br/><br/>");
            body.AppendLine(" As requested, we've created a new SalesNet password for you.");
            body.AppendLine("<br/><br/>");
            body.AppendLine(" You will now be able to login using the password: " + userToReset.Password + ".");
            body.AppendLine("<br/><br/>");
            body.AppendLine("SalesNet log in URL " + logInUrl.Value);
            body.AppendLine("<br/><br/>");
            body.AppendLine("Cheers from the SalesNet team! ");

            return new MailMessage(mailFrom,                
				userToReset.Contacts.Email,
                "Your new SalesNet password",
                body.ToString())
                {IsBodyHtml = true};
        }
    }

    public class EmailRecipientNotFoundException : Exception
    {
        public EmailRecipientNotFoundException()
        {
            var exception = base.GetBaseException();
        }

        public override string Message
        {
            get { return "No account with that email address could be found"; }
        }
    }

    public class EmailRecipientCustomerAcctSuspendedException : Exception
    {
        public override string Message
        {
            get { return "No active account with that email address could be found"; }
        }
    }
}
