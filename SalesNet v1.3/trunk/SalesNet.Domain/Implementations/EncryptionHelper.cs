﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace SalesNet.Domain.Implementations
{
    public static  class EncryptionHelper
    {
        public static string CreateHash(string data)
        {
            var encoder = new ASCIIEncoding();
            var dataBytes = encoder.GetBytes(data);

            var sha1 = new SHA1CryptoServiceProvider();
            sha1.ComputeHash(dataBytes);

            return Convert.ToBase64String(sha1.Hash);
        }
    }
}