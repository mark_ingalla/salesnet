﻿using System;
using Ap21API.Entities.Order;

namespace SalesNet.Domain.Extensions
{
    public static class OrderSkuExtensions
    {
        public static Matric OrderStateBalance(this OrderSKU orderSku, string orderState)
        {
            if(orderState.Equals("invoiced", StringComparison.InvariantCultureIgnoreCase))
            {
                return new Matric()
                {
                    Quantity = orderSku.Ordered.Quantity - orderSku.Invoiced.Quantity,
                    Gross = orderSku.Ordered.Gross - orderSku.Invoiced.Gross,
                    Net = orderSku.Ordered.Net - orderSku.Invoiced.Net,
                    Discount = orderSku.Ordered.Discount - orderSku.Invoiced.Discount,
                    Tax = orderSku.Ordered.Tax - orderSku.Invoiced.Tax,
                    Value = orderSku.Ordered.Value - orderSku.Invoiced.Value,
                };
            }else
            {
                return orderSku.Ordered;
            }
        }

        public static bool HasNoQuantity(this OrderSKU orderSku)
        {
            return orderSku.Invoiced.Quantity == 0 && 
                   orderSku.Ordered.Quantity == 0 &&
                   orderSku.Outstanding.Quantity == 0;
        }
    }
}



