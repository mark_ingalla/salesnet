﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ap21API.Entities.Order;

namespace SalesNet.Domain.Extensions
{
    public static class OrderDetailExtensions
    {
        public static List<OrderClr> OrderedColours(this OrderDetail detail)
        {
            return detail.Clrs.Clr.OrderBy(x => x, new ClrSequenceComparer()).ToList();
        }

        public static List<string> OrderedSizes(this OrderDetail detail)
        {
            return detail.Clrs.Clr.SelectMany(colour => colour.SKUs.SKU, (colour, sku) => sku).Where(x => !x.HasNoQuantity())
                .Distinct(new SkuSizeCodeComparer())
                .OrderBy(x => x, new SkuSequenceComparer())
                .Select(x => x.SizeCode).ToList();
        }

        public static bool HasColours(this OrderDetail detail)
        {
            return !detail.Clrs.Clr[0].ClrCode.Equals("");
        }

        public static bool HasSkus(this OrderDetail detail)
        {
            return detail.Clrs.Clr.Any(colour => colour.SKUs.SKU.Any(y => !y.HasNoQuantity()));
        }
    }
}
