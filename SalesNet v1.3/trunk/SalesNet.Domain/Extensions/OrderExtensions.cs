﻿using System;
using Ap21API.Entities.Order;

namespace SalesNet.Domain.Extensions
{
    public static class OrderExtensions
    {
        public static Matric OrderStateBalance(this Order order)
        {
            if (order.OrderState.Equals("invoiced", StringComparison.InvariantCultureIgnoreCase))
            {
                return new Matric() { Quantity = order.Ordered.Quantity - order.Invoiced.Quantity,
                                      Gross = order.Ordered.Gross - order.Invoiced.Gross,
                                      Net = order.Ordered.Net - order.Invoiced.Net,
                                      Discount = order.Ordered.Discount - order.Invoiced.Discount,
                                      Tax = order.Ordered.Tax - order.Invoiced.Tax,
                                      Value = order.Ordered.Value - order.Invoiced.Value,
                                    };
            }
            else
            {
                return order.Ordered;
            }
        }
    }
}
