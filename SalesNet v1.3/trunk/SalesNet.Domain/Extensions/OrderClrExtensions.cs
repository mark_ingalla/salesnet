﻿using System;
using Ap21API.Entities.Order;

namespace SalesNet.Domain.Extensions
{
    public static class OrderClrExtensions
    {
        public static bool HasNoSizes(this OrderClr colour)
        {
            return colour.SKUs.SKU.Count == 1 && colour.SKUs.SKU[0].SizeCode.Equals("");
        }
    }
}
