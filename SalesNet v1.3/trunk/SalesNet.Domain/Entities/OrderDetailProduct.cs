﻿using System.Linq;
using Ap21API.Entities;
using Ap21API.Entities.Order;
using Ap21API.Entities.Product;

namespace SalesNet.Domain.Entities
{
    public class OrderDetailProduct
    {
        public string ProductId { get; set; }
        public OrderDetail OrderDetail { get; set; }
        public Product Product { get; set; }
        public Currency Currency { get; set; }

        public OrderDetailProduct(string productId, bool orderDetailRepalceProduct = false)
        {
            ProductId = productId;
            var currentOrder = Order.CurrentOrder; // local so we don't have to make another http req costing us 2-350ms

            Currency = currentOrder.Currency;
    
            var productInCurrentOrder = from currentDetail in currentOrder.OrderDetails.OrderDetail
                                        where currentDetail.ProductId == productId
                                        select currentDetail;

            OrderDetail = productInCurrentOrder.FirstOrDefault();

            if (orderDetailRepalceProduct && OrderDetail != null)
            {
                Product = null;
            }
            else
            {
                Product = Product.Find(productId);
            }
        }       
    }
}
