﻿
namespace SalesNet.Domain.Entities
{
    public class PhonesSimple
    {
        private readonly Ap21API.Entities.Person.PhonesSimple phoneSimpleApi;

        public string Home
        {
            get { return phoneSimpleApi.Home; }
            set { phoneSimpleApi.Home = value; }
        }
        public string Mobile
        {
            get { return phoneSimpleApi.Mobile; }
            set { phoneSimpleApi.Mobile = value; }
        }
        public string Work
        {
            get { return phoneSimpleApi.Work; }
            set { phoneSimpleApi.Work = value; }
        }

        public PhonesSimple(Ap21API.Entities.Person.PhonesSimple phonesSimpleApi)
        {
            phoneSimpleApi = phonesSimpleApi;
        }
    }
    
}
