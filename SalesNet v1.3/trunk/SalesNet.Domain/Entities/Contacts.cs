﻿namespace SalesNet.Domain.Entities
{
    public class Contacts
    {
        public string Email { get; set; }
        public PhonesSimple Phones { get; set; }

        public Contacts(Ap21API.Entities.Person.Contacts contactsApi)
        {
            Email = contactsApi.Email;
            Phones = new PhonesSimple(contactsApi.Phones);
        }
    }
}
