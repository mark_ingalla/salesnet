﻿
using System;
using SalesNet.Domain.Implementations;

namespace SalesNet.Domain.Entities
{
    public class Person
    {
        private readonly Ap21API.Entities.Person.Person underlyingPerson;

        public string Password { get; set; }
        public bool IsAgent { get; set; }
        public string Id { get { return underlyingPerson.Id; } }
        public Contacts Contacts { get; set; }
        public Ap21API.Entities.Currency Currency
        {
            get
            {
                if (underlyingPerson != null)
                    return underlyingPerson.Currency;
                return null;

            }
        }

        public string FullName 
        { 
            get { return string.Format("{0} {1}", underlyingPerson.Firstname, underlyingPerson.Surname); }
        }

        public Person(Ap21API.Entities.Person.Person ap21Person)
        {
            this.underlyingPerson = ap21Person;
            IsAgent = ap21Person.IsAgent;
            Contacts = new Contacts(ap21Person.Contacts);
        }

        public void ChangePassword(string newPassword)
        {
            Password = newPassword;
            underlyingPerson.Password = EncryptionHelper.CreateHash(newPassword);
            underlyingPerson.Update();
        }

        public void GenerateNewPassword(int length = 8)
        {
            var f = true;
            string[] c = {
                             "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "qu", "r", "s", "t", "v", "w", "x"
                             , "z", "ch", "cr", "fr", "nd", "ng", "nk", "ph", "pr", "rd","sh","sl","sp", "st","th","tr"
                         };
            string[] v = {"a","e","i","o","u","y"};
            string r = "";

            var randomNumber = new Random();

            for (int i=length*1; i>0; i--)
            {
                double nextDouble = randomNumber.NextDouble();
                r = r.Insert(r.Length, f ? c[(int) (nextDouble*(c.Length - 1))] : v[(int) (nextDouble*(v.Length - 1))]);
                f = !f;
            }
            ChangePassword(r);
        }
    }

    public static class PersonFactory
    {
        public static Person Create(Ap21API.Entities.Person.Person apiPerson)
        {
            return new Person(apiPerson);
        }
    }
}
