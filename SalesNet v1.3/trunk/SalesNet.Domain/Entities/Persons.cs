﻿using Ap21API.Entities.Person;

namespace SalesNet.Domain.Entities
{
    public class Persons 
    {
        public static Person Find(string code = null, string email = null, string firstName = null, string password = null, string phone = null, string surname = null)
        {
            Ap21API.Entities.Person.Persons persons = Ap21API.Entities.Person.Persons.Find(code: code, email: email, firstName: firstName, password: password, phone: phone, surname: surname);

            if (persons != null && persons.Person.Count.Equals(1))
                return PersonFactory.Create(persons.Person[0]);

            return null;
        }
    }
}
