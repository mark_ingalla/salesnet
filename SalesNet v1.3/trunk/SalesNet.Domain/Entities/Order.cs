﻿using System;
using System.Collections.Generic;
using Ap21API.Entities.Order;

namespace SalesNet.Domain.Entities
{
    public class Order : Ap21API.Entities.Order.Order
    {

        public struct ProductQuantity
        {
            public string SkuId { get; set; }
            public string Quantity { get; set; }
        }

        public static Ap21API.Entities.Order.Order CurrentOrder
        {
            get
            {
                return Create();
            }
            set
            {
                CurrentOrder = value;
            }
        }

        /// <summary>
        /// Handles the operation on the orderdetail. 
        /// There are 4 Scenarios as being commented in line.
        /// </summary>
        /// <param name="order"></param>
        /// <param name="productQuantities"></param>
        /// <returns></returns>
        public static int UpdateCurrentOrder(OrderDetailProduct order, List<ProductQuantity> productQuantities)
        {
            //Maybe something like update order quantities
            var orderDetail = UpdateOrderQuantities(order, productQuantities);

            if (order.OrderDetail != null && orderDetail.Ordered.Quantity > 0)
            {
                orderDetail.Update(CurrentOrder.Id);
                return 1; // "OrderDetail has been updated successfully."
            } 
            
            else if (order.OrderDetail != null && orderDetail.Ordered.Quantity == 0)  //remove empty orderdetail
            {
                OrderDetail.Delete(CurrentOrder.Id, orderDetail.Id);
                return 0; // "OrderDetail has zero total quantities and has been deleted."
            }

            else if (order.OrderDetail == null && orderDetail.Ordered.Quantity > 0) //add new orderdetail
            {
                OrderDetail.Create(CurrentOrder.Id, orderDetail);
                return 2; // "OrderDetail has been added successfully."
            }

            else
            {
                return -1; // You are trying to add an empty order detail. 
            }
        }

        public static void SaveDeliveryDetails(string customerReference, string deliveryInstructions, string specialInstructions)
        {
            var currentOrder = CurrentOrder;
            currentOrder.CustomerReference = customerReference;
            currentOrder.DeliveryInstructions = deliveryInstructions;
            currentOrder.SpecialInstructions = specialInstructions;
            currentOrder.Update();
        }
        
        private static OrderDetail UpdateOrderQuantities(OrderDetailProduct productOrderDetail, List<ProductQuantity> productQuantities)
        {
            var orderDetail = GetOrderDetail(productOrderDetail);

            foreach (var productQuantity in productQuantities)
            {
                foreach (var colour in orderDetail.Clrs.Clr)
                {
                    foreach (var sku in colour.SKUs.SKU)
                    {
                        if(sku.SkuId.Equals(productQuantity.SkuId))
                        {
                            sku.Ordered.Quantity = decimal.Parse(productQuantity.Quantity);
                            colour.Ordered.Quantity += decimal.Parse(productQuantity.Quantity);
                            orderDetail.Ordered.Quantity += decimal.Parse(productQuantity.Quantity);
                        }
                    }
                }            
            }

            return orderDetail;
        }

        private static OrderDetail GetOrderDetail(OrderDetailProduct orderDetailProduct)
        {
            var orderDetail = orderDetailProduct.OrderDetail;

            return orderDetail ?? CreateBlankOrderDetail(orderDetailProduct);
        }

        private static OrderDetail CreateBlankOrderDetail(OrderDetailProduct orderDetailProduct)
        {
            try
            {
                var orderDetail = new OrderDetail()
                {
                    ProductId = orderDetailProduct.Product.Id,
                    ProductCode = orderDetailProduct.Product.Code,
                    ProductDescription = orderDetailProduct.Product.Description,
                    ProductName = orderDetailProduct.Product.Name,
                    Invoiced = new Matric(),
                    Ordered = new Matric(),
                    Outstanding = new Matric(),
                    Clrs = new OrderClrs()
                };

                orderDetail.Clrs.Clr = new OrderClrList();

                foreach (var clr in orderDetailProduct.Product.Clrs.Clr)
                {
                    var colour = new OrderClr()
                    {
                        ClrCode = clr.Code,
                        ClrId = clr.Id,
                        ClrName = clr.Name,
                        Invoiced = new Matric(),
                        Ordered = new Matric(),
                        Outstanding = new Matric(),
                        SKUs = new OrderSKUs()
                    };

                    colour.SKUs.SKU = new OrderSkuList();
                    foreach (var sku in clr.SKUs.SKU)
                    {
                        var orderSku = new OrderSKU()
                        {
                            Invoiced = new Matric(),
                            Ordered = new Matric(),
                            Outstanding = new Matric(),
                            SkuId = sku.Id,
                            Price = sku.Price,
                            Sequence = sku.Sequence,
                            SizeCode = sku.SizeCode
                        };
                        colour.SKUs.SKU.Add(orderSku);
                    }
                    orderDetail.Clrs.Clr.Add(colour);
                }
                return orderDetail;
            }
            catch 
            {
                throw new Exception("Could not create blank order detail");
            }
        }

        public static void FinaliseCurrentOrder(string customerReference, string deliveryInstructions, string specialInstructions)
        {
            var currentOrder = CurrentOrder;

            currentOrder.OrderState = "Processing";
            currentOrder.CustomerReference = customerReference;
            currentOrder.DeliveryInstructions = deliveryInstructions;
            currentOrder.SpecialInstructions = specialInstructions;
            currentOrder.Update();
        }

        
        public static void ChangeDeliveryAddress(string deliveryAddressId)
        {
            var currentOrder = CurrentOrder;

            currentOrder.Addresses.Delivery.Id = deliveryAddressId;

            currentOrder.Update();
        }

        public static void DeleteCurrentSingleOrder(string orderDetailId)
        {
            OrderDetail.Delete(CurrentOrder.Id, orderDetailId);
        }

        public static void DeleteCurrentOrder()
        {
            var currentOrder = CurrentOrder;
            for (int i = 0; i < currentOrder.OrderDetails.OrderDetail.Count; i++)
            {
                OrderDetail.Delete(currentOrder.Id, currentOrder.OrderDetails[i].Id);
            }
        }

    
    }

    public static class MatricHelper
    {
        public static void Add(this Matric matrix1, Matric matrix2)
        {
            matrix1.Discount += matrix2.Discount;
            matrix1.Gross += matrix2.Gross;
            matrix1.Net += matrix2.Net;
            matrix1.Quantity += matrix2.Quantity;
            matrix1.Tax += matrix2.Tax;
            matrix1.Value += matrix2.Value;
        }

        public static void Minus(this Matric matrix1, Matric matrix2)
        {
            matrix1.Discount -= matrix2.Discount;
            matrix1.Gross -= matrix2.Gross;
            matrix1.Net -= matrix2.Net;
            matrix1.Quantity -= matrix2.Quantity;
            matrix1.Tax -= matrix2.Tax;
            matrix1.Value -= matrix2.Value;
        }
    }

        
}
